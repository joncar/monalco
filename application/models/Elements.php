<?php

class Elements extends CI_Model{
	function categoria_fotos_privada($where = array()){
		$categorias = $this->db->get_where('categoria_fotos_privada',$where);
		foreach($categorias->result() as $n=>$f){
			$categorias->row($n)->fotos = $this->galeria_privada(array('categoria_galeria_id'=>$f->id));
		}
		return $categorias;
	}

	function galeria_privada($where = array()){
		$this->db->order_by('orden','ASC');
		$fotos = $this->db->get_where('galeria_privada',$where);
		foreach($fotos->result() as $n=>$f){
			$fotos->row($n)->foto = base_url('img/galeria/'.$f->foto);
		}
		return $fotos;
	}
}