CREATE VIEW view_tipo as
SELECT 
id,
nombre,
tipo,
CASE tipo
	WHEN 1 THEN 'Documentos'
    WHEN 2 THEN 'Autorizaciones'
    WHEN 3 THEN 'Becas'
END AS tipo_doc

FROM `categorias_documentos`