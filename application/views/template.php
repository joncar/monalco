<!doctype html>
<html lang="es">

    <head>
        <title><?= empty($title) ? 'Monalco' : $title ?></title>
        <meta http-equiv="content-type" content="text/html; charset=utf8"/>
        <meta charset="utf-8">
        <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
        <meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" />     
        <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>    
        <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
        <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

    <script>var URL = '<?= base_url() ?>';</script>


        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">    
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700italic,700,400italic%7CLato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic%7COpen+Sans:Open+Sans:400,800italic,800,700italic,700,600italic,600,400italic,300italic,300%7CHandlee">
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/megafish.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/superfish.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/flexslider.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery.qtip.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery-supersized.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery.nivo.slider.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/fancybox/jquery.fancybox.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/fancybox/helpers/jquery.fancybox-buttons.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/revslider/layers.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/revslider/settings.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/revslider/navigation.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/base.css?v=1.1"/> 
        <!-- Social-feed css -->
        <link href="<?= base_url() ?>css/social.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/responsive.css?v=1.0"/>
        <!--<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/retina.css"/> -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/editor.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/isotope.css"/> 
        <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        

        <?php if($this->router->fetch_method()!='editor'): ?>
        <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
        <?php endif ?>
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
        <?php if(!empty($customStyle)) echo $customStyle ?>
        <script>
            var URL = '<?= base_url() ?>';
        </script>
    </head>

    <body>      
        <!--<a href="whatsapp://send?text=Hola deseo contactarlos&phone=+34608188004" style="font-size:18px;padding:8px 8px;text-decoration:none;background-color:#189D0E;color:white;text-shadow:none;position:fixed;right:0;bottom:0px;z-index: 1000;"><i class="fa fa-whatsapp"></i> whatsapp</a>  -->
        <div id="buscarDiv" class="modalsearch">
            <div class="pattern"></div>            
            <a href="#" id="searchNavClose" class="searchNavClose"><img src="<?= base_url('img/x.png') ?>" style="width:32px;"></a>
            <form id="formsearch" action="<?= base_url('paginas/frontend/buscar') ?>">
                <input type="text" name="q" placeholder="Buscar">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
        <?= $this->load->view('notice'); ?>
        <?= $this->load->view($view) ?>
        <?= $this->load->view('includes/template/footer') ?>        
        <?php if($this->router->fetch_method()!='editor') $this->load->view('includes/template/scripts') ?>
    </body>

</html>
