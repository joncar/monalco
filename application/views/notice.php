<?php if(!empty($this->ajustes->topbar_text)): ?>
<div class="template-footer template-footer-sticky" id="notice">
    <!-- Top footer -->
    <div class="template-footer-top">
        <!-- Main -->
        <div class="template-main">
        <h4 class="editablesection" id="mce_0">
        	<?= $this->ajustes->topbar_text ?>
        	<a href="<?= $this->ajustes->topbar_link ?>" class="template-component-button template-component-button-style-1" id="contact-form-submit">INFO</a>
        </h4>
        </div>
    </div>
    <!-- Bottom footer -->
</div>
<?php endif ?>