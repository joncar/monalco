<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'notificaciones'=>array('admin/notificaciones'),
                        'b'=>array('blog_categorias','blog'),
                        'informaciones'=>array('informaciones_mensuales'),
                        'paginas'=>array('admin/paginas','admin/subscriptores','admin/categoria_galeria','admin/banner','idiomas/idiomas'),
                        'verano'=>array('cursos/cursos','cursos/cursos_semanas','cursos/casal_precios','cursos/casal_fotos'),
                        'calificaciones'=>array('admin/asignaturas','admin/calificaciones','admin/encuestas'),
                        'entradas'=>array('cursos/cursos_niveles','calendario/setCalendario','documento/documentos','extraescolares/extraescolar','servicio/servicios','proyecto/proyectos','comedor/comedor_menu','sport/sports','libros/libros','revista_anual','becas/becas','popups'),
                        'multimedia'=>array('admin/categoria_videos','admin/categoria_fotos_privada','admin/galeria_home'),
                        'seguridad'=>array('ajustes','cookies','grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'cursos_niveles'=>array('Cursos',''),
                        'categoria_galeria'=>array('Galeria',''),
                        'comedor_menu'=>array('Comedor',''),
                        'b'=>array('Blog','fa fa-book'),
                        'setCalendario'=>array('Calendario'),
                        'solicitudes'=>array('Presupuestos'),
                        'verano'=>array('Verano','fa fa-rocket'),
                        'notificaciones'=>array('Notificaciones','fa fa-bullhorn'),
                        'grupos_destinos'=>array('Grupos','fa fa-group'),
                        'paginas'=>array('Paginas','fa fa-file-powerpoint-o'),   
                        'idiomas'=>array('Textos','fa fa-file'),                     
                        'seguridad'=>array('Seguridad','fa fa-user-secret'),
                        'casal_precios'=>array('Precios',''),
                        'casal_fotos'=>array('Fotos','')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">EVA software</a>
        </div>
        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
