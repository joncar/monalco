<nav class="template-component-menu-default">

                <ul class="sf-menu template-clear-fix">

                    <li class="sf-mega-enable-0">
                        <a rel="canonical" href="<?= site_url() ?>">
                            <span class="template-icon-menu template-icon-menu-home"></span>Inici
                        </a>                        
                    </li>
                    <li class="sf-mega-enable-1">
                        <a rel="canonical" href="#"><span class="template-icon-menu template-icon-menu-people"></span>L'escola</a>
                        <div class="sf-mega template-layout-25x25x25x25 template-clear-fix">
                            <div class="template-layout-column-left">
                                <span class="sf-mega-header">L'ESCOLA</span>
                                <ul>
                                    <li><a rel="canonical" href="<?= site_url('historia.html') ?>">Història</a></li>
                                    
                                    <?php if($this->user->log): ?>
                                        <li><a rel="canonical" href="<?= site_url('quotes.html') ?>">Quotes</a></li>
                                    <?php else: ?>
                                        <li><a rel="canonical" href="javascript:quotes()">Quotes</a></li>
                                    <?php endif ?>
                                    <li><a rel="canonical" href="<?= site_url('instalacions.html') ?>">Instal·lacions</a></li>
                                    <li><a rel="canonical" href="<?= site_url('pec.html') ?>">PEC</a></li>
                                </ul>
                            </div>
                            <div class="template-layout-column-center-left">
                                <span class="sf-mega-header">PROPOSTA EDUCATIVA</span>
                                <ul>
                                    <li><a rel="canonical" href="<?= site_url('llar-d-infants.html') ?>">Llar d'infants</a></li>
                                    <li><a rel="canonical" href="<?= site_url('infantil.html') ?>">Infantil</a></li>
                                    <li><a rel="canonical" href="<?= site_url('primaria.html') ?>">Primària</a></li>
                                    <li><a rel="canonical" href="<?= site_url('secundaria.html') ?>">Secundària</a></li>
                                </ul>												
                            </div>
                            <div class="template-layout-column-center-right">
                                <span class="sf-mega-header">ORGANITZACIÓ</span>
                                <ul>
                                    <li><a rel="canonical" href="<?= site_url('organigrama.html') ?>">Organigrama</a></li>
                                    <li><a rel="canonical" href="<?= site_url('calendari-escolar') ?>">Calendari Escolar</a></li>
                                    <li><a rel="canonical" href="<?= site_url('horaris.html') ?>">Horaris</a></li>
                                    <li><a rel="canonical" href="<?= site_url('consell-escolar.html') ?>">Consell Escolar</a></li>
                                    <li><a rel="canonical" href="<?= site_url('ampa.html') ?>">AMPA</a></li>
                                </ul>												
                            </div>

                            <div class="template-layout-column-right">
                                <span class="sf-mega-header">SECRETARIA</span>
                                <ul>
                                    <?php if($this->user->log): ?>
                                        <li><a rel="canonical" href="<?= site_url('documents') ?>">Documents</a></li>
                                        <li><a rel="canonical" href="<?= site_url('autorizacions') ?>">Autoritzacions</a></li>
                                    <?php else: ?>                                        
                                        <li><a rel="canonical" href="javascript:documentos()">Documents</a></li>
                                        <li><a rel="canonical" href="javascript:autorizaciones()">Autoritzacions</a></li>
                                    <?php endif ?>
                                    
                                    <li><a rel="canonical" href="<?= site_url('libres-de-text.html') ?>">Llibres de Text</a></li>                                                                        
                                    <li><a rel="canonical" href="<?= site_url('beques-i-subvencions.html') ?>">Beques i subvencions</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-1">
                        <a rel="canonical" href="#"><span class="template-icon-menu template-icon-menu-printer"></span>Serveis</a>
                        <div class="sf-mega template-layout-50x50 template-clear-fix">
                            <div class="template-layout-column-left">                                
                                <ul>
                                    <li class=""><a rel="canonical" href="<?= base_url() ?>servicios/1-acollida-i-ludoteca.html">Acollida i Ludoteca</a></li>
                                    <li><a rel="canonical" href="<?= base_url() ?>servicios/2-menjador.html">Menjador</a></li>
                                    <li><a rel="canonical" href="<?= base_url() ?>extraescolars">Extraescolars</a></li>
                                    <li><a rel="canonical" href="<?= base_url() ?>sports.html">Esports</a></li>
                                    <li><a rel="canonical" href="<?= base_url() ?>casal.html">Casal</a></li>                                    
                                </ul>
                            </div>
                            <div class="template-layout-column-right">                                
                                <ul>
                                    <li><a rel="canonical" href="<?= base_url() ?>equip-psicopedag-ic.html">Servei psicopedagògic</a></li>                                    
                                    <li><a rel="canonical" href="<?= base_url() ?>equip-de-mediaci-escolar.html">Mediació</a></li>
                                    <li><a rel="canonical" href="<?= base_url() ?>natacio.html">Natació</a></li>
                                    <li><a rel="canonical" href="<?= base_url() ?>transport.html">Transport Escolar</a></li>
                                    <li><a rel="canonical" href="<?= base_url() ?>calma.html">Mediateca/Estudi Dirigit</a></li>                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-1">
                        <a rel="canonical" href="#"><span class="template-icon-menu template-icon-menu-projector"></span>Projectes</a>
                        <div class="sf-mega template-layout-25x25x25x25 template-clear-fix" style="display: none;">
                            <div class="template-layout-column-left" style="visibility: visible;">
                                <span class="sf-mega-header">PROJECTES CENTRE</span>
                                <ul>
                                    <?php 
                                        $this->db->order_by('orden','ASC');
                                        $this->db->limit('7');
                                        foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>1))->result() as $p): 
                                    ?>
                                        <li><a rel="canonical" href="<?= site_url('proyectos/'.toUrl($p->id.'-'.$p->proyectos_nombre)) ?>"><?= $p->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="template-layout-column-center-left" style="visibility: visible; border-right: 1px solid lightgray;">
                                <span class="sf-mega-header">&nbsp;</span>
                                <ul>                                                                        
                                    <?php 
                                        $this->db->order_by('orden','ASC');
                                        $this->db->limit('6',7);
                                        foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>1))->result() as $p): 
                                    ?>
                                        <li><a rel="canonical" href="<?= site_url('proyectos/'.toUrl($p->id.'-'.$p->proyectos_nombre)) ?>"><?= $p->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="template-layout-column-center-right" style="visibility: visible;">
                                <span class="sf-mega-header">PROJECTES TR@MS</span>
                                <ul>
                                    <?php 
                                        $this->db->order_by('orden','ASC');
                                        $this->db->limit('4');
                                        foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>2))->result() as $p): 
                                    ?>
                                        <li><a rel="canonical" href="#"><?= $p->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="template-layout-column-right" style="visibility: visible;">
                                <span class="sf-mega-header">&nbsp;</span>
                                <ul>                                    
                                    <?php 
                                        $this->db->order_by('orden','ASC');
                                        $this->db->limit('3',4);
                                        foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>2))->result() as $p): 
                                    ?>
                                        <li><a rel="canonical" href="<?= site_url('proyectos/'.toUrl($p->id.'-'.$p->proyectos_nombre)) ?>"><?= $p->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a rel="canonical" href="<?= base_url('blog') ?>"><span class="template-icon-menu template-icon-menu-speaker"></span>Blog</a>
                        <ul>
                            <?php 
                                $this->db->order_by('orden','ASC');
                                foreach($this->db->get('blog_categorias')->result() as $l): 
                            ?>
                                <li><a rel="canonical" href="<?= site_url('blog/categorias/'. toURL($l->id.'-'.$l->blog_categorias_nombre)) ?>"><?= $l->blog_categorias_nombre ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a rel="canonical" href="#"><span class="template-icon-menu template-icon-menu-gallery"></span>Galeria</a>
                        <ul>
                            <li><a rel="canonical" href="<?= site_url('galeria') ?>">Fotografies</a></li>
                            <li><a rel="canonical" href="<?= site_url('videos') ?>">Vídeos</a></li>
                            <?php if($this->user->log): ?>
                                <li><a rel="canonical" href="<?= site_url('zona-privada') ?>">Zona privada</a></li>
                            <?php else: ?>
                                <li><a rel="canonical" href="javascript:zonaprivada()">Zona privada</a></li>
                            <?php endif ?>
                        </ul>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a rel="canonical" href="<?= site_url('contacte.html') ?>"><span class="template-icon-menu template-icon-menu-envelope"></span>Contacte</a>
                    </li>
                    <li class="searchNav">
                        <a rel="canonical" href="#">
                            <span class="template-icon-menu template-icon-menu-magnifier"></span>
                            Buscar
                        </a>
                        
                    </li>
                    <?php if($this->user->log): ?>
                        <li class="sf-mega-enable-0"><a rel="canonical" href="javascript:sortirPrivada()"><span class="template-icon-menu template-icon-menu-meal"></span>Sortir</a></li>
                    <?php endif ?>
                </ul>

            </nav>

            <?php $this->load->view('includes/template/menu_mobile'); ?>


