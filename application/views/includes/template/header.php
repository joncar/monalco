
<div class="template-header-top">

    <div class="template-main template-clear-fix">

        <div class="template-header-top-logo">
            <a href="<?= site_url() ?>">
                <img src="<?= base_url() ?>img/logo_header.png" alt="" />
            </a>
        </div>

        <div class="template-header-top-menu template-clear-fix">	

            <?php $this->load->view('includes/template/menu'); ?>

        </div>

    </div>
</div>