<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places,geometry"></script>
<script type="text/javascript" src="<?= base_url() ?>js/frame.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/plugins.js"></script>
<div class="popup" id="divForm">
    <div class="template-component-notice template-component-notice-style-1">
		<div class="template-component-notice-content">
			<div class="template-component-notice-content-left">
				<div class="template-icon-feature template-icon-feature-size-medium template-icon-feature-name-bell-alt"></div>
			</div>
			<div class="template-component-notice-content-right">
				<div class="left">
					<h5 style="text-align: center">
						Informació mensual 
						<span class="mes" style="text-transform: uppercase;"></span>
						<div class="template-component-divider template-component-divider-style-2"></div>
					</h5>
					<p style="text-align: center;">
						Us informem de les activitats que realitzarem a <br/>l'escola durant el mes de
						<span class="mes"></span>
					</p>					
				</div>
				<div class="right">
					<a href="#" id="pdf" target="_new">
						<img src="<?= base_url('img/pdf.png') ?>" style="">
						<span style="font-size:10px">Descarregar PDF</span>
					</a>
				</div>
			</div>		
		</div>
	</div>
</div>

<div class="popup" id="zonaprivada">
    <div class="template-component-notice template-component-notice-style-1">
		<div class="template-component-notice-content">
			<div class="template-component-notice-content-left">
				<div class="template-icon-feature template-icon-feature-size-large template-icon-feature-name-lock-alt"></div>
			</div>
			<div class="template-component-notice-content-right">
				<form onsubmit="return logzone(this)">
					<label style="width:100%;">Benvingut a la zona privada <br/> d'Escola Monalco</label>
					<input type="password" name="pass" placeholder="La teva contrasenya" style="width:100%;margin-top: 13px;display: block; font-size:12px;">
					<input type="hidden" name="redirect" value="" id="footerRedirect">
					<button class="template-component-button template-component-button-style-1" type="submit" style="border: 0px;margin-top: 20px;">Entrar</button>
					<div id="message" style="font-size:12px; font-weight:200"></div>
				</form>
			</div>		
		</div>
	</div>
</div>

<div class="popup" id="sortirprivada">
    <div class="template-component-notice template-component-notice-style-1">
		<div class="template-component-notice-content">
			<div class="template-component-notice-content-left" style="background:#c52a17">
				<div class="template-icon-feature template-icon-feature-size-large template-icon-feature-name-heart-alt" ></div>
			</div>
			<div class="template-component-notice-content-right" style="padding:25px;">				
				<div style="width:200px;">Gracies per la teva visita. <br/> Fins aviat</div>
			</div>		
		</div>
	</div>
</div>

<script>
	function showInfo(titulo,pdf){
		<?php if(empty($_SESSION['user'])): ?>
			jQuery.fancybox.open({href: "#zonaprivada"});
		<?php else: ?>
			jQuery("#divForm #pdf").attr('href','<?= base_url() ?>files/'+pdf);
			jQuery("#divForm .mes").html(titulo);
			jQuery.fancybox.open({href: "#divForm"});
		<?php endif ?>
	}

	function zonaprivada(){
		jQuery("#footerRedirect").val('zona-privada');
		jQuery.fancybox.open({href: "#zonaprivada"});
	}

	function documentos(redirect)
	{
		if(!redirect){
			jQuery("#footerRedirect").val('documents');
		}else{
			jQuery("#footerRedirect").val(redirect);
		}
		jQuery.fancybox.open({href: "#zonaprivada"});
	}

	function showLogin(redirect){
		jQuery("#footerRedirect").val(redirect);
		jQuery.fancybox.open({href: "#zonaprivada"});	
	}

	function quotes(){
		jQuery("#footerRedirect").val('quotes.html');
		jQuery.fancybox.open({href: "#zonaprivada"});
	}

	function autorizaciones(){
		jQuery("#footerRedirect").val('autorizacions');
		jQuery.fancybox.open({href: "#zonaprivada"});
	}

	function sortirPrivada(){
		jQuery.fancybox.open({href: "#sortirprivada"});
		setTimeout(function(){			
			document.location.href="<?= base_url('main/unlog') ?>";
		},3000);
	}

	function logzone(form){
		var data = new FormData(form);
		data.append('email','zonaprivada@monalco.com');
		jQuery.ajax({
			url: '<?= base_url('main/loginZone') ?>',
            data: data,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(dat){
            	jQuery("#message").html(dat);
            }
		});
		jQuery("#message").html('Contraseña incorrecta');
		return false;
	}

	jQuery(document).on('click','#vermasproyectos',function(e){
		e.preventDefault();
		jQuery(".proyectos li").show();
		jQuery(this).hide();
		jQuery("#vermenosproyectos").show();

	});

	jQuery(document).on('click','#vermenosproyectos',function(e){
		e.preventDefault();
		jQuery(".proyectos li.oculto").hide();
		jQuery(this).hide();
		jQuery("#vermasproyectos").show();
		
	});
</script>





<!---- Social Feed ---->
<!-- jQuery -->
<!-- Codebird.js - required for TWITTER -->
<script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
<script src="<?= base_url() ?>js/social/bower_components/codebird-js/codebird.js"></script>
<!-- doT.js for rendering templates -->
<script src="<?= base_url() ?>js/social/bower_components/doT/doT.min.js"></script>
<!-- Moment.js for showing "time ago" and/or "date"-->
<script src="<?= base_url() ?>js/social/bower_components/moment/min/moment.min.js"></script>
<!-- Moment Locale to format the date to your language (eg. italian lang)-->
<script src="<?= base_url() ?>js/social/bower_components/moment/locale/it.js"></script>
<!-- Social-feed js -->
<script src="<?= base_url() ?>js/social/js/jquery.socialfeed.js"></script>
<script>
    jQuery(document).on('ready',function(){  
    	jQuery('.isotope-grid').isotope();
    	jQuery(".isotope-grid").css('min-height','1235.05px');
    	jQuery(".filter").on("click","li a",function(){var e=jQuery(this),a=e.parents("ul.filter").data("related-grid");if(a){e.parents("ul.filter").find("li").removeClass("active"),e.parent("li").addClass("active");var r=e.attr("data-filter");jQuery("#"+a).isotope({filter:r},function(){})}else alert("Please specify the dala-related-grid");return!1});  	
        jQuery('.social-feed-container').socialfeed({                        
            instagram:{
                accounts: ['@colmonalco'],  //Array: Specify a list of accounts from which to pull posts
                limit: 4,         
                type:'instagram',                         //Integer: max number of posts to load
                client_id: '33e0f749cac44ee8a999fee9f1ce9ed1',       //String: Instagram client id (option if using access token)
                access_token: '4552057016.33e0f74.2b898550540447239765ac6175ace8d4' //String: Instagram access token
            },
            twitter:{
		        accounts: ['@colmonalco'],                       //Array: Specify a list of accounts from which to pull tweets
		        limit: 4,                                    //Integer: max number of tweets to load
		        consumer_key: 'JB0T9F9gDxzMxxIamSIEA0fHA',           //String: consumer key. make sure to have your app read-only
		        consumer_secret: 'MWDrS2GmSRUVkHWIAqCkAlVK3Z21Pvc7stCAPbVzRfoHzr7pUy', //String: consumer secret key. make sure to have your app read-only
		        tweet_mode: 'compatibility',                  //String: change to "extended" to show the whole tweet
		        proxy:'<?= base_url() ?>proxy'
		     },
            
            // GENERAL SETTINGS
            length:400,                                     //Integer: For posts with text longer than this length, show an ellipsis.
            show_media:true,                                //Boolean: if false, doesn't display any post images
            media_min_width: 300,                           //Integer: Only get posts with images larger than this value
            update_period: 5000,                            //Integer: Number of seconds before social-feed will attempt to load new posts.
            template: "<?= base_url() ?>js/social/js/template.html",                         //String: Filename used to get the post template.            
            date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
            date_locale: "en",                              //String: The locale of the date (see: http://momentjs.com/docs/#/i18n/changing-locale/)
            moderation: function(content) {                 //Function: if returns false, template will have class hidden
            	jQuery(".isotope-grid").isotope('reloadItems');
	        	jQuery(".isotope-grid").isotope('layout');
                return  (content.text) ? content.text.indexOf('fuck') == -1 : true;
            },
            callback: function() {  
            	//Function: This is a callback function which is evoked when all the posts are collected and displayed
                jQuery(".isotope-grid").isotope('reloadItems');
	        	jQuery(".isotope-grid").isotope('layout');
	            jQuery("#allSocial").trigger('click');
	            jQuery(".thumb-overlay").fancybox();     
	            jQuery(".isotope-grid").show();  
            }
        });
    });
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>