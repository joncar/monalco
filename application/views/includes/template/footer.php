<!-- Footer -->
<div class="template-footer" id="footer">

    <!-- Top footer -->
    <div class="template-footer-top">

        <!-- Main -->
        <div class="template-main">

            <!-- Widget list -->
            <ul class="template-widget-list template-layout-25x25x25x25 template-clear-fix">

                <!-- Left column -->
                <li class="template-layout-column-left">
                    <h6>
                        <span>Zona Privada</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-text">


                            <p class="template-margin-reset hidden-xs">
                                <a href="http://monalco.clickedu.eu/user.php?action=login">
                                    <img src="<?= base_url() ?>img/edu.jpg" style="width: 75.8%; alt="View Post &quot;Drawing Lesson&quot;"/>
                                </a>
                                <br>
                            </p>
                            <p class="template-margin-reset hidden-xs" style="margin-top:-12px !important">
                                <a href="http://agora.xtec.cat/escolamonalco/moodle/">
                                    <img src="<?= base_url() ?>img/moodle.jpg" style="width: 75.8%; alt="View Post &quot;Drawing Lesson&quot;"/>
                                </a>
                            </p>

                            <p class="template-margin-reset hidden-xs" style="margin-top:12px !important">
                                <a href="http://www.fundaciotrams.org/" target="_new">
                                    <img src="<?= base_url() ?>img/trans.png" style="width: 75.8%; alt="View Post &quot;Drawing Lesson&quot;"/>
                                </a>
                            </p>
                            
                            <p class="template-margin-reset hidden-xs" style="margin-top:12px !important">
                                <a href="https://www.escolaconcertada.org/" target="_new">
                                    <img src="<?= base_url() ?>img/escoles.jpg" style="width: 75.8%; alt="View Post &quot;Drawing Lesson&quot;"/>
                                </a>
                            </p>
                            
                            <div class="responsivelogofooter">
                                <a href="http://monalco.clickedu.eu/user.php?action=login" target="_blank"><img src="<?= base_url() ?>img/edu.jpg" ></a>
                                <a href="http://agora.xtec.cat/escolamonalco/moodle/" target="_blank"><img src="<?= base_url() ?>img/moodle.jpg"></a>
                                <a href="http://www.fundaciotrams.org/" target="_blank"><img src="<?= base_url() ?>img/trans.png"></a>
                                <a href="https://www.escolaconcertada.org/" target="_blank"><img src="<?= base_url() ?>img/escoles.jpg"></a>
                                
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Center left column -->
                <li class="template-layout-column-center-left">
                    <h6>
                        <span>Informacions Mensuals</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-archive template-widget-archive-style-1">
                            <ul>
                                <?php 

                                    $this->db->limit(6);
                                    $this->db->order_by('fecha','ASC');
                                    foreach($this->db->get_where('informaciones_mensuales')->result() as $c): 
                                ?>
                                    <li>
                                        <?php if(empty($_SESSION['user']) && $c->requiere_contrasena==1): ?>
                                            <a href="javascript:showInfo()" title="<?= $c->titulo ?>">
                                                <?= utf8_encode(strftime('%B %Y',strtotime($c->fecha))) ?>
                                            </a>
                                        <?php else: ?>
                                            <a href="javascript:showInfo('<?= strftime('%B',strtotime($c->fecha)) ?>','<?= $c->pdf ?>')" title="<?= $c->titulo ?>">
                                                <?= utf8_encode(strftime('%B %Y',strtotime($c->fecha))) ?>
                                            </a>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </li>

                <!-- Center right column -->
                <li class="template-layout-column-center-right">
                    <h6>
                        <span>Secretaria</span>
                        <span></span>
                    </h6>
                    <div>
                        <p>Tots aquells documents descarregables:</p>
                        <div class="template-component-list template-component-list-style-1-alt template-margin-top-2">
                            <ul>
                                <?php 
                                    $this->db->order_by('orden','ASC')
                                             ->limit(6)
                                             ->where('mostrar_en_footer',1);
                                    foreach($this->db->get_where('documentos')->result() as $d): 
                                    if(!empty($_SESSION['user']) || $d->requiere_contrasena==0): 
                                ?>
                                    <li><a href="<?= base_url('files/documentos/'.$d->fichero) ?>" target="_new"><?= $d->nombre; ?></a></li>
                                <?php else: ?>
                                    <li><a href="javascript:documentos('files/documentos/<?= $d->fichero ?>')"><?= $d->nombre; ?></a></li>                                    
                                <?php endif ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </li>

                <!-- Right column -->
                <li class="template-layout-column-right">
                    <!--<h6>
                        <span>Notícies recents</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-recent-post template-widget-recent-post-style-1">
                            <ul>

                                <?php
                                $this->db->order_by('fecha','DESC'); 
                                $this->db->limit(2);
                                foreach($this->db->get_where('blog')->result() as $b): ?>
                                    <li>
                                        <a href="<?= site_url('blog/'.toURL($b->id.'-'.$b->titulo)) ?>">
                                            <img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="<?= $b->titulo ?>"/>
                                        </a>
                                        <h6>
                                            <a href="<?= site_url('blog/'.toURL($b->id.'-'.$b->titulo)) ?>" title="<?= $b->titulo ?>">
                                                <?= $b->titulo ?>
                                            </a>
                                        </h6>
                                        <span class="template-icon-blog template-icon-blog-date"><?= strftime("%B, %d %Y",strtotime($b->fecha)) ?></span>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>									
                    </div>-->

                    <h6>
                        <span>Menú Menjador</span>
                        <span></span>
                    </h6>

                    <div class="template-widget-archive template-widget-archive-style-1">
                        <ul>
                            <?php 
                                $this->db->order_by('orden','ASC');
                                $this->db->limit(2);
                                foreach($this->db->get_where('comedor_menu_descarga')->result() as $p): 
                            ?>
                                <li>
                                    <a href="<?= base_url('files/menjador/'.$p->fichero) ?>" title="<?= $p->titulo ?>" target="_new"><?= $p->titulo ?></a>
                                </li>
                            <?php endforeach ?>
                            
                        </ul>
                    </div>

                    <h6 style="margin-top: 40px;">
                        <span>Revista anual</span>
                        <span></span>
                    </h6>

                    <div class="template-widget-archive template-widget-archive-style-1">
                        <ul>
                            <li>
                                <a href="<?= base_url('files/revista_anual/'.$this->db->get('revista_anual')->row()->fichero) ?>" title="Revista anual <?= date("Y") ?>" target="_new">Revista anual <?= date("Y") ?></a>
                            </li>
                                            <a href="<?= base_url() ?>"><img src="<?= base_url() ?>img/logo_footer.png" class="template-footer-logo template-margin-top-2" alt="" /></a>
                        </ul>
                        
                    </div>
                    
                </li>

            </ul>

        </div>

    </div>

    <!-- Bottom footer -->
    <div class="template-footer-bottom">
        <div class="template-align-center template-main">
            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix template-margin-bottom-2">
                <ul>                    
                    <li class="template-component-social-icon-instagram"><a href="https://www.instagram.com/colmonalco/" class="template-component-social-icon-instagram"></a></li>
                    <li><a href="https://www.facebook.com/Collegi-Monalco-271404473272766/" class="template-component-social-icon-facebook"></a></li>
                    <li><a href="https://twitter.com/colmonalco" class="template-component-social-icon-twitter"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCYyjdYz11qURluB59cNmgsQ" class="template-component-social-icon-youtube"></a></li>
                </ul>
            </div>
            <div>
                &copy;&nbsp;
                <a href="#">Copyright © <?= date("Y") ?> - Col·legi Monalco - </a>
                by <a href="http://www.jordimagana.com">Jordi Magaña</a>
                - <a href="<?= base_url() ?>p/aviso-legal">Nota Legal -</a>
            </div>
        </div>
    </div>

</div>
<!-- Go to top button -->
<a href="#go-to-top" class="template-component-go-to-top"></a>