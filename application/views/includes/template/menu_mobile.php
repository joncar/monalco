<nav class="template-component-menu-responsive">

                <ul class="template-clear-fix">

                    <li>
                        <a href="#">MENÚ<span></span></a>
                        <ul>
                            <li>
                                <a href="<?= site_url() ?>">INICI</a>
                            </li>
                            <li>
                                <a href="#">L'ESCOLA <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <li><a href="<?= site_url('historia.html') ?>">HISTÒRIA <i class="fa fa-caret-"></i></a></li>
                                    <?php if($this->user->log): ?>
                                        <li><a href="<?= site_url('quotes.html') ?>">QUOTES</a></li>
                                    <?php else: ?>
                                        <li><a href="javascript:quotes()">QUOTES</a></li>
                                    <?php endif ?>
                                    <li><a href="<?= site_url('instalacions.html') ?>">INSTAL·LACIONS</a></li>
                                    <li><a href="<?= site_url('pec.html') ?>">PEC</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PROPOSTA EDUCATIVA <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <li><a href="<?= site_url('llar-d-infants.html') ?>">LLAR D'INFANTS</a></li>
                                    <li><a href="<?= site_url('infantil.html') ?>">INFANTIL</a></li>
                                    <li><a href="<?= site_url('primaria.html') ?>">PRIMÀRIA</a></li>
                                    <li><a href="<?= site_url('secundaria.html') ?>">SECUNDÀRIA</a></li>
                                </ul>   
                            </li>
                            <li>
                                <a href="#">ORGANITZACIÓ <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <li><a href="<?= site_url('organigrama.html') ?>">ORGANIGRAMA</a></li>
                                    <li><a href="<?= site_url('calendari-escolar') ?>">CALENDARI ESCOLAR</a></li>
                                    <li><a href="<?= site_url('horaris.html') ?>">HORARIS</a></li>
                                    <li><a href="<?= site_url('consell-escolar.html') ?>">CONSELL ESCOLAR</a></li>
                                    <li><a href="<?= site_url('ampa.html') ?>">AMPA</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">SECRETARIA <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <?php if($this->user->log): ?>
                                        <li><a href="<?= site_url('documents') ?>">DOCUMENTS</a></li>
                                        <li><a href="<?= site_url('autorizacions') ?>">AUTORITZACIONS</a></li>
                                    <?php else: ?>                                        
                                        <li><a href="javascript:documentos()">DOCUMENTS</a></li>
                                        <li><a href="javascript:autorizaciones()">AUTORITZACIONS</a></li>
                                    <?php endif ?>
                                    
                                    <li><a href="<?= site_url('libres-de-text.html') ?>">LLIBRES DE TEXT</a></li>                                                                        
                                    <li><a href="<?= site_url('beques-i-subvencions.html') ?>">BEQUES I SUBVENCIONS</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">SERVEIS <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <li class=""><a href="<?= base_url() ?>servicios/1-acollida-i-ludoteca.html">ACOLLIDA I LUDOTECA</a></li>
                                    <li><a href="<?= base_url() ?>servicios/2-menjador.html">MENJADOR</a></li>
                                    <li><a href="<?= base_url() ?>extraescolars">EXTRAESCOLARS</a></li>
                                    <li><a href="<?= base_url() ?>sports.html">ESPORTS</a></li>
                                    <li><a href="<?= base_url() ?>casal.html">CASAL</a></li>   
                                    <li><a href="<?= base_url() ?>equip-psicopedag-ic.html">SERVEI PSICOPEDAGÒGIC</a></li>                                    
                                    <li><a href="<?= base_url() ?>equip-de-mediaci-escolar.html">MEDIACIÓ</a></li>
                                    <li><a href="<?= base_url() ?>natacio.html">NATACIÓ</a></li>
                                    <li><a href="<?= base_url() ?>transport.html">TRANSPORT ESCOLAR</a></li>
                                    <li><a href="<?= base_url() ?>calma.html">MEDIATECA/ESTUDI DIRIGIT</a></li>                                  
                                </ul>
                            </li>
                            <li>
                                <a href="#">PROJECTES <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <?php 
                                        $this->db->order_by('orden','ASC');
                                        $this->db->limit('7');
                                        foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>1))->result() as $p): 
                                    ?>
                                        <li><a href="<?= site_url('proyectos/'.toUrl($p->id.'-'.$p->proyectos_nombre)) ?>"><?= $p->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                    <?php 
                                        $this->db->order_by('orden','ASC');
                                        $this->db->limit('4');
                                        foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>2))->result() as $p): 
                                    ?>
                                        <li><a href="<?= site_url('proyectos/'.toUrl($p->id.'-'.$p->proyectos_nombre)) ?>"><?= $p->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </li>
                            <li>
                                <a href="#">BLOG <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <?php foreach($this->db->get('blog_categorias')->result() as $l): ?>
                                        <li><a href="<?= site_url('blog/categorias/'. toURL($l->id.'-'.$l->blog_categorias_nombre)) ?>"><?= $l->blog_categorias_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </li>
                            <li>
                                <a href="#">GALERIA <i class="fa fa-chevron-down"></i></a>
                                <ul>
                                    <li><a href="<?= site_url('galeria') ?>">FOTOGRAFIES</a></li>
                                    <li><a href="<?= site_url('videos') ?>">VÍDEOS</a></li>
                                    <?php if($this->user->log): ?>
                                        <li><a href="<?= site_url('zona-privada') ?>">ZONA PRIVADA</a></li>
                                    <?php else: ?>
                                        <li><a href="javascript:zonaprivada()">ZONA PRIVADA</a></li>
                                    <?php endif ?>
                                </ul>
                            </li>
                            <li><a href="<?= site_url('contacte.html') ?>">CONTACTE</a></li>
                            <?php if($this->user->log): ?>
                                <li><a href="javascript:sortirPrivada()">SORTIR</a></li>
                            <?php endif ?>
                        </ul>

                    <li>

                </ul>

            </nav>