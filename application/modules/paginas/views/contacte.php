<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-10 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Contacta'ns</h1>
                <h6>Ens posarem en contacte amb tu</h6>
            </div>
        </div>
    </div>
</div>

<!-- Content -->
<div class="template-content">

    <!-- Main -->
    <div class="template-content-section template-main">
        

        <!-- Feature -->
        <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">

            <!-- Layout 33x33x33 -->
            <ul class="template-layout-33x33x33 template-clear-fix">

                <!-- Left column -->
                <li class="template-layout-column-left">
                    <div class="template-icon-feature template-icon-feature-name-envelope-alt"></div>
                    <h5>Adreça</h5>
                    <p>
                        Carrer Capellades, 2<br/>
                        08700 Igualada<br/>
                        Centre Concertat GC
                    </p>
                </li>

                <!-- Center column -->
                <li class="template-layout-column-center">
                    <div class="template-icon-feature template-icon-feature-name-mobile-alt"></div>
                    <h5>Telèfon i email</h5>
                    <p>
                        Tel: 93 803 15 77<br/>
                        Mòbil: 608 188 004<br/>
                        <a href="mailto:office@fable.com">info@monalco.cat</a>
                    </p>
                </li>

                <!-- Right column -->
                <li class="template-layout-column-right template-margin-bottom-5">
                    <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                    <h5>Horaris Secretaria</h5>
                    <p>
                        Dilluns a divendres<br/>
                        8h – 19h<br/>
                        Cap de setmana tancat
                    </p>
                </li>

            </ul>

        </div>

        <!-- Contact form -->
        <div class="template-component-contact-form" id="contactenosform">

            <form action="<?= base_url('paginas/frontend/contacto') ?>" method="post">
                
                <!-- Layout 50x50 -->
                <div class="template-layout-50x50 template-clear-fix">

                    <!-- Left column -->
                    <div class="template-layout-column-left">

                        <!-- Name -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-name">Nom *</label>
                            <input type="text" name="nombre" id="contact-form-name" />
                        </div>

                        <!-- E-mail -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-email"> E-mail *</label>
                            <input type="text" name="email" id="contact-form-email" />
                        </div>

                        <!-- Subject -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-subject">Tema</label>
                            <input type="text" name="tema" id="contact-form-subject" />
                        </div>

                    </div>

                    <!-- Right column -->
                    <div class="template-layout-column-right">
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-message">Missatge *</label>
                            <textarea rows="1" cols="1" name="message" id="contact-form-message"></textarea>
                        </div>
                    </div>

                </div>
                <div style="margin:10px">
                    <?php 
                        echo @$_SESSION['msj'];
                        unset($_SESSION['msj']);
                    ?>
                </div>
                <div class="template-form-line template-align-center">
                    <div style="display:inline-block; width:auto;">
                        <div class="g-recaptcha" data-sitekey="6LcjXm0UAAAAAMO1NqE3fW0ITNfL1l0xxSz6uXeN"></div>
                    </div>
                </div>

                <div class="template-form-line template-state-block" style="text-align:center; margin:20px;">
                    <input name="politicas" id="contact-form-subject" placeholder="Curs i informació sol·licitud matrícula" type="checkbox"> He llegit i accepto la <a href="<?= base_url('aviso-legal.html') ?>" target="_new">política de privacitat*</a>
                </div>

                <div class="template-form-line template-form-line-submit template-align-center">

                    <div class="template-state-block">

                        <!-- Submit button -->
                        <input class="template-component-button template-component-button-style-1" type="submit" value="enviar missatge" name="contact-form-submit" id="contact-form-submit"/>

                    </div>

                </div>

            </form>

        </div>

    </div>

    <div class="template-content-section template-padding-top-reset template-padding-bottom-reset">

        <!-- Google map -->
        <div class="template-component-google-map" id="template-component-google-map-1" data-lat="41.5861278" data-lon="1.6192896999999675"></div>

    </div>

</div>
