<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Secretaria</h1>
                <h6>Tot el paperam descarregable</h6>
            </div>
        </div>

    </div>
</div>

<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-content-layout template-main template-clear-fix ">
        <div class="template-component-header-subheader">
            <center><img src="<?= base_url('img/icon/autor.png') ?>" style="width:120px"></center>
            <h2>Autoritzacions</h2>                
            <div></div>
        </div>
        [documentos]
    </div>

</div>