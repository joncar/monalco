<?php foreach($this->db->get_where('extraescolar')->result() as $e): ?>
                <div class="template-layout-50x50 template-clear-fix template-margin-top-5">
                    <!-- Left column -->
                    <div class="template-layout-column-left">
                        <!-- Header -->
                        <img src="<?= base_url('img/extraescolares/'.$e->icono) ?>" style="display: inline-block;height: 70px;float: left;margin-right: 20px;"> 
                        <h4><?= $e->titulo ?></h4>
                        <p><?= strip_tags($e->texto)  ?></p>
                        
                        <!-- Vertical grid -->
                        <div class="template-component-vertical-grid template-margin-top-3">
                            <ul>
                                <?php foreach($this->db->get_where('extraescolar_apartados',array('extraescolar_id'=>$e->id))->result() as $n=>$a): ?>
                                    <li class="template-component-vertical-grid-line-<?= $n%2==0?'1':'2' ?>n">
                                        <div><?= $a->titulo ?></div>
                                        <div><?= $a->descripcion ?></div>
                                    </li>
                                <?php endforeach ?>
                                <?php $precios = $this->db->get_where('extraescolar_precios',array('extraescolar_id'=>$e->id)); ?>
                                <li class="<?= $precios->num_rows()>0?'activarPrecios':''; ?>" style="background: #c52a17b3; color:white; cursor:pointer">
                                    <div>
                                        <div class="iconomas" style="<?= $precios->num_rows()==0?'display:none;':'display: inline-block;'; ?>background:url(http://hipo.tv/monalco/img/accordion_icon.png);width: 36px;height: 36px;vertical-align: middle;margin-left: -20px;"></div>
                                         Preu: 
                                     </div>
                                    <div style="color:white"><?= $e->precio ?></div>
                                </li>
                                <?php foreach($precios->result() as $p): ?>
                                <li class="preciosocultos">
                                    <div>                                        
                                         <?= $p->descripcion ?>
                                    </div>
                                    <div><?= $p->precio ?></div>
                                </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                    <!-- Right column -->
                    <div class="template-layout-column-right">
                        <!-- Nivo slider -->
                        <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                            <div>                                
                                <img src="<?= base_url() ?>img/extraescolares/<?= $e->foto1 ?>" data-thumb="<?= base_url() ?>img/extraescolares/<?= $e->foto1 ?>" alt=""/>
                                <img src="<?= base_url() ?>img/extraescolares/<?= $e->foto2 ?>" data-thumb="<?= base_url() ?>img/extraescolares/<?= $e->foto2 ?>" alt=""/>
                                <img src="<?= base_url() ?>img/extraescolares/<?= $e->foto3 ?>" data-thumb="<?= base_url() ?>img/extraescolares/<?= $e->foto3 ?>" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>