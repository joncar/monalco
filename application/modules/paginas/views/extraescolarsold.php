<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Serveis</h1>
                <h6>Tot allò que oferim</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-main">

        <!-- Header and subheader -->
        <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
                <center><img src="http://hipo.tv/monalco/img/icon/extraescolaricon.png" style="width:120px"></center>
                <h2 id="mce_15" class="">Extraescolars</h2>                
                <div></div>
        </div>


        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4>Espai d'acollida i ludoteca</h4>

                <p>Al matí, els infants van arribant a l'acollida, on tenen opció d'esmorzar si a casa encara no ho han fet, i tenen diverses propostes: material de dibuix i manualitats, jocs de taula, jocs de construcció, trens, cotxes, animals, etc. A partir de les 8:30, els que vulguin poden sortir al jardí amb un dels acompanyants i poden fer ús de les instal·lacions: sorral, pista, caseta, etc. A les 8:55h acompanyem als infants als ambients.</p>

                <!-- List -->
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Esmorzar</li>
                        <li>Jocs i entreteniment</li>
                        <li>Manualitats</li>
                        <li>Acompanyament a les aules</li>											
                    </ul>
                </div>

                <!-- Vertical grid -->
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Horaris:</div>
                            <div>de 7 a 9h matí <br> de 17 a 19h tarda <br> de 13 a 13'30h migdia</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu mensual 1h:</div>
                            <div>30€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu mensual 1'30h:</div>
                            <div>35€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu mensual 3h:</div>
                            <div>40€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu dia eventual:</div>
                            <div>3€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu mensual mig dia:</div>
                            <div>10€</div>
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/acollidaicon2.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/acollidaicon2.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/5.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/11.jpg" alt=""/>
                    </div>
                </div>

            </div>

        </div>

    </div>
    
    <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4>Rítmica</h4>

                <p>Dues tardes a la setmana s'ofereix l'activitat extraescolar de rítmica, a triar entre dilluns i dimecres o dimarts i dijous.La gimnàstica rítmica és un esport que combina elements de ballet, gimnàstica, dansa i l'ús de diferents aparells com la corda, l'aro, la pilota, la cinta... Al llarg del curs les alumnes van aprenent diferents exercicis que els permeten treballar de forma individual i en equip al ritme de la música.</p>

                <!-- List -->
                <!-- 
<div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Esmorzar</li>
                        <li>Jocs i entreteniment</li>
                        <li>Manualitats</li>
                        <li>Acompanyament a les aules</li>											
                    </ul>
                </div>
 -->

                <!-- Vertical grid -->
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Dies:</div>
                            <div>Dilluns i Divendres</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Horari:</div>
                            <div>de 17 a 18h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Cursos:</div>
                            <div>A partir de P5</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu mensual:</div>
                            <div>40€</div>
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/ritmicaicon.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/ritmicaicon.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/ritmica2.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/ritmica2.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/ritmica3.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/ritmica3.jpg" alt=""/>
                    </div>
                </div>

            </div>

        </div>

    </div>
     <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-main">
    <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4>Robòtica</h4>

                <p>Activitat dirigida als alumnes de Primària. Es tracta d'una activitat per treballar en profunditat, i sempre al ritme de cada alumne, la tecnologia aplicada a la robòtica. Activitat organitzada per Ments Creatives </p>

                <!-- List -->
                <!-- 
<div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Esmorzar</li>
                        <li>Jocs i entreteniment</li>
                        <li>Manualitats</li>
                        <li>Acompanyament a les aules</li>											
                    </ul>
                </div>
 -->

                <!-- Vertical grid -->
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Horari Grup Iniciació:</div>
                            <div>Dimarts de 17 a 18'30h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Horari Grup Avançat:</div>
                            <div>Dijous de 17 a 18'30h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Equip docent:</div>
                            <div>Acadèmia LEGO Education</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Grups de treball:</div>
                            <div>grups de 15 alumnes en equips de 2/3 alumnes</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu mensual:</div>
                            <div>35€</div>
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/roboticaicon.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/roboticaicon.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/robot2.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/robot2.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/11.jpg" alt=""/>
                    </div>
                </div>

            </div>

        </div>
        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4>Esports</h4>

                <p>Dues o tres tardes a la setmana s'ofereix l'activitat extraescolar de poliesportiu, bàsquet, futbol sala....</p>

                <!-- List -->
                <!-- 
<div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Esmorzar</li>
                        <li>Jocs i entreteniment</li>
                        <li>Manualitats</li>
                        <li>Acompanyament a les aules</li>											
                    </ul>
                </div>
 -->

                <!-- Vertical grid -->
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Iniciació Esportiva P4 / P5</div>
                            <div>Dimarts i Dijous de 17 a 18h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Poliesportiu Masculí 1r/2n Primària</div>
                            <div>Dimecres i Divendres de 17 a 18h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Poliesportiu Femení 1r/2n Primària</div>
                            <div>Dimarts i Dijous de 17 a 18h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Poliesportiu Masculí 3r/4t Primària</div>
                            <div>Dimarts i Dijous de 18 a 19h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Poliesportiu Femení 3r/4t Primària</div>
                            <div>Dilluns de 17 a 18h / Divendres de 18 a 19h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Bàsquet Aleví Masculí 5è/6è Primària</div>
                            <div>Dilluns i Divendres de 19 a 20h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Bàsquet Aleví Femení 5è/6è Primària</div>
                            <div>Dilluns i Divendres de 18 a 19h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Futbol Sala Aleví Masculí 5è/6è Primària</div>
                            <div>Dimarts i Dijous de 19 a 20h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Bàsquet Infantil/Cadet Femení 1r-2n/3t-4t ESO</div>
                            <div>Dimarts, Dijous i Divendres de 19 a 20h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Futbol Sala Cadet</div>
                            <div>Dimecres de 18 a 19h</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu mensual:</div>
                            <div>30€</div>
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/sporticon.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/sporticon.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/esport2.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/esport2.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/esport3.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/esport3.jpg" alt=""/>
                    </div>
                </div>

            </div>

        </div>


    </div>
    </div>

    <!-- Section -->
   <!-- 
 <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Feature ~~>
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-app-alt"></div>
                        <h5>Magento Care</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pin-alt"></div>
                        <h5>Nunc Blandit</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-piano-alt"></div>
                        <h5>Mauris Solutide</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>
                </ul>
            </div>

            <!~~ Feature ~~>
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-people"></div>
                        <h5>Quisque Morbi</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pencil"></div>
                        <h5>Libro Retrum</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-paintbrush"></div>
                        <h5>Phasellus Novum</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>
                </ul>
            </div>

        </div>

    </div>

    <!~~ Section ~~>
    <div class="template-content-section template-background-image template-background-image-4">
        <div class="template-main">

            <!~~ Testimonials ~~>
            <div class="template-section-white">
                <div class="template-component-testimonial template-component-testimonial-style-2">
                    <ul class="template-layout-100">
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>Fable Kindergarten is a great place for my daughter to start her schooling experience. It’s welcoming and safe and my daughter loves being there.</p>
                            <div></div>
                            <span>Fredric Greene</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>I have a 1 year old and a 5 year old who have been attending for a year now. I can not tell you how much I adore and appreciate all of the wonderful staff.</p>
                            <div></div>
                            <span>Patricia Morgan</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>I have to say that I have 2 children ages 5 and 2 and have used various daycare’s in Kindergartens and this is by far the very best I have ever used.</p>
                            <div></div>
                            <span>Joann Simms</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>Fable Kindergarten is a great place for my daughter to start her schooling experience. It’s welcoming and safe and my daughter loves being there.</p>
                            <div></div>
                            <span>Shelia Perry</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>This letter is to recognize you and your staff for doing an excellent job teaching my son. His skill level is significantly better since attending Fable.</p>
                            <div></div>
                            <span>Tony I. Robinette</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>I have to say that I have 2 children ages 5 and 2 and have used various daycare’s in Kindergartens and this is by far the very best I have ever used.</p>
                            <div></div>
                            <span>Claire Willmore</span>
                        </li>
                    </ul>
                    <div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>			

        </div>
    </div>

    <!~~ Section ~~>
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Layout 50x50 ~~>
            <div class="template-layout-50x50 template-clear-fix">

                <!~~ Left column ~~>
                <div class="template-layout-column-left">

                    <!~~ Accordion ~~>
                    <div class="template-component-accordion">
                        <h6><a href="#">Educational Field Trips and Presentations</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Reporting on Individual Achievement</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>										
                        <h6><a href="#">Writing and Reading Classes</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Scientific Investigation Opportunities</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>
                    </div>

                </div>

                <!~~ Right column ~~>
                <div class="template-layout-column-right">

                    <!~~ Counters list ~~>
                    <div class="template-component-counter-list">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Art Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">165</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Writing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">125</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Drawing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">52</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left template-margin-bottom-reset">
                                <span class="template-component-counter-list-label">Yoga Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">75</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!~~ Section ~~>
    <div class="template-content-section template-main template-padding-top-reset template-padding-bottom-5">

        <!~~ Header and subheader ~~>
        <div class="template-component-header-subheader">
            <h2>Class Educators</h2>
            <h6>With education and experience in early childhood care</h6>
            <div></div>
        </div>

        <!~~ Team ~~>
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix">
                <li class="template-layout-column-left">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/11.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/11.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Linda Moore</h6>
                                    <span>Teacher</span>
                                </div>
                                <p><b>Linda Moore</b> Teacher</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">I hold a degree in Early Childhood Education and an advanced English language certificate. I have been working as a kindergarten teacher since 2002.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                </ul>
                            </div>					
                        </li>
                    </ul>			
                </li>
                <li class="template-layout-column-right">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/2.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/2.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Claire Willmore</h6>
                                    <span>Teacher</span>
                                </div>
                                <p><b>Claire Willmore</b> Teacher</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">I have just finished my studies in Early Childhood Education, and I am also the kid’s yoga teacher here at Fable. I enjoy cooking, swimming and bike riding in my free time.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                </ul>
                            </div>						
                        </li>
                    </ul>				
                </li>
            </ul>
        </div>

    </div>
 -->

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">
        <div class="template-main">

            <!-- Call to action -->
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!-- Content -->
                <div class="template-component-call-to-action-content">

                    <!-- Left column -->
                    <div class="template-component-call-to-action-content-left">

                        <!-- Header -->
                        <h3>Vols contactar amb extraescolars?</h3>

                    </div>

                    <!-- Right column -->
                    <div class="template-component-call-to-action-content-right">

                        <!-- Button -->
                        <a href="#" class="template-component-button template-component-button-style-1">Contacta aquí<i></i></a>

                    </div>									

                </div>

            </div>				

        </div>

    </div>

</div>