<?php 
    $fields = explode('--',$popup->campos);
    $hiddens = !empty($fields[1])?$fields[1]:'';
    $fields = explode(';',$fields[0]);
    $hiddens = explode(';',$hiddens);
?>  
<div class="popupContent" style="background:url(<?= base_url().'img/popups/'.$popup->foto ?>); background-size:cover; background-repeat:no-repeat;">
    <div>
        <form action="paginas/frontend/contacto/ajax" onsubmit="return sendForm(this,'#response');" method="post" style="display: inline-block; ">
            <div style="text-align:center;">
                <h4 id="mce_203" class="" style="margin-bottom: 0"><?= $popup->titulo ?></h4>
                <p style="color:#707070"><?= $popup->subtitulo ?></p>
            </div>
            <!-- Layout 50x50 -->
            <div class="template-clear-fix">
                <!-- Left column -->
                <!-- Name -->
                <?php foreach($fields as $f): if(!empty($f)): ?>
                    <div class="template-form-line template-state-block">
                        <?= $f ?>
                    </div>
                <?php endif; endforeach ?>                    
                <div class="template-form-line template-state-block" style="margin-top: 10px; text-align: center;">
                    <div id="response" style="max-width: 200px; display: none;"></div>
                </div>
                <div class="template-form-line template-state-block">                        
                    <input name="politicas" id="contact-form-subject" placeholder="Curs i informació sol·licitud matrícula" type="checkbox"> He llegit i accepto la <a href="<?= base_url('aviso-legal.html') ?>" target="_new">política de privacitat*</a>
                </div>
                <div class="template-form-line template-align-center">
                    <div class="template-state-block">
                        <!-- Submit button -->
                        <input class="template-component-button template-component-button-style-1" type="submit" name="contact-form-submit" id="contact-form-submit" value="Enviar">
                    </div>
                </div>                
            </div>
            <?php foreach($hiddens as $f): ?>
                <?= $f ?>
            <?php endforeach ?>        
            <input type="hidden" name="echo" value="1">        
                
            </form>
        </div>
    </div>