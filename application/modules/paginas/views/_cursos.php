<!-- Navigation -->
            <ul>
                <?php 
                    $this->db->order_by('orden','ASC'); 
                    $cursos = $this->db->get('cursos');
                ?>
                <?php foreach($cursos->result() as $c): ?>
                    <li>
                        <a href="#template-tab-<?= $c->id ?>"><?= $c->nombre ?></a>
                        <span></span>
                    </li>
                <?php endforeach ?>                
            </ul>

            <?php 
                
                foreach($cursos->result() as $c): 
            ?>
                <!-- Tab #1 -->
                <div id="template-tab-<?= $c->id ?>">
                    <?php if(!empty($c->pdf)): ?>
                        <div style="padding-bottom:40px; text-align: right;"><a href="<?= base_url('files/'.$c->pdf) ?>" target="_blank"><i class="fa fa-print"></i> Descarregar PDF</a></div>
                    <?php endif ?>
                    <?php 
                        $this->db->order_by('semana','ASC'); 
                        foreach($this->db->get_where('cursos_semanas',array('cursos_id'=>$c->id))->result() as $t): 
                    ?>
                        <?php 
                            $this->db->order_by('fecha','ASC'); 
                            $dias = $this->db->get_where('dias_semanas',array('cursos_semanas_id'=>$t->id)); 
                        ?>
                        <table class="preutable" style="margin-bottom: 50px;">

                                <tr class="odd2" style="background:<?= $t->color_tabla ?>">
                                    <th></th>
                                    <?php foreach($dias->result() as $d): ?>
                                    <th style="text-align: center"><b><?= strftime('%A <br/>%d/%m/%Y',strtotime($d->fecha)) ?></b></th>
                                    <?php endforeach ?>
                                </tr>
                                <tr>
                                    <th><b>MATÍ</b></th>
                                    <?php foreach($dias->result() as $d): ?>
                                    <td style="text-align: center"><?= $d->manana ?></td>
                                    <?php endforeach ?>
                                </tr>
                                <tr class="odd">
                                    <th><b>TARDA</b></th>
                                    <?php foreach($dias->result() as $d): ?>
                                    <td style="text-align: center"><?= $d->tarde ?></td>
                                    <?php endforeach ?>
                                </tr>

                            </tbody>
                        </table>
                    <?php endforeach ?>
                </div>
            <?php endforeach ?>