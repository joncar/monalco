<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>AMPA</h1>
                <h6>Què és i què fem</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
		<div class="template-content">
			<?php if(empty($content)): ?>
			<!-- Section -->
			<div class="template-content-section template-padding-bottom-5">

				<!-- Main -->
				<div class="template-main">

					<!-- Layout 50x50 -->
					<div class="template-layout-50x50 template-clear-fix">

						<!-- Left column -->
						<div class="template-layout-column-left template-margin-bottom-reset">

							<div class="template-align-center">

								<h3>Welcoming place that <strong>engages each child.</strong></h3>

								<div class="template-component-divider template-component-divider-style-2"></div>

								<div class="template-component-italic template-margin-top-3">
									Nulla adiscipling elite forte, nodis est advance pulvinar maecenas est dolor, novum elite lacina.
								</div>

								<p class="template-margin-top-3">
									Praesent arcu gravida a vehicula est node maecenas loareet maecenas morbi dosis luctus mode. Urna eget lacinia eleifend molibden dosis et gravida dosis sit amet terminal.
								</p>

								<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>

							</div>

						</div>

						<!-- Right column -->
						<div class="template-layout-column-right template-margin-bottom-reset">

							<!-- Gallery -->
							<div class="template-component-gallery">
								<ul class="template-layout-100 template-clear-fix">
									<li class="template-layout-column-left">
										<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
											<a href="media/image/_sample/1050x770/10.jpg" data-fancybox-group="gallery-1">
												<img src="media/image/_sample/690x506/10.jpg" alt="" />
												<span><span><span></span></span></span>
											</a>
											<div>
												<h6>Play Time In Kindergarten</h6>
												<span>Tenderhearts Class</span>
											</div>
											<p><b>Play Time In Kindergarten</b> Tenderhearts Class</p>
										</div>
									</li>
								</ul>
							</div>

						</div>

					</div>

				</div>

			</div>

			<!-- Section -->
			<div class="template-content-section template-padding-bottom-5 template-background-color-2">

				<!-- Main -->
				<div class="template-main">

					<!-- Layout 50x50 -->
					<div class="template-layout-50x50 template-clear-fix">

						<!-- Left column -->
						<div class="template-layout-column-left template-margin-bottom-reset">

							<!-- Gallery -->
							<div class="template-component-gallery">
								<ul class="template-layout-100 template-clear-fix">
									<li class="template-layout-column-left">
										<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
											<a href="media/image/_sample/1050x770/7.jpg" data-fancybox-group="gallery-1">
												<img src="media/image/_sample/690x506/7.jpg" alt="" />
												<span><span><span></span></span></span>
											</a>
											<div>
												<h6>Drawing and Painting Lessons</h6>
												<span>Tenderhearts Class</span>
											</div>
											<p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
										</div>
									</li>
								</ul>
							</div>

						</div>

						<!-- Right column -->
						<div class="template-layout-column-right template-margin-bottom-reset">

							<div class="template-align-center">

								<h3>Friendly atmosphere plus <strong>quality children care.</strong></h3>

								<div class="template-component-divider template-component-divider-style-2"></div>

								<div class="template-component-italic template-margin-top-3">
									Nulla adiscipling elite forte, nodis est advance pulvinar maecenas est dolor, novum elite lacina.
								</div>

								<p class="template-margin-top-3">
									Praesent arcu gravida a vehicula est node maecenas loareet maecenas morbi dosis luctus mode. Urna eget lacinia eleifend molibden dosis et gravida dosis sit amet terminal.
								</p>

								<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>

							</div>

						</div>

					</div>

				</div>

			</div>

			<!-- Section -->
			<div class="template-content-section template-padding-bottom-5">

				<!-- Main -->
				<div class="template-main">

					<!-- Layout 50x50 -->
					<div class="template-layout-50x50 template-clear-fix">

						<!-- Left column -->
						<div class="template-layout-column-left template-margin-bottom-reset">

							<div class="template-align-center">

								<h3>Dedicated classrooms with <strong>top skilled educators.</strong></h3>

								<div class="template-component-divider template-component-divider-style-2"></div>

								<div class="template-component-italic template-margin-top-3">
									Nulla adiscipling elite forte, nodis est advance pulvinar maecenas est dolor, novum elite lacina.
								</div>

								<p class="template-margin-top-3">
									Praesent arcu gravida a vehicula est node maecenas loareet maecenas morbi dosis luctus mode. Urna eget lacinia eleifend molibden dosis et gravida dosis sit amet terminal.
								</p>

								<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>

							</div>

						</div>

						<!-- Right column -->
						<div class="template-layout-column-right template-margin-bottom-reset">

							<!-- Gallery -->
							<div class="template-component-gallery">
								<ul class="template-layout-100 template-clear-fix">
									<li class="template-layout-column-left">
										<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
											<a href="media/image/_sample/1050x770/1.jpg" data-fancybox-group="gallery-1">
												<img src="media/image/_sample/690x506/1.jpg" alt="" />
												<span><span><span></span></span></span>
											</a>
											<div>
												<h6>Outdoor Activity During Recess</h6>
												<span>Tenderhearts Class</span>
											</div>
											<p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
										</div>
									</li>
								</ul>
							</div>

						</div>

					</div>

				</div>

			</div>

			<!-- Section -->
			<div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

				<!-- Main -->
				<div class="template-main template-section-white">

					<!-- Feature -->
					<div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
						<ul class="template-layout-25x25x25x25 template-clear-fix">
							<li class="template-layout-column-left">
								<div class="template-icon-feature template-icon-feature-name-teddy-alt"></div>
								<h5>Morbi Etos</h5>
								<p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
							</li>
							<li class="template-layout-column-center-left">
								<div class="template-icon-feature template-icon-feature-name-heart-alt"></div>
								<h5>Congue Gravida</h5>
								<p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna etos interdum vitae est.</p>
							</li>	
							<li class="template-layout-column-center-right">
								<div class="template-icon-feature template-icon-feature-name-graph-alt"></div>
								<h5>Maecenas Node</h5>
								<p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
							</li>
							<li class="template-layout-column-right">
								<div class="template-icon-feature template-icon-feature-name-globe-alt"></div>
								<h5>Placerat Etos</h5>
								<p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
							</li>
						</ul>
					</div>

				</div>

			</div>

			<!-- Section -->
			<div class="template-content-section template-padding-bottom-5 template-padding-top-reset">

				<!-- Main -->
				<div class="template-main">

					<!-- Header and subheader -->
					<div class="template-component-header-subheader">
						<h2>What&#039;s New</h2>
						<h6>Keep up to date with the latest news</h6>
						<div></div>
					</div>	

					<!-- Recent post -->
					<div class="template-component-recent-post template-component-recent-post-style-1">

						<!-- Layout 33x33x33 -->
						<ul class="template-layout-33x33x33 template-clear-fix">

							<!-- Left column -->
							<li class="template-layout-column-left">
								<div class="template-component-recent-post-date">October 03, 2014</div>
								<div class="template-component-image template-preloader">
									<a href="#">
										<img src="media/image/_sample/690x414/7.jpg" alt=""/>
										<span><span><span></span></span></span>
									</a>
									<div class="template-component-recent-post-comment-count">12</div>
								</div>
								<h5><a href="#">Drawing and Painting Lessons</a></h5>
								<p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
								<ul class="template-component-recent-post-meta template-clear-fix">
									<li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
									<li class="template-icon-blog template-icon-blog-category">
										<a href="#">Events</a>,
										<a href="#">Fun</a>
									</li>
								</ul>
							</li>

							<!-- Center column -->
							<li class="template-layout-column-center">
								<div class="template-component-recent-post-date">October 03, 2014</div>
								<div class="template-component-image template-preloader">
									<a href="#">
										<img src="media/image/_sample/690x414/2.jpg" alt=""/>
										<span><span><span></span></span></span>
									</a>
									<div class="template-component-recent-post-comment-count">4</div>
								</div>
								<h5><a href="#">Fall Parents Meeting Day</a></h5>
								<p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
								<ul class="template-component-recent-post-meta template-clear-fix">
									<li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
									<li class="template-icon-blog template-icon-blog-category">
										<a href="#">Dance</a>,
										<a href="#">Education</a>
									</li>
								</ul>			
							</li>

							<!-- Right column -->
							<li class="template-layout-column-right">
								<div class="template-component-recent-post-date">September 20, 2014</div>
								<div class="template-component-image template-preloader">
									<a href="#">
										<img src="media/image/_sample/690x414/9.jpg" alt=""/>
										<span><span><span></span></span></span>
									</a>
									<div class="template-component-recent-post-comment-count">4</div>
								</div>
								<h5><a href="#">Birthday in Kindergarten</a></h5>
								<p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
								<ul class="template-component-recent-post-meta template-clear-fix">
									<li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
									<li class="template-icon-blog template-icon-blog-category">
										<a href="#">Games</a>,
										<a href="#">General</a>
									</li>
								</ul>			
							</li>

						</ul>

					</div>
					
				</div>

			</div>
			<?php else: ?>
		        <?= $content ?>
		    <?php endif ?>
		</div>