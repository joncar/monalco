
            
            
            <!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_0" class="">Organització</h1>
                <h6 id="mce_13" class="">Entenem una mica l'escola</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-content-layout template-content-layout-sidebar-right template-main template-clear-fix">

                <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/consell.png') ?>" style="width:120px"></center>
                <h2>Consell Escolar</h2>                
                <div></div>
            </div>

		<!-- Content -->
		<div class="template-content-layout">

			<div data-id="section-1">
                <!-- Accordion -->
                <div class="template-component-accordion">
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Director del Centre</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Sra. Susana Vicente</th>
                                
                            </tr>
                        
                            
                        </table>                        
                    </div>

                     <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Representants del Titular</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Sr. Josep M. Vicente Ripollès</th>
                                
                            </tr>
                            <tr>
                                <th>Sr. Josep Manel Navarro Salgado</th>
                                
                            </tr>
                            <tr>
                                <th>Sra. Teresa Queralt Canet</th>
                                
                            </tr>
                            
                        </table>                        
                    </div>

                     <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Representants dels professors</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Sra. Eva Durán Pascual</th>
                                
                            </tr>
                            <tr>
                                <th>Sra. Alicia Corral Romero</th>
                                
                            </tr>
                            <tr>
                                <th>Sr. Jordi Banqué Amat</th>
                                
                            </tr>
                            <tr>
                                <th>Sra. Natalia Durán Cervera</th>
                                
                            </tr>
                            
                            
                        </table>                        
                    </div>
                    
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Representants dels pares</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Sra. Mònica Morist Borràs</th>
                                
                            </tr>
                            <tr>
                                <th>Sra. Sílvia González Campos</th>
                                
                            </tr>
                            <tr>
                                <th>Sra. Imma Soteras Bartolí</th>
                                
                            </tr>
                            <tr>
                                <th>Sra. Ester Ramon Robles</th>
                               
                            </tr>
                            
                        </table>                        
                    </div>
                    
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Representants dels alumnes</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Sra. Carla Calbó Marsal</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Sra. Gina Marsinyach Corral</th>
                                <td></td>
                            </tr>
                            
                            
                        </table>                        
                    </div>
                    
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Representant del personal no docent</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Sr. Martí Salanova García</th>
                                
                            </tr>
                            
                        </table>                        
                    </div>
                   
                </div>
            </div>
         </div>
     </div>
</div>
        