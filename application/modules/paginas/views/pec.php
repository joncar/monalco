
            
            
            
            
            
            
            
            <!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_13" class="">L'Escola</h1>
                <h6 id="mce_14" class="">Coneix el nostre centre</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/pec.png') ?>" style="width:120px"></center>
                <h2>Projecte Educatiu</h2>
                <div></div>
            <p class="template-margin-top-3" id="mce_17">Al llarg de tota la nostra trajectòria pedagògica, el Col·legi Monalco ha ofert un model educatiu altament participatiu, centrat en l’alumne, facilitador en la innovació, i perseguint sempre l’excel·lència.
<br>Per aquest motiu oferim una escola oberta, amb una gran capacitat d’innovació i un eficaç projecte lingüístic i de recerca científica i tecnològica.</p></div>
        <!-- Header and subheader -->                
                

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3 id="mce_15" class="">El seu futur <strong>el nostre repte</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            La nostra escola ofereix una proposta educativa completa dels 4 mesos fins als 16 anys.
                        </div>

                        <p class="template-margin-top-3" id="mce_16">L’objectiu de la nostra escola és preparar els alumnes amb les competències personals i acadèmiques per a una societat diferent, emmarcada en ple segle XXI. Volem obrir els joves a la societat amb una formació humana i amb una maduresa intel·lectual. Oferim un itinerari innovador de manera transversal, en totes les etapes educatives de l’escola, on l’alumne és el centre de l’aprenentatge. Un projecte compartit entre la família, l’alumne i l’escola.</p>

                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix" id="mce_17"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/pec1c.jpg" data-fancybox-group="gallery-1" id="mce_18"> <img src="[base_url]img/_sample/690x506/pec1c.jpg" alt=""> <span id="mce_19" class=""><span id="mce_20" class=""><span id="mce_21" class=""><br></span></span></span> </a><div><h6 id="mce_22">Molt més que un projecte educatiu</h6><span id="mce_23">Un projecte educatiu pluridimensional</span></div><p id="mce_24"><b>Play Time In Kindergarten</b> Tenderhearts Class</p></div></li></ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix" id="mce_28"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/pec2b.jpg" data-fancybox-group="gallery-1" id="mce_29" class=""><img src="[base_url]img/_sample/690x506/pec2.jpg" alt=""> <span id="mce_30" class=""><span id="mce_31"><span id="mce_32"></span></span></span></a><div><h6 id="mce_33" class="">L'alumne cerca, raona, investiga....</h6><span id="mce_34" class="">Escola Vivencial</span></div><p id="mce_35" class=""><b>L'alumne cerca, raona, investiga....</b>Escola Vivencial</p></div></li></ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3 id="mce_36" class="">Una escola <strong>oberta i vivencial</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            A través d’un projecte inclusiu, competencial i cooperatiu
treballem amb metodologies significatives, vivencials,
amb reptes i amb propostes perquè l’alumne cerqui,
raoni, pensi i sigui el protagonista del seu aprenentatge.
                        </div>

                        <p class="template-margin-top-3" id="mce_37">Volem que l’alumne s’emocioni aprenent. Sense emoció no hi ha aprenentatge. Proposem una atenció individualitzada respectant els ritmes i les necessitats de cada alumne. Potenciem els talents de cada alumne, a partir de les intel·ligències múltiples. Ajudem els alumnes davant de les dificultats amb les tutories individualitzades.</p>

                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3 id="mce_41" class="">Una escola <strong>familiar, acollidora i innovadora</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            La nostra identitat i caràcter propi.
                        </div>

                        <p class="template-margin-top-3" id="mce_42">El Monalco és una escola activa, familiar, innovadora i acollidora. Té la finalitat de promoure una educació basada en el respecte, en la solidaritat i en el diàleg. Així com també vol promoure el ple desenvolupament de la personalitat de l’alumnat tenint en compte els aspectes físics, psicoafectius i socials. És una escola catalana basada en la innovació educativa i oberta a totes les famílies. Som un centre laic i concertat per la Generalitat de Catalunya. Una escola catalana que assumeix la realitat sociocultural de Catalunya i el compromís ferm de servei a la cultura i a la llengua catalana. Com escola innovadora que som, formem part de la Fundació Tr@ms.</p>

                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix" id="mce_43"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/pec3b.jpg" data-fancybox-group="gallery-1" id="mce_44" class=""><img src="[base_url]img/_sample/690x506/pec3.jpg" alt=""> <span id="mce_45" class=""><span id="mce_46" class=""><span id="mce_47" class=""><br></span></span></span></a><div><h6 id="mce_48" class="">Escola activa, familiar, innovadora i acollidora</h6><span id="mce_49" class="">Participació dels pares amb les activitats</span></div><p id="mce_50" class=""><b>Escola activa, familiar, innovadora i acollidora</b> Participació dels pares amb les activitats</p></div></li></ul>
                    </div>

                </div>

            </div>

        </div>

    </div>
<div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
               <br>
                 <br><br>
                 <br>
                 <br><br>
                 <br>
                 <br><br>
                 <br>
                 <br>
                 <br>
            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

				<!-- Content -->
				<div class="template-main">

					<!-- Header -->
					<h3 id="mce_41" class="template-align-center">Projecte <strong>Pedagògic</strong></h3><div class="template-component-divider template-component-divider-style-2"></div>
					
					<!-- Section -->
					<div data-id="section-1">
<!-- Dropcap -->
<div class="template-component-italic template-margin-top-3 template-margin-bottom-3">Tenim un projecte metodològic inclusiu, competencial i cooperatiu per a la interdisciplinarietat a l’aula. Un projecte pedagògic fet a mida per a la nostra escola en què tots els alumnes aprenguin a aprendre i aprenguin a conviure.</div><div style="position: relative;" class="template-content-layout template-content-layout-sidebar-right template-main template-clear-fix ">

				<!-- Content -->
				<div class="">

					<!-- Layout 50x50 -->
					<ul class="template-layout-50x50" id="mce_90"><!-- Left column --><li class="template-layout-column-left" style="visibility: visible;" data-mce-style="visibility: visible;"><!-- Header --><h5 id="mce_25" class="">Què volem?</h5><!-- Section --><div data-id="section-1"><div class="template-component-list template-component-list-style-1"><ul id="mce_26" class=""><li>Prioritzar el treball competencial<strong></strong></li><li>L'ús de metodologies interdisciplinàries<strong></strong></li><li>Fomentar la inclusió<strong></strong></li><li>Demanar més autonomia a l'alumnat<strong></strong></li><li>Educar en valors<strong></strong></li><li>Ensenyar l'alumnat a treballar en equip<strong></strong></li></ul></div></div><!-- Preformatted text --></li><!-- Right column --><li class="template-layout-column-right" style="visibility: visible;" data-mce-style="visibility: visible;"><!-- Header --><h5 id="mce_30" class=""><br></h5><!-- Section --><div data-id="section-2"><div class="template-component-list template-component-list-style-1"><ul id="mce_31" class=""><li>Donar respostes educatives a la societat en què vivim<strong></strong></li><li>Fer un ensenyament més experimental i vivencial<strong></strong></li><li>Que l'alumne cerqui i trobi respostes<strong></strong></li><li>Alumnes oberts a la creativitat i a la innovació<strong></strong></li><li>Connectar amb els interessos de l'alumnat<strong></strong></li><li>Assolir les competències bàsiques per aplicar-les en contextos reals<strong></strong></li></ul></div></div><!-- Preformatted text --></li><!-- Left column --><li class="template-layout-column-left" style="visibility: visible;" data-mce-style="visibility: visible;"><!-- Header --><h5 id="mce_35" class="">7 principis bàsics del nostre projecte</h5><!-- Section --><div data-id="section-3"><div class="template-component-list template-component-list-style-1"><ul id="mce_36" class=""><li>L'alumne, com a protagonista del seu aprenentatge<strong></strong></li><li>Aprenentatge de naturalesa social<strong></strong></li><li>Les emocions, com a part integral d'aquest aprenentatge<strong></strong></li><li>Entorns d'aprenentatge que tinguin en compte les diferències individuals i les intel·ligències múltiples<strong></strong></li></ul></div></div><!-- Preformatted text --></li><!-- Right column --><li class="template-layout-column-right" style="visibility: visible;" data-mce-style="visibility: visible;"><!-- Header --><h5 id="mce_40" class=""><br></h5><!-- Section --><div data-id="section-4"><div class="template-component-list template-component-list-style-1"><ul id="mce_41" class=""><li>L'esforç de tot l'alumnat és la clau de l'aprenentatge<strong></strong></li><li>Avaluació contínua: formativa i formadora<strong></strong></li><li>Aprenentatge significatiu, globalitzat i interdisciplinari<strong></strong></li></ul></div></div><!-- Preformatted text --></li><!-- Left column --><li class="template-layout-column-left" style="visibility: visible;" data-mce-style="visibility: visible;"><!-- Header --><h5 id="mce_45" class="">Objectius</h5><!-- Section --><div data-id="section-5"><div class="template-component-list template-component-list-style-1"><ul id="mce_46"><li>Treballar la cohesió social, la inclusió i l'equitat<strong></strong></li><li>Fer protagonistes els alumnes<strong></strong></li><li>Treballar els valors<strong></strong></li><li>Incrementar la responsabilitat<strong></strong></li></ul></div></div><!-- Preformatted text --></li><!-- Right column --><li class="template-layout-column-right" style="visibility: visible;" data-mce-style="visibility: visible;"><!-- Header --><h5 id="mce_50" class="">Avantatges</h5><!-- Section --><div data-id="section-6"><div class="template-component-list template-component-list-style-1"><ul id="mce_51" class=""><li>Millora l'actitud dels estudiants i la seva autoestima<strong></strong></li><li>Proporciona més ajuda per assolir millors resultats<strong></strong></li><li>Millora la convivència<strong></strong></li><li>Millora el rendiment acadèmic<strong></strong></li></ul></div></div><!-- Preformatted text --></li></ul></div>

				<!-- Sidebar -->
				<div class="template-content-layout-column-right">
							
					<!-- Widgets list -->
					<!-- 
<ul class="template-widget-list" id="mce_131"><li><h6><span>Shortcodes</span> <span></span></h6><div><div class="template-widget-menu"><ul><li><a href="shortcode-accordion.html" title="Accordion">Accordion</a></li><li><a href="shortcode-audio.html" title="Audio">Audio</a></li><li><a href="shortcode-background-video.html" title="Background Video">Background Video</a></li><li><a href="shortcode-blockquote.html" title="Blockquote">Blockquote</a></li><li><a href="shortcode-button.html" title="Button">Button</a></li><li><a href="shortcode-call-to-action.html" title="Call To Action">Call To Action</a></li><li><a href="shortcode-class.html" title="Class">Class</a></li><li><a href="shortcode-contact-form.html" title="Contact Form">Contact Form</a></li><li><a href="shortcode-counter-box.html" title="Counter Box">Counter Box</a></li><li><a href="shortcode-counter-list.html" title="Counter List">Counter List</a></li><li><a href="shortcode-divider.html" title="Divider">Divider</a></li><li><a href="shortcode-dropcap.html" title="Dropcap">Dropcap</a></li><li><a href="shortcode-feature.html" title="Feature">Feature</a></li><li><a href="shortcode-flexslider.html" title="Flexslider">Flexslider</a></li><li><a href="shortcode-gallery.html" title="Gallery">Gallery</a></li><li><a href="shortcode-google-map.html" title="Google Map">Google Map</a></li><li><a href="shortcode-header-subheader.html" title="header And Subheader">Header And Subheader</a></li><li><a href="shortcode-header.html" title="Header">Header</a></li><li><a href="shortcode-iframe.html" title="Iframe">Iframe</a></li><li><a href="shortcode-layout.html" title="Layout">Layout</a></li><li><a href="shortcode-list.html" title="List">List</a></li><li><a href="shortcode-nivo-slider.html" title="Nivo Slider">Nivo Slider</a></li><li><a href="shortcode-notice.html" title="notice">Notice</a></li><li><a href="shortcode-preformatted-text.html" title="Preformatted Text">Preformatted Text</a></li><li><a href="shortcode-pricing-plan.html" title="Pricing Plan">Pricing Plan</a></li><li><a href="shortcode-recent-post.html" title="Recent Post">Recent Post</a></li><li><a href="shortcode-sitemap.html" title="Sitemap">Sitemap</a></li><li><a href="shortcode-social-icon.html" title="Social Icon">Social Icon</a></li><li><a href="shortcode-supersized.html" title="Supersized">Supersized</a></li><li><a href="shortcode-tab.html" title="Tab">Tab</a></li><li><a href="shortcode-team.html" title="Team">Team</a></li><li><a href="shortcode-testimonial.html" title="Testimonial">Testimonial</a></li><li><a href="shortcode-twitter-user-timeline.html" title="Twitter User Timeline">Twitter User Timeline</a></li><li><a href="shortcode-vertical-grid.html" title="Vertical Grid">Vertical Grid</a></li><li><a href="shortcode-zaccordion.html" title="Zaacordion">Zaacordion</a></li><li><a href="shortcode-list-of-icon.html" title="List Of Icon">List Of Icon</a></li><li><a href="shortcode-list-of-widget.html" title="List Of Widget">List Of Widget</a></li></ul></div></div></li></ul>				
				
 -->
				</div>

			</div>
					
					</div>
					

					<!-- Preformatted text -->
					<div class="template-component-preformatted-text" data-id-rel="section-1">
						
						<pre></pre>
						
					</div>
					

					<!-- Header -->
					

					<!-- Section -->
					

					
						
					</div>							

				</div>
<div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">
<h3 id="mce_41" class="template-align-center">Projecte <strong>Lingüístic</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div><div class="template-align-center template-component-italic template-margin-top-3 template-margin-bottom-3">
El coneixement de diferents llengües i cultures és clau per al desenvolupament d’una societat solidària i emprenedora</div>
            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">


                        
                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        

                        

                        <p class="template-component-dropcap template-component-dropcap-style-2" id="mce_136">Al Monalco apostem pel plurilingüisme, la immersió lingüística és un dels eixos rellevants del nostre Projecte Educatiu.<br> Treballem per aconseguir un projecte estructurat i coherent i un assoliment de les llengües estrangeres profitós, motivador i eficaç. Potenciem el domini perfecte (oral i escrit) de les dues llengües oficials a casa nostra, el català i el castellà, i també de l’anglès. <br><br>Les llengües que els alumnes aprenen al llarg de la seva escolaritat al Monalco són: <br>• Català <br>• Castellà <br>• Anglès <br>• I, com a segona llengua estrangera, poden escollir entre: francès o alemany <br><br>La llengua anglesa s’inicia de manera oral a partir de la Llar d’Infants (P1) a través de contes, cançons, jocs... <br><br>A partir de P3 (Infantil 3 anys) es fa una hora d’anglès diària. S’incorpora d’una manera lúdica i natural al dia a dia dels nostres alumnes com a eina fonamental de comunicació i expressió. S’imparteix l’anglès en una àrea no curricular: Science, Science Lab, Psicomotricitat, Educació física i teatre <br>L’anglès passa a ser també una eina de comunicació a l’escola per diferents canals.</p>

                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">                    
                    <div>
                        <img alt="" src="[base_url]img/_sample/690x506/pl1.jpg" data-thumb="[base_url]img/_sample/690x506/pl1.jpg">
                                                <img alt="" src="[base_url]img/_sample/690x506/ling1.jpg" data-thumb="[base_url]img/_sample/690x506/ling1.jpg">
                        <img data-thumb="[base_url]img/_sample/1050x770/eng1b.jpg" alt="" src="[base_url]img/_sample/1050x770/eng1b.jpg">
                    </div>
                </div>

            </div>


            </div>

        </div>

    </div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div>

				<!-- Sidebar -->
				<div class="template-content-layout-column-right">

					

				</div>

			</div><div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
                <ul class="template-layout-25x25x25x25 template-clear-fix" id="mce_54"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-teddy-alt"><br></div><h5 id="mce_55" class="">Potenciar</h5><p id="mce_56" class="">Totes les persones tenim talents que cal descobrir i potenciar</p></li><li class="template-layout-column-center-left"><div class="template-icon-feature template-icon-feature-name-heart-alt"><br></div><h5 id="mce_57" class="">Ajudar</h5><p id="mce_58">Totes les persones necessitem ajuda davant les dificultats</p></li><li class="template-layout-column-center-right"><div class="template-icon-feature template-icon-feature-name-graph-alt"><br></div><h5 id="mce_59">Orientar</h5><p id="mce_60" class="">Totes les persones, amb una bona orientació, podem assolir els nostres propòsits</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-globe-alt"><br></div><h5 id="mce_61" class="">Acompanyar</h5><p id="mce_62">L'objectiu de la nostra escola és una formació integral de l'alumne i posem eines per aconseguir-ho</p></li></ul>
            </div>

        </div>

    </div>

    <!-- Section -->

                        