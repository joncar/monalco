<!-- Tabs -->
<div class="template-component-tab">

    <!-- Navigation -->
    <ul>
    	<?php foreach($documentos->result() as $g): ?>
            <li>
                <a href="#template-tab-<?= $g->id ?>"><?= $g->nombre ?></a>
                <span></span>
            </li>
    	<?php endforeach ?>
    </ul>
    <?php foreach($documentos->result() as $g): ?>
        <!-- Tab #1 -->
        <div id="template-tab-<?= $g->id ?>">
            <!-- Accordion -->
            <div class="template-component-accordion">
                <!-- Header 1 -->
                <?php foreach($g->subcategorias->result() as $s): ?>
	                <h6>
	                    <a href="#"><h5><?= $s->nombre ?></h5></a>
	                </h6>
	                <!-- Content 1 -->
	                <div>
	                    <table class="table">
							<?php foreach($s->documentos->result() as $d): ?>
		                        <tr>
		                            <th><?= $d->nombre ?></th>
		                            <td style="text-align: center; width:14%"><a href="<?= base_url('files/documentos/'.$d->fichero) ?>" target="_new"><img src="<?= base_url('img/pdf.png') ?>" style="width:30px; display: inline-block;"><br/><span style="font-size:12px; text-decoration: underline;">Descarregar PDF</span></a></td>
		                        </tr>
	                        <?php endforeach ?>
	                    </table>                        
	                </div>
            	<?php endforeach ?>
            </div>
        </div>
	<?php endforeach ?>

    </div>