<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Proposta Educativa</h1>
                <h6>Completa des dels 4 mesos fins als 16 anys</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/inf.png') ?>" style="width:120px"></center>
                <h2>Infantil</h2>                
                <div></div>
            </div>

        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <!-- 
<h4>Què pretenem</h4>
 -->

 <b>El segon cicle d'Educació Infantil queda definit com l'etapa que va dels 3  als 6 anys.</b><br>               <p>Aquesta etapa es caracteritza principalment pel desenvolupament de les capacitats cognitives, psicomotores, d’equilibri personal, de relació interpersonal i d’integració social. 
<br>L'acció educativa d’aquest cicle passa pel coneixement d'un mateix i de l'entorn per així, de mica en mica, anar aconseguint una major independència i autonomia. Com més autònoma és la criatura, més oberta i receptiva restarà a qualsevol aprenentatge, ja que se sentirà més segura.
<br>És important en aquest cicle ensenyar a comunicar-se i expressar-se per mitjà dels diversos llenguatges (corporal, verbal, gràfic, plàstic, musical i matemàtic), així com també ensenyar als nens i nenes a pensar, a escoltar i a preguntar.
<br>En aquesta etapa donem molta importància a l'àrea artística dins el projecte propi de centre. El nostre alumnat té l'oportunitat d'assolir un desenvolupament més integral, a partir del treball cooperatiu. Així mateix, també potenciem l'expressió corporal i musical a través del teatre i de l'esport.
<br>La cura en les relacions entre iguals i l’acompanyament en el procés de creixement personal formen part del nostre dia a dia. 
<br>Oferim un aprenentatge globalitzat respectant els ritmes de treball de cada infant. La nostra fita és aconseguir una formació integral en què els alumnes desenvolupin les seves capacitats psicomotrius, cognitives, comunicatives i afectives al voltant dels àmbits a l'entorn dels quals gira el nostre projecte educatiu: TOTS.<br>Treballem per posar unes bones bases i valors que possibilitin a l’infant a desenvolupar totes les seves capacitats. 

</p>

                <!-- List -->
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Aprenentatge de qualitat</li>
                        <li>Aprenentatge proper i vivencial</li>
                        <li>Aprenentatge cooperatiu</li>
                        <li>Treball per projectes, tallers i ambients d'aprenentatge</li>
                        <li>Projecte AXIOMA (matemàtiques)</li>
                        <li>Fer l'infant protagonista del seu propi aprenentatge</li>
                        <li>Donar valor a l'activitat autònoma basada en la iniciativa del nen/a</li>
                        <li>Respectar els diferents ritmes d'aprenentatge</li>
                        <li>Cuidar el benestar emocional de cada infant</li>
                        <li>Mantenir el desig per descobrir i per aprendre</li>
                        <li>Vetllar per les necessitats bàsiques de cada alumne</li>
                        <li>Atenció personalitzada per a cada infant</li>
                        <li>Ampli horari setmanal d'immersió en llengua anglesa</li>											
                    </ul>
                </div>

                <!-- Vertical grid -->
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>P3 - Tortugues d'aigua i Tortugues de terra:</div>
                            <div>3-4 anys</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>P4 - Ossos i Pingüins:</div>
                            <div>4-5 anys</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>P5 - Tigres i Dofins:</div>
                            <div>5-6 anys</div>
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                                                <img src="<?= base_url() ?>img/_sample/690x506/inf1.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/inf1.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/inf5.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/inf5.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/inf3.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/inf3.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/inf41.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/inf41.jpg" alt=""/>
                         <img src="<?= base_url() ?>img/_sample/690x506/inf33.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/inf33.jpg" alt=""/>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                        <h5>Horaris</h5>
                        <p>de 9 a 13h i de 15 a 17 de la tarda</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-salver-alt"></div>
                        <h5>Acollida</h5>
                        <p>de 7 a 9h, de 13 a 13'30h i de 17 a 19h</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-people-alt"></div>
                        <h5>Famílies</h5>
                        <p>apropem l'escola a les famílies fomentant les tardes en família</p>			
                    </li>
                </ul>
            </div>

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-keyboard-alt"></div>
                        <h5>Extraescolars</h5>
                        <p>activitats lúdiques, esportives i culturals</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-flag-alt"></div>
                        <h5>Activitats</h5>
                        <p>anglès cada dia, natació dins l'horari escolar...</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-cutlery-alt"></div>
                        <h5>Menjador</h5>
                        <p>serveis de menjador i espai migdia</p>			
                    </li>
                </ul>
            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-background-image template-background-image-4">
        <div class="template-main">

            <!-- Testimonials -->
            <div class="template-section-white">
                <div class="template-component-testimonial template-component-testimonial-style-2">
                    <ul class="template-layout-100">
                        <li class="template-layout-column-left">
                           <i></i>
                            <p><br>A Infantil treballem per posar unes bones bases i valors que possibilitin a l´infant desenvolupar totes les seves capacitats</p>
                            <div></div>
                            <span></span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p><br>Volem aconseguir un model de persones competents i compromeses; creatives, sociables i respectuoses envers els altres i el seu entorn</p>
                            <div></div>
                            <span></span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p><br>A partir de P3 tenim un ampli horari setmanal d'immersió en la llengua anglesa amb professors especialistes. Treballem les llengües per convertir-les en eines imprescindibles per al coneixement i la relació amb l'entorn</p>
                            <div></div>
                            <span></span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p><br>Fomentem l´esperit científic dels infants apropant-los al món que ens envolta amb actitud de respecte i de curiositat. Els infants han d´observar i experimentar per aprendre.
</p>
                            <div></div>
                            <span></span>
                        </li>
                        <!-- 
<li class="template-layout-column-left">
                            <i></i>
                            <p>La clau no és ensenyar, és despertar.</p>
                            <div></div>
                            <span>Ernest Renan</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>L'art d'ensenyar és l'art d'ajudar a descobrir.</p>
                            <div></div>
                            <span>Mark Van Doren</span>
                        </li>
 -->
                    </ul>
                    <div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>			

        </div>
    </div>

    <!-- Section -->
    <!-- 
<div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Layout 50x50 ~~>
            <div class="template-layout-50x50 template-clear-fix">

                <!~~ Left column ~~>
                <div class="template-layout-column-left">

                    <!~~ Accordion ~~>
                    <div class="template-component-accordion">
                        <h6><a href="#">Educational Field Trips and Presentations</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Reporting on Individual Achievement</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>										
                        <h6><a href="#">Writing and Reading Classes</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Scientific Investigation Opportunities</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>
                    </div>

                </div>

                <!~~ Right column ~~>
                <div class="template-layout-column-right">

                    <!~~ Counters list ~~>
                    <div class="template-component-counter-list">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Art Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">165</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Writing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">125</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Drawing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">52</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left template-margin-bottom-reset">
                                <span class="template-component-counter-list-label">Yoga Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">75</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>
 -->

    <!-- Section -->
    <div class="template-content-section template-main template-padding-top-reset template-padding-bottom-5">
        <div class="template-component-counter-box template-margin-top-5">
            <center>
                <h3>Enquestes</h3>
                <div class="template-component-divider template-component-divider-style-2"></div>
                <div class="template-component-italic template-margin-top-3">
                    Resultats enquesta satisfacció Monalco
                </div>
            </center>
        </div>
        [encuesta2]
        <!-- Header and subheader -->
        <div class="template-component-header-subheader template-margin-top-5">
            <h2>L'equip d'infantil</h2>
            <h6>Comptem amb un bon equip humà de professionals ben cohesionats i que treballen en equip al servei dels nostres alumnes i les nostres famílies</h6>
            <div></div>
        </div>

        <!-- Team -->
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix">
                <li class="template-layout-column-left">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/banque.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/banque.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Jordi Banqué</h6>
                                    <span>Professor</span>
                                </div>
                                <p><b>Jordi Banqué</b> Professor</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">Volem crear uns entorns d'aprenentatge diferenciats tenint en compte les inteligències múltiples i les emocions de cada alumne.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!-- <li><a href="#" class="template-component-social-icon-mail"></a></li> -->
                                    <!-- 
<li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
 -->
                                </ul>
                            </div>					
                        </li>
                    </ul>			
                </li>
                <li class="template-layout-column-right">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/campo.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/campo.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Manel Campo</h6>
                                    <span>Alumne</span>
                                </div>
                                <p><b>Manel Campo</b>Alumne</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">Volem una escola per aprendre, per conviure, per créixer, per compartir. M'agrada molt la meva escola!</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!-- <li><a href="#" class="template-component-social-icon-mail"></a></li> -->
                                    <!-- 
<li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
 -->
                                </ul>
                            </div>						
                        </li>
                    </ul>				
                </li>
            </ul>
        </div>

    </div>
<!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <h2>El Blog d'Infantil</h2>
            <h6>T'informem de les activitats que es realitzen a Infantil</h6>
            <div></div>
        </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
            <div class="template-main">
                <!-- Recen post -->
                <div class="template-component-recent-post template-component-recent-post-style-1">
                    <ul class="template-layout-33x33x33 template-clear-fix">
                        [foreach:blog]
                        <li class="template-layout-column-[posicion]" style="visibility: visible;">
                            <div class="template-component-recent-post-date">[fecha]</div>
                            <div class="template-component-image">
                                <a href="[link]" id="mce_266" class="">
                                    <img src="[foto]" alt=""> 
                                    <span id="mce_267" class=""><span id="mce_268">
                                            <span id="mce_269">
                                                
                                            </span>
                                        </span>
                                    </span>
                                </a>                                
                            </div>
                            <h5 id="mce_270" class="">
                                <a href="[link]" id="mce_271" data-mce-href="#" class="">
                                    [titulo]
                                </a>
                            </h5>
                            <p id="mce_272" class="">
                                [descripcion]
                            </p>
                            <ul class="template-component-recent-post-meta template-clear-fix">
                                <li class="template-icon-blog template-icon-blog-author">
                                    <a href="[link]" id="mce_273" class="">[user]</a>
                                </li>
                                <li class="template-icon-blog template-icon-blog-category">
                                    <a href="[link]" id="mce_274" class="">Events</a>,
                                    <a href="[link]" id="mce_275" class="">Fun</a>
                                </li>
                            </ul>
                        </li>
                        [/foreach]
                     </ul>
                 </div>  

            </div>

    </div>

</div>