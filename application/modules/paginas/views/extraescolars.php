<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Serveis</h1>
                <h6>Tot allò que oferim</h6>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="template-content">
    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-main">
        <!-- Header and subheader -->
        <div class="template-content-section template-padding-bottom-5 template-main">
            <div class="template-component-header-subheader">
                <center><img src="[base_url]img/icon/extraescolaricon.png" style="width:120px"></center>
                <h2 id="mce_15" class="">Extraescolars</h2>
                <div></div>
            </div>
            <!-- Layout 50x50 -->
            

            [extraescolar]

            <script>
                $(".activarPrecios").on('click',function(){
                    var activados = $(this).parent().find('.preciosocultos')[0];
                    if($(activados).hasClass('show')){
                        $(this).parent().find('.preciosocultos').removeClass('show');
                        $(this).find('.iconomas').css('background-position','0 top');
                    }else{
                        $(this).parent().find('.preciosocultos').addClass('show');
                        $(this).find('.iconomas').css('background-position','0 bottom');
                    }
                });
            </script>
            <h6>*Si no hi ha més de 12 alumnes l'activitat quedarà anul·lada.</h6><br>
<h4>NOVETAT! Activitat extraescolar + esport per només 60€/mes</h4>
        </div>
    </div>
    
</div>