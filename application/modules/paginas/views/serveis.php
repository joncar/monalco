<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-3 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Serveis</h1>
                <h6>de l'escola</h6>
            </div>
        </div>
    </div>
</div>


<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-content-layout template-content-layout-sidebar-right template-main template-clear-fix">
        <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/menjador.png') ?>" style="width:120px"></center>
                <h2>Escola Verda</h2>                
                <div></div>
            </div>
        <!-- Content -->
        <div class="template-content-layout-column-left">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <h4 class="template-margin-bottom-50">Pre-Nursery Classes</h4>
                    <p class="template-margin-reset">Diaspis movum blandit elementum forte detos morbi est dosis maecenas vehicula gravida. Elipsis magna a terminal elite elementum ester elite forte maecenas magna etos interdum vitae est.</p>

                    <!-- List -->
                    <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                        <ul>
                            <li>Educational field trips</li>
                            <li>Welcoming place</li>
                            <li>Positive learning</li>
                            <li>Friendly environment</li>	
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">
                    <img src="<?= base_url() ?>img/_sample/525x531/3.png" alt=""/>
                </div>

            </div>		

        </div>

        <!-- Sidebar -->
        <div class="template-content-layout-column-right">

            <!-- Widdet list -->
            <ul class="template-widget-list">
                <!-- Call to action widget -->
                <li>
                    <div class="template-component-call-to-action template-component-call-to-action-style-3">
                        <div class="template-component-call-to-action-content">
                            <div class="template-component-call-to-action-content-left">
                                <h4>Free Call</h4>
                                <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum magna terminal est nulla.</p>
                                <h5>1-800-32-34-36</h5>
                            </div>
                            <div class="template-component-call-to-action-content-right">
                                <a href="#" class="template-component-button template-component-button-style-1">Contact Us<i></i></a>
                            </div>									
                        </div>
                    </div>				
                </li>
            </ul>				
        </div>
    </div>


    <!-- Section -->
    <div class="template-content-layout template-content-layout-sidebar-left template-main template-clear-fix ">

        <!-- Sidebar -->
        <div class="template-content-layout-column-left">

            <!-- Widdet list -->
            <ul class="template-widget-list">

               

                <!-- Accordion widget -->
                <li>
                    <h6>
                        <span>Accordion</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-component-accordion">
                            <h6><a href="#">Program with after-school care</a></h6>
                            <div>
                                <p>
                                    Pulvinar est metro ligula blandit maecenas retrum gravida cuprum magna terminal est nulla.
                                </p>
                            </div>
                            <h6><a href="#">Positive learning environment</a></h6>
                            <div>
                                <p>
                                    Elipsis magna a terminal nulla elementum elite forte maecenas est magna etos interdum vitae est.
                                </p>
                            </div>										
                            <h6><a href="#">Educational field trips</a></h6>
                            <div>
                                <p>
                                    Diaspis movum blandit elementum pulvinar detos morbi a dosis maecenas retrum gravida.
                                </p>
                            </div>
                            <h6><a href="#">Friendly and welcoming place</a></h6>
                            <div>
                                <p>
                                    Pulvinar est metro ligula blandit maecenas retrum gravida cuprum magna terminal est nulla.
                                </p>
                            </div>
                        </div>					
                    </div>			
                </li>                

            </ul>				

        </div>

        <!-- Content -->
        <div class="template-content-layout-column-right">
            <!-- Gallery -->
            <div class="template-component-gallery">

                <!-- Layout 50x50 -->
                <ul class="template-layout-50x50 template-clear-fix">

                    <!-- Left column -->
                    <li class="template-layout-column-left">
                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                            <a href="<?= base_url() ?>img/_sample/1050x770/12.jpg" data-fancybox-group="gallery-1">
                                <img src="<?= base_url() ?>img/_sample/690x506/12.jpg" alt="" />
                                <span><span><span></span></span></span>
                            </a>
                            <div>
                                <h6>Aliquam sit amet</h6>
                                <span>Fusce vitae diam aliquet vestibulum eros</span>
                            </div>
                            <p><b>Aliquam sit amet</b> Fusce vitae diam aliquet vestibulum eros/p>
                        </div>
                    </li>

                    <!-- Right column -->
                    <li class="template-layout-column-right">
                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                            <a href="<?= base_url() ?>img/_sample/1050x770/4.jpg" data-fancybox-group="gallery-1">
                                <img src="<?= base_url() ?>img/_sample/690x506/4.jpg" alt="" />
                                <span><span><span></span></span></span>
                            </a>
                            <div>
                                <h6>Sed faucibus arcu</h6>
                                <span>Maecenas faucibus ex ligula eget</span>
                            </div>
                            <p><b>Sed faucibus arcu</b> Maecenas faucibus ex ligula eget</p>
                        </div>			
                    </li>

                    <!-- Left column -->
                    <li class="template-layout-column-left">
                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                            <a href="<?= base_url() ?>img/_sample/1050x770/11.jpg" data-fancybox-group="gallery-1">
                                <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" alt="" />
                                <span><span><span></span></span></span>
                            </a>
                            <div>
                                <h6>Classes in Painting</h6>
                                <span>Bouncy Bears Class</span>
                            </div>
                            <p><b>Classes in Painting</b> Bouncy Bears Class</p>
                        </div>
                    </li>

                    <!-- Right column -->
                    <li class="template-layout-column-right">
                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                            <a href="<?= base_url() ?>img/_sample/1050x770/10.jpg" data-fancybox-group="gallery-1">
                                <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" alt="" />
                                <span><span><span></span></span></span>
                            </a>
                            <div>
                                <h6>Play Time In Kindergarten</h6>
                                <span>Tenderhearts Class</span>
                            </div>
                            <p><b>Play Time In Kindergarten</b> Tenderhearts Class</p>
                        </div>			
                    </li>

                </ul>

            </div>

            <!-- Section -->
            <div class="template-background-color-3 template-section-white">

                <!-- Call to action -->
                <div class="template-component-call-to-action template-component-call-to-action-style-1 template-margin-top-2">
                    <div class="template-component-call-to-action-content">
                        <div class="template-component-call-to-action-content-left">
                            <h3>Pulvinar forte maestro<br/>node terminal elipsis.</h3>
                        </div>
                        <div class="template-component-call-to-action-content-right">
                            <a href="#" class="template-component-button template-component-button-style-2">Learn More<i></i></a>
                        </div>									
                    </div>
                </div>

            </div>				

        </div>

    </div>
</div>