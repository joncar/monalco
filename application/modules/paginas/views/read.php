<?php          
	if($this->router->fetch_method()!='editor'){
		


		$page = str_replace('[menu]',$this->load->view('includes/template/menu',array(),TRUE),$page);
		$page = str_replace('[_cursos]',$this->load->view('_cursos',array(),TRUE),$page);
		$page = str_replace('[header]',$this->load->view('includes/template/header',array(),TRUE),$page);	
		$page = str_replace('[_grafico]',$this->load->view('_grafico',array(),TRUE),$page);	
		$page = str_replace('[_grafico2]',$this->load->view('_grafico2',array(),TRUE),$page);	
		$page = str_replace('[base_url]',base_url(),$page);	
		$page = str_replace('[encuesta1]',$this->load->view('_encuesta',array('curso'=>'3'),TRUE),$page);
		$page = str_replace('[encuesta2]',$this->load->view('_encuesta',array('curso'=>'4'),TRUE),$page);
		$page = str_replace('[encuesta3]',$this->load->view('_encuesta',array('curso'=>'1'),TRUE),$page);
		$page = str_replace('[encuesta4]',$this->load->view('_encuesta',array('curso'=>'2'),TRUE),$page);

		$this->db->limit(3);
		$this->db->order_by('fecha','DESC');			
		if(@$theme=='ampa'){
			$this->db->where('blog_categorias_id',6);
		}	
		if(@$theme=='llar-d-infants'){
			$this->db->where('blog_categorias_id',1);
		}
		if(@$theme=='infantil'){
			$this->db->where('blog_categorias_id',2);
		}	

		if(@$theme=='primaria'){
			$this->db->where('blog_categorias_id',3);
		}

		if(@$theme=='secundaria'){
			$this->db->where('blog_categorias_id',4);
		}

		if(@$theme=='sports'){
			$this->db->where('blog_categorias_id',7);
		}

		$blog = $this->db->get_where('blog',array('status'=>1));
		foreach($blog->result() as $nn=>$b){
			$b->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $b->foto = base_url('img/blog/'.$b->foto);
            $b->fecha = strftime('%B %d, %Y',strtotime($b->fecha));
            $b->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();            
            $b->descripcion = cortar_palabras(strip_tags($b->texto),20);
            $b->posicion = $nn==0?'left':'';
            $b->posicion = $nn==1?'center':$b->posicion;
            $b->posicion = $nn==2?'right':$b->posicion;
		}

		$this->db->order_by('orden','ASC');
		$sports = $this->db->get_where('sports');
		foreach($sports->result() as $n=>$s){
			$sports->row($n)->foto = base_url('img/sports/'.$s->foto);
			$sports->row($n)->position = $n%2==0?'left':'right';
			$sports->row($n)->enlace1 = !empty($s->calendario)?'<div><i class="fa fa-calendar" style="color:#a22f1d; font-size:12px !important;"></i> <a href="'.$s->calendario.'" target="_new" style="font-family:\'Lato\'; font-size:12px; font-weight:300;">Calendari</a></div>':'';
			$sports->row($n)->enlace2 = !empty($s->resultados)?'<div><i class="fa fa-trophy" style="color:#a22f1d;  font-size:12px !important;"></i> <a href="'.$s->resultados.'" target="_new" style="font-family:\'Lato\'; font-size:12px; font-weight:300;">Resultats</a></div>':'';
			$sports->row($n)->enlace3 = !empty($s->horarios)?'<div><i class="fa fa-clock-o" style="color:#a22f1d;  font-size:12px !important;"></i> <a href="'.base_url('img/sports/'.$s->horarios).'" target="_new" style="font-family:\'Lato\'; font-size:12px; font-weight:300;">Horaris</a></div>':'';
		}
		$this->db->where('main',1);
		$this->db->limit(6);		
		$this->db->order_by('orden','ASC');
		$proyectos = $this->db->get('proyectos');
		foreach($proyectos->result() as $n=>$p){
			$proyectos->row($n)->portada = base_url('img/proyectos/'.$p->foto);
			$proyectos->row($n)->foto = 'style="background:url('.base_url('img/proyectos/'.$p->foto).'); background-size: cover; background-repeat: no-repeat; background-position: 100% center;"';			
			$proyectos->row($n)->link = site_url('proyectos/'.toURL($p->id.'-'.$p->proyectos_nombre));
			$proyectos->row($n)->posicion = $n%2==0?'left':'right';
			$proyectos->row($n)->display = $n<4?'display:block':'display:none';			
			$proyectos->row($n)->class = $n<4?'':'oculto';

		}
		$page = $this->querys->fillFields($page,array('sports'=>$sports,'proyectos'=>$proyectos,'blog'=>$blog));

	}



    echo $page;
?>