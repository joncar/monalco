
            <!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_13" class="">L'Escola</h1>
                <h6 id="mce_14" class="">Coneix el nostre centre</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
                <center><img src="[base_url]img/icon/insta.png" style="width:120px"></center>
                <h2 id="mce_15" class="">Instal·lacions</h2>                
                <div></div>
            <p class="template-margin-top-3" id="mce_17">L’escola Monalco consta de quatre edificis amb una capacitat per a 800 alumnes: gimnàs, 5 patis exteriors, sorral, menjadors, biblioteca, laboratoris, ambients d'aprenentatge, pistes poliesportives, espais comuns de sessió i administració, lavabos, sala de professors, aula de música, hort... i zona verda i bosquet. Totes les aules tenen llum natural.</p></div>
        <!-- Header and subheader -->
        

        <!-- Feature -->
        <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
            <ul class="template-layout-33x33x33 template-clear-fix" id="mce_19"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-wallet-alt"><br></div><h5 id="mce_20">Aules</h5><p id="mce_21">Aules àmplies, amb llum natural, funcionals i innovadores. Tot pensant en la comoditat dels nostres alumnes.</p></li><li class="template-layout-column-center"><div class="template-icon-feature template-icon-feature-name-video-alt"><br></div><h5 id="mce_22">Instal·lacions esportives</h5><p id="mce_23">Interiors i exteriors. Amb un gimnàs situat al pis soterrani de l'escola consta de 100m2 de superfícies, vestuaris i tot el material esportiu necessari sota cobert.</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-paintbrush-alt"><br></div><h5 id="mce_24">Laboratoris</h5><p id="mce_25">Química, Física, Tecnologia i Biologia.</p></li></ul>
        </div>

        <!-- Feature -->
        <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium">
            <ul class="template-layout-33x33x33 template-clear-fix" id="mce_26"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-toy"><br></div><h5 id="mce_27">Aules TIC</h5><p id="mce_28">Aules funcionals i innovadores que consten de tot el material informàtic i de robòtica d'última generació.</p></li><li class="template-layout-column-center"><div class="template-icon-feature template-icon-feature-name-teddy"><br></div><h5 id="mce_29">Patis</h5><p id="mce_30">Trobareu 5 patis a tota l'escola i bosquet.</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-tags"><br></div><h5 id="mce_31">Novetats</h5><p id="mce_32">Estem ampliant l’escola. En aquesta 1a. fase hem reformat les aules de p3 i llar d'infants. Més funcionals innovadores i modernes. I els lavabos dels nois del pati. El menjador de la Llar d'infants. I les aules de 1r d’ ESO. També hem ampliat 300 metres quadrats de pati d'infantil i la Llar (curs 2016/18)
En una segona fase (estiu 2019) es reformarà entre altres coses les aules de P4.
En una 3a fase es reformarà el menjador i la sala de psicomotricitat dels més petits.
</p></li></ul>
        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-reset">

        <!-- Gallery -->
        <div class="template-component-gallery">
        <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix" id="mce_36"><li class="template-layout-column-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in99.jpg" data-fancybox-group="gallery-1" id="mce_37"> <img src="[base_url]img/_sample/1050x770/in99.jpg" alt=""> <span id="mce_38" class=""><span id="mce_39" class=""><span id="mce_40"></span></span></span> </a></div></li><li class="template-layout-column-center-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in98.jpg" data-fancybox-group="gallery-1" id="mce_41"> <img src="[base_url]img/_sample/1050x770/in98.jpg" alt=""> <span id="mce_42" class=""><span id="mce_43" class=""><span id="mce_44"></span></span></span> </a></div></li><li class="template-layout-column-center-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in97.jpg" data-fancybox-group="gallery-1" id="mce_45"> <img src="[base_url]img/_sample/1050x770/in97.jpg" alt=""> <span id="mce_46" class=""><span id="mce_47" class=""><span id="mce_48"></span></span></span> </a></div></li><li class="template-layout-column-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in96.jpg" data-fancybox-group="gallery-1" id="mce_49"> <img src="[base_url]img/_sample/1050x770/in96.jpg" alt=""> <span id="mce_50" class=""><span id="mce_51" class=""><span id="mce_52"></span></span></span> </a></div></li></ul>
            <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix" id="mce_36"><li class="template-layout-column-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/aula1.jpg" data-fancybox-group="gallery-1" id="mce_37"> <img src="[base_url]img/_sample/1050x770/aula1.jpg" alt=""> <span id="mce_38" class=""><span id="mce_39" class=""><span id="mce_40"></span></span></span> </a></div></li><li class="template-layout-column-center-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/aula2.jpg" data-fancybox-group="gallery-1" id="mce_41"> <img src="[base_url]img/_sample/1050x770/aula2.jpg" alt=""> <span id="mce_42" class=""><span id="mce_43" class=""><span id="mce_44"></span></span></span> </a></div></li><li class="template-layout-column-center-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst1.jpg" data-fancybox-group="gallery-1" id="mce_45"> <img src="[base_url]img/_sample/1050x770/inst1.jpg" alt=""> <span id="mce_46" class=""><span id="mce_47" class=""><span id="mce_48"></span></span></span> </a></div></li><li class="template-layout-column-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/aula4.jpg" data-fancybox-group="gallery-1" id="mce_49"> <img src="[base_url]img/_sample/1050x770/aula4.jpg" alt=""> <span id="mce_50" class=""><span id="mce_51" class=""><span id="mce_52"></span></span></span> </a></div></li></ul>
        <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix" id="mce_36"><li class="template-layout-column-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst9.jpg" data-fancybox-group="gallery-1" id="mce_37"> <img src="[base_url]img/_sample/1050x770/inst9.jpg" alt=""> <span id="mce_38" class=""><span id="mce_39" class=""><span id="mce_40"></span></span></span> </a></div></li><li class="template-layout-column-center-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst4.jpg" data-fancybox-group="gallery-1" id="mce_41"> <img src="[base_url]img/_sample/1050x770/inst4.jpg" alt=""> <span id="mce_42" class=""><span id="mce_43" class=""><span id="mce_44"></span></span></span> </a></div></li><li class="template-layout-column-center-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst5.jpg" data-fancybox-group="gallery-1" id="mce_45"> <img src="[base_url]img/_sample/1050x770/inst5.jpg" alt=""> <span id="mce_46" class=""><span id="mce_47" class=""><span id="mce_48"></span></span></span> </a></div></li><li class="template-layout-column-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst3.jpg" data-fancybox-group="gallery-1" id="mce_49"> <img src="[base_url]img/_sample/1050x770/inst3.jpg" alt=""> <span id="mce_50" class=""><span id="mce_51" class=""><span id="mce_52"></span></span></span> </a></div></li></ul>
        <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix" id="mce_36"><li class="template-layout-column-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in95.jpg" data-fancybox-group="gallery-1" id="mce_37"> <img src="[base_url]img/_sample/1050x770/in95.jpg" alt=""> <span id="mce_38" class=""><span id="mce_39" class=""><span id="mce_40"></span></span></span> </a></div></li><li class="template-layout-column-center-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in94.jpg" data-fancybox-group="gallery-1" id="mce_41"> <img src="[base_url]img/_sample/1050x770/in94.jpg" alt=""> <span id="mce_42" class=""><span id="mce_43" class=""><span id="mce_44"></span></span></span> </a></div></li><li class="template-layout-column-center-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in93.jpg" data-fancybox-group="gallery-1" id="mce_45"> <img src="[base_url]img/_sample/1050x770/in93.jpg" alt=""> <span id="mce_46" class=""><span id="mce_47" class=""><span id="mce_48"></span></span></span> </a></div></li><li class="template-layout-column-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/in92.jpg" data-fancybox-group="gallery-1" id="mce_49"> <img src="[base_url]img/_sample/1050x770/in92.jpg" alt=""> <span id="mce_50" class=""><span id="mce_51" class=""><span id="mce_52"></span></span></span> </a></div></li></ul>
        <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix" id="mce_36"><li class="template-layout-column-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst7.jpg" data-fancybox-group="gallery-1" id="mce_37"> <img src="[base_url]img/_sample/1050x770/inst7.jpg" alt=""> <span id="mce_38" class=""><span id="mce_39" class=""><span id="mce_40"></span></span></span> </a></div></li><li class="template-layout-column-center-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst2.jpg" data-fancybox-group="gallery-1" id="mce_41"> <img src="[base_url]img/_sample/1050x770/inst2.jpg" alt=""> <span id="mce_42" class=""><span id="mce_43" class=""><span id="mce_44"></span></span></span> </a></div></li><li class="template-layout-column-center-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst10.jpg" data-fancybox-group="gallery-1" id="mce_45"> <img src="[base_url]img/_sample/1050x770/inst10.jpg" alt=""> <span id="mce_46" class=""><span id="mce_47" class=""><span id="mce_48"></span></span></span> </a></div></li><li class="template-layout-column-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/inst8.jpg" data-fancybox-group="gallery-1" id="mce_49"> <img src="[base_url]img/_sample/1050x770/inst8.jpg" alt=""> <span id="mce_50" class=""><span id="mce_51" class=""><span id="mce_52"></span></span></span> </a></div></li></ul>
<ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix" id="mce_36"><li class="template-layout-column-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/i1.jpg" data-fancybox-group="gallery-1" id="mce_37"> <img src="[base_url]img/_sample/1050x770/i1.jpg" alt=""> <span id="mce_38" class=""><span id="mce_39" class=""><span id="mce_40"></span></span></span> </a></div></li><li class="template-layout-column-center-left"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/i25.jpg" data-fancybox-group="gallery-1" id="mce_41"> <img src="[base_url]img/_sample/1050x770/i25.jpg" alt=""> <span id="mce_42" class=""><span id="mce_43" class=""><span id="mce_44"></span></span></span> </a></div></li><li class="template-layout-column-center-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/i3.jpg" data-fancybox-group="gallery-1" id="mce_45"> <img src="[base_url]img/_sample/1050x770/i3.jpg" alt=""> <span id="mce_46" class=""><span id="mce_47" class=""><span id="mce_48"></span></span></span> </a></div></li><li class="template-layout-column-right"><div class="template-component-image template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/i4.jpg" data-fancybox-group="gallery-1" id="mce_49"> <img src="[base_url]img/_sample/1050x770/i4.jpg" alt=""> <span id="mce_50" class=""><span id="mce_51" class=""><span id="mce_52"></span></span></span> </a></div></li></ul>

        </div>						

    </div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div>		

    <!-- Section -->
    <!-- 
<div class="template-content-section template-padding-top-reset template-main">

        <!~~ Header and subheader ~~>
        <div class="template-component-header-subheader">
            <h2>Aules temàtiques</h2>
            <h6>We offer a quality children care</h6>
            <div></div>
        </div>

        <!~~ Feature ~~>
        <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
            <ul class="template-layout-33x33x33 template-clear-fix">
                <li class="template-layout-column-left">
                    <div class="template-icon-feature template-icon-feature-name-stroller"></div>
                    <h5>Nulla Pretium Etos</h5>
                    <p>Duis est elto rutrum node auctor vitae maximus est maecenas deto nulla interdum tortor semper.</p>
                </li>
                <li class="template-layout-column-center">
                    <div class="template-icon-feature template-icon-feature-name-signpost"></div>
                    <h5>Gravida Eget Mauris</h5>
                    <p>Mauris vestibulum tellus ut semper lacinia lorem enim pretium eros at lobortis nisi aliquet tellus sit mauris.</p>			
                </li>	
                <li class="template-layout-column-right">
                    <div class="template-icon-feature template-icon-feature-name-school"></div>
                    <h5>Egestas Codea Primus</h5>
                    <p>Ornare purus est dignissim porttitor tellus fusce maximus nunc eu purus suscipit faucibus lance eget.</p>		
                </li>
                <li class="template-layout-column-left">
                    <div class="template-icon-feature template-icon-feature-name-lab"></div>
                    <h5>Duis Ague Labortis</h5>
                    <p>Mauris vestibulum tellus ut semper lacinia lorem enim pretium eros at lobortis nisi aliquet tellus sit mauris.</p>
                </li>
                <li class="template-layout-column-center">
                    <div class="template-icon-feature template-icon-feature-name-bell"></div>
                    <h5>Loreat Lance Fuscia</h5>
                    <p>Ornare purus est dignissim porttitor tellus fusce maximus nunc eu purus suscipit faucibus lance eget.</p>			
                </li>	
                <li class="template-layout-column-right">
                    <div class="template-icon-feature template-icon-feature-name-meal"></div>
                    <h5>Class Libero Tellus</h5>
                    <p>Duis est elto rutrum node auctor vitae maximus est maecenas deto nulla interdum tortor semper.</p>		
                </li>
            </ul>
        </div>

        <div class="template-align-center">

            <!~~ Button ~~>
            <a href="#" class="template-component-button template-component-button-style-1">Go To Services<i></i></a>

        </div>

    </div>	

    <!~~ Section ~~>
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main ">

            <!~~ Header and subheader ~~>
            <div class="template-component-header-subheader">
                <h2>Aules especials</h2>
                <h6>Full day session starting from $25</h6>
                <div></div>
            </div>	

            <!~~ Pricing plan ~~>
            <div class="template-component-pricing-plan template-component-pricing-plan-style-2">
                <ul class="template-layout-50x50 template-clear-fix">
                    <li class="template-layout-column-left template-component-pricing-plan-background-1">
                        <div>
                            <div class="template-component-pricing-plan-price">
                                <span>24</span>
                                <span>alumnes</span>
                            </div>
                            <h5 class="template-component-pricing-plan-header">
                                Aula 1
                            </h5>
                            <div class="template-component-pricing-plan-description">
                                Movum elementum pulvinar detos morbi a dosis retrum gravida.
                            </div>
                            <div class="template-component-pricing-plan-feature">
                                <div class="template-component-list template-component-list-style-1">
                                    <ul>
                                        <li>8 am - 12 Noon Care</li>
                                        <li>3 Meals a Day</li>
                                        <li>Science Classes</li>
                                        <li>2 Educators</li>	
                                    </ul>
                                </div>
                            </div>
                            <div class="template-component-pricing-plan-button">
<!~~                                <a href="#" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>~~>
                            </div>
                        </div>
                    </li>
                    <li class="template-layout-column-right template-component-pricing-plan-background-2">
                        <div>
                            <div class="template-component-pricing-plan-price">
                                <span>15</span>
                                <span>alumnes</span>
                            </div>
                            <h5 class="template-component-pricing-plan-header">
                                Full Day Session
                            </h5>
                            <div class="template-component-pricing-plan-description">
                                Movum elementum pulvinar detos morbi a dosis retrum gravida.
                            </div>
                            <div class="template-component-pricing-plan-feature">
                                <div class="template-component-list template-component-list-style-1">
                                    <ul>
                                        <li>8 am - 12 Noon Care</li>
                                        <li>3 Meals a Day</li>
                                        <li>Science Classes</li>
                                        <li>3 Educators</li>	
                                    </ul>
 -->
                                </div>
                            