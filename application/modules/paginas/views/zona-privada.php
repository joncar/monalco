
<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom">

            <div class="template-header-bottom-background template-header-bottom-background-img-8 template-header-bottom-background-style-1">
                <div class="template-main">
                    <h1>Galeria fotogràfica</h1>
                    <h6>l'escola en imatges</h6>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">

        <!-- Tabs -->
        <div class="template-component-tab">

            <!-- Navigation -->
            <ul>
            	<?php foreach($galeria->result() as $g): ?>
	                <li>
	                    <a href="#template-tab-<?= $g->id ?>"><?= $g->nombre ?></a>
	                    <span></span>
	                </li>
            	<?php endforeach ?>
            </ul>
            <?php foreach($galeria->result() as $n=>$g): ?>
	            <!-- Tab #1 -->
	            <div id="template-tab-<?= $g->id ?>">
	                <!-- Gallery -->
	                <div class="template-component-gallery">
	                    <!-- Layout 50x50 -->
	                    <ul class="template-layout-25x25x25 template-clear-fix">	                    	
	                    	<?php $x = 0; ?>
	                    	<?php foreach($g->fotos->result() as $f): ?>
	                    	<?php 
	                    		switch($x){
	                    			case 0: $posicion = 'left'; break;	                    			
	                    			case 1: $posicion = 'center'; break;
                                    case 2: $posicion = 'right'; break;
	                    		}
	                    		$x++; 
	                    		$x = $x>2?0:$x;
	                    	?>
	                        <!-- Left column -->
	                        <li class="template-layout-column-<?= $posicion ?>">
	                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
	                                <a href="<?= base_url('img/galeria/'.$f->foto) ?>" data-fancybox-group="gallery-1">
	                                    <img src="<?= base_url('img/galeria/'.$f->foto) ?>" alt="" />
	                                    <span><span><span></span></span></span>
	                                </a>
	                                <div>
	                                    <h6><?= @explode('|',$f->titulo)[0] ?></h6>
	                                    <span><?= @explode('|',$f->titulo)[1] ?></span>
	                                </div>	                                
	                            </div>
	                        </li>
	                    	<?php endforeach ?>
	                    </ul>
	                </div>	
	            </div>
        	<?php endforeach ?>

            </div>

        </div>				

    </div>
    
    </div>

</div>