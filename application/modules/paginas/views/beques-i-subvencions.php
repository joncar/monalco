<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Secretaria</h1>
                <h6>Tot el paperam descarregable</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main ">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/beques.png') ?>" style="width:120px"></center>
                <h2>Beques i Subvencions</h2>                
                <div></div>
            </div>

        <div id="template-tab-1" aria-labelledby="ui-id-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-hidden="false">

            <div class="template-component-vertical-grid template-margin-top-3">
                <?php 
                    $this->db->order_by('orden','ASC');
                    $becas = $this->db->get_where('becas');
                if($becas->num_rows()>0): ?>
                <table class="preutable" style="margin-bottom: 50px;">
                    <tbody>
                        <tr>
                            <th style="text-align: left; font-weight:bold;">Nom</th>                        
                            <th style="text-align: left; font-weight:bold;">Enllaç</th>                        
                        </tr>
                        <?php 
                            
                            foreach($becas->result() as $b): ?>
                        <tr class="odd">
                            <td><?= $b->nombre ?></td>                        
                            <td><p><?= $b->descripcion ?></p><p><a href="<?= $b->enlace ?>"><?= $b->enlace ?></a></p></td>                        
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>    
                <?php else: ?>
                    No existeixen beques registrades en aquest moment
                <?php endif ?>                                
            </div>
            <!-- Button -->
                    
        </div>

        </div>

    </div>

    

</div>