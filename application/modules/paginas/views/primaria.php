<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_13" class="">Proposta Educativa</h1>
                <h6 id="mce_14" class="">Completa des dels 4 mesos fins els 16 anys</h6>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="template-content">
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
            <center><img src="[base_url]img/icon/prim.png" style="width:120px"></center>
            <h2 id="mce_15" class="">Primària</h2>
            <div></div>
        </div>
        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">
            <!-- Left column -->
            <div class="template-layout-column-left">
                <!-- Header -->
                <!-- <h4>Metodologia</h4> -->
                <b>Aquesta és una etapa que dura dels 6 als 12 anys.</b><br>                <p id="mce_16" class="">En aquesta etapa volem formar alumnes respectuosos, tolerants i compromesos. Treballem les emocions i l'equilibri personal dels nostres alumnes. <br>La nostra finalitat és preparar-los per donar respostes innovadores en una societat canviant i en evolució constant. <br>És una etapa molt important per al seu creixement individual i col·lectiu. <br>Aprendre a aprendre és la base de l'Educació Primària. <br>De manera transversal i interdisciplinària, l'alumnat adquireix hàbits de treball, valors i normes per a la seva autonomia personal. Aprèn a resoldre problemes i situacions des de qualsevol de les àrees curriculars, aprèn a gestionar les seves emocions i les dels altres. <br>També fomentem l’esperit i el pensament crític, la iniciativa, el gust per aprendre, la capacitat de l’esforç per aconseguir allò que volem i/o ens proposem, la cultura del treball amb ganes i estimació pel que es fa. <br>L’assoliment de les competències bàsiques a l’etapa de Primària és la base per continuar l’aprenentatge i el projectes que es proposin. <br>En aquesta etapa donem molta importància a l'àrea artística dins el projecte propi de centre. El nostre alumnat té l'oportunitat d'assolir un desenvolupament més integral, a partir del treball cooperatiu de plàstica. Així mateix, també potenciem l'expressió corporal i musical, a través del teatre i l'esport. Treballem per posar unes bones bases i valors que possibilitin a l'infant a desenvolupar totes les seves capacitats.</p>
                <!-- List -->
                
                <!-- Vertical grid -->
                
            </div>
            <!-- Right column -->
            <div class="template-layout-column-right">
                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="[base_url]img/_sample/690x506/inf40.jpg" data-thumb="[base_url]img/_sample/690x506/inf40.jpg" alt="">
                        <img src="[base_url]img/_sample/690x506/pr1.jpg" data-thumb="[base_url]img/_sample/690x506/pr1.jpg" alt="">
                        <img src="[base_url]img/_sample/690x506/pr2.jpg" data-thumb="[base_url]img/_sample/690x506/pr2.jpg" alt="">
                        <img src="[base_url]img/_sample/690x506/prim4.jpg" data-thumb="[base_url]img/_sample/690x506/prim4.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="template-layout-50x50 template-clear-fix">
            <!-- Left column -->
            <div class="template-layout-column-left">
                <!-- Header -->
                <!-- <h4>Metodologia</h4> -->
                
                <!-- List -->
                <div class="template-component-list template-component-list-style-1">
                    <ul id="mce_17" class=""><li>Aprenentatge de qualitat</li><li>Potenciar l'autonomia de l'alumne a partir de rutines de treball, estudi i hàbits</li><li>Potenciar la seva curiositat per descobrir i aprendre</li><li>Incloure com a valor principal les arts plàstiques, la música i l'esport</li><li>Consolidar els aprenentatges fonamentals</li><li>Fomentar les habilitats personals i socials</li><li>Garantir per a tot l'alumnat l'assoliment d'un bon nivell de comprensió lectora, d'expressió oral i escrita, d'agilitat en el càlcul i en la resolució de problemes i d'autonomia d'aprenentatge</li></ul>
                </div>
                <!-- Vertical grid -->
                
            </div>
            <!-- Right column -->
            <div class="template-layout-column-right">
                <!-- Header -->
                <!-- <h4>Metodologia</h4> -->
                
                <!-- List -->
                <div class="template-component-list template-component-list-style-1">
                    <ul id="mce_17" class=""><li>Introduïm una àrea curricular en anglès (Science i ScienceLab)</li><li>Vetllar perquè els continguts vinculats a la coeducació, la ciutadania, la convivència, la solidaritat, l'educació ambiental... formin part dels processos de desenvolupament i aprenentatge</li><li>Utilitzar correctament la llengua catalana i castellana, tant oralment com per escrit, i impulsar, alhora, l'aprenentatge de les llengües estrangeres</li><li>Integrar les tecnologies de la informació i comunicació en el procés d'ensenyament i aprenentatge</li><li>Cuidar el benestar emocional de cada infant</li><li>Atenció personalitzada per a cada infant</li><li>Ampli horari setmanal d'immersió en llengua anglesa</li></ul>
                </div>
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul id="mce_18" class=""><li class="template-component-vertical-grid-line-1n"><div>Cicle Inicial <br>1r (Cirerers i Pomers)<br>2n (Magraners i Castanyers)</div><div>6-8 anys</div></li><li class="template-component-vertical-grid-line-2n"><div>Cicle Mitjà <br>3r (Noguers i Ametllers)<br>4t (Til.lers i Oliveres)</div><div>8-10 anys</div></li><li class="template-component-vertical-grid-line-1n"><div>Cicle Superior <br>5è (Pins i Avets)<br>6è (Alzines i Roures)</div><div>10-12 anys</div></li></ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">
        <!-- Main -->
        <div class="template-main">
            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix" id="mce_22"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-clock-alt"><br></div><h5 id="mce_23">Horaris</h5><p id="mce_24">de 9 a 13h i de 15 a 17h de la tarda</p></li><li class="template-layout-column-center"><div class="template-icon-feature template-icon-feature-name-salver-alt"><br></div><h5 id="mce_25">Acollida</h5><p id="mce_26">de 7 a 9h, de 13 a 13'30h i de 17 a 19h</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-people-alt"><br></div><h5 id="mce_27">Famílies</h5><p id="mce_28">volem un tracte familiar i proper amb les nostres famílies</p></li></ul>
            </div>
            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix" id="mce_29"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-keyboard-alt"><br></div><h5 id="mce_30">Extraescolars</h5><p id="mce_31">activitats lúdiques, esportives i culturals</p></li><li class="template-layout-column-center"><div class="template-icon-feature template-icon-feature-name-flag-alt"><br></div><h5 id="mce_32">Activitats</h5><p id="mce_33">teatre, sciences lab, robòtica i natació</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-cutlery-alt"><br></div><h5 id="mce_34">Menjador</h5><p id="mce_35">serveis de menjador i espai migdia</p></li></ul>
            </div>
        </div>
    </div>
    <!-- Section -->
    <div class="template-content-section template-background-image template-background-image-4">
        <div class="template-main">
            <!-- Testimonials -->
            <div class="template-section-white">
                <div class="template-component-testimonial template-component-testimonial-style-2">
                    <ul class="template-layout-100" id="mce_39"><li class="template-layout-column-left"><i></i><p id="mce_40"><br>Som una escola amb voluntat d'innovació pedagògica que aposta de forma clara per incorporar les llengües estrangeres, l'educació emocional i l'estimulació de les habilitats artístiques, de forma transversal a les diverses etapes educatives</p><div><br></div><span id="mce_41"></span></li><li class="template-layout-column-left"><i></i><p id="mce_42"><br>A Primària eduquem el nostre alumnat per ser, per conèixer, per fer i per conviure</p><div><br></div><span id="mce_43"></span></li><li class="template-layout-column-left"><i></i><p id="mce_44"><br>La metedologia que utilitzem és global i interdisciplinar. L'aprenentatge es crea des de les àrees, el treball per projectes i els ambients d´aprenentatge</p><div><br></div><span id="mce_45"></span></li><li class="template-layout-column-left"><i></i><p id="mce_46"><br>La llengua anglesa es treballa com a primera llengua estrangera des de la Llar d’Infants. A partir de 1r de Primària, és també la llengua d’aprenentatge de l’àrea de Ciències Naturals (Science), introduint així l’aprenentatge integral de continguts en llengua estrangera</p><div><br></div><span id="mce_47"></span></li></ul><div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="template-content-section template-clear-fix template-margin-top-reset template-background-color-2">
        <!-- Section -->
        <div data-id="section-3">
            <div class="template-main">
                
                
                <!-- Layout 50x50 -->
                [_grafico]                
            </div>
        </div>
    </div>


    <!-- Section -->
    <div class="template-content-section template-main template-padding-bottom-5">
        <!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <h2 id="mce_48" class="">L'equip de Primària</h2>
            <h6 id="mce_49" class="">Tenim professorat especialitat en: educació musical, llengua estrangera, informàtica, robòtica, educació física, visual i plàstica</h6>
            <div></div>
        </div>
        <!-- Team -->
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix" id="mce_50">
                <li class="template-layout-column-left">
                    <ul class="template-layout-50x50 template-clear-fix" id="mce_51">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="[base_url]img/_sample/525x560/solano.jpg" data-fancybox-group="team-2" id="mce_52" data-mce-href="[base_url]img/_sample/525x560/11.jpg">
                                    <img src="[base_url]img/_sample/525x560/solano.jpg" alt="">
                                    <span id="mce_53" class="">
                                        <span id="mce_54" class="">
                                            <span id="mce_55" class="">
                                                <br data-mce-bogus="1">
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                <div>
                                    <h6 id="mce_56">Judit Solano</h6>
                                    <span id="mce_57">Professora d'anglès</span>
                                </div>
                                <p id="mce_58"><b>Judit Solano</b> Professora d'anglès</p>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote">
                                <br>
                            </div>
                            <p class="template-component-team-description" id="mce_59">
                                Com escola multilingüe que som al Monalco treballem per aconseguir un projecte estructurat i coherent i un assoliment de les llengües estrangeres profitós, motivador i eficaç.</p><div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix"><ul id="mce_60"><!-- <li><a href="#" class="template-component-social-icon-mail" id="mce_61" data-mce-href="#"></a></li> -->
                            </ul>
                        
                            </div>					
                        </li>
                    </ul>			
                </li>
                <li class="template-layout-column-right">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/morist.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/morist.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Gemma Morist</h6>
                                    <span>Coordinadora CI</span>
                                </div>
                                <p><b>Gemma Morist</b>Coordinadora CI</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">Fomentem persones responsables i compromeses amb la societat. Obertes a la creativitat i innovació.</p>                                                        
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- Header and subheader -->
    <div class="template-component-header-subheader">
        <h2 id="mce_73" class="">El Blog de Primària</h2>
        <h6 id="mce_74" class="">T'informem de les activitats que es realitzen a Primària</h6>
        <div></div>
    </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
        <div class="template-main">
            <!-- Recen post -->
            <div class="template-component-recent-post template-component-recent-post-style-1">
                <ul class="template-layout-33x33x33 template-clear-fix" id="mce_75">
                    [foreach:blog]
                    <li class="template-layout-column-[posicion]" style="visibility: visible;" data-mce-style="visibility: visible;">
                        <div class="template-component-recent-post-date">[fecha]</div>
                        <div class="template-component-image">
                            <a href="[link]" id="mce_266" class="" data-mce-href="[link]"> 
                                <img src="[foto]" alt=""> 
                                <span id="mce_267" class="">
                                <span id="mce_268" class="">
                                <span id="mce_269" class=""> 
                                </span></span></span> 
                            </a>
                        </div><h5 id="mce_270" class=""><a href="[link]" id="mce_271" data-mce-href="#" class="">[titulo]</a></h5><p id="mce_272" class="">[descripcion]</p><ul class="template-component-recent-post-meta template-clear-fix" id="mce_76"><li class="template-icon-blog template-icon-blog-author"><a href="[link]" id="mce_273" class="" data-mce-href="[link]">[user]</a></li><li class="template-icon-blog template-icon-blog-category"><a href="[link]" id="mce_274" class="" data-mce-href="[link]">Events</a>, <a href="[link]" id="mce_275" class="" data-mce-href="[link]">Fun</a></li></ul></li>[/foreach]</ul>
            </div>
        </div>
    </div>
</div>