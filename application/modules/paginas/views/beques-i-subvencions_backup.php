<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Secretaria</h1>
                <h6>Tot el paperam descarregable</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main ">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/menjadoricon.png') ?>" style="width:120px"></center>
                <h2>Beques i Subvencions</h2>                
                <div></div>
            </div>

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Beques</h2>
                <h6>Ajudes de la Generalitat de Catalunya</h6>
                <div></div>
            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">
                    <img src="<?= base_url() ?>img/_sample/525x531/1.png" alt=""/>
                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <h4>Ajuts per a alumnes amb necessitat específica de suport educatiu</h4>

                    <p>Ajuts per a alumnes escolaritzats en centres educatius espanyols que acreditin la necessitat específica de rebre suport educatiu i reuneixin els requisits establerts.</p>

                    <!-- Vertical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Termini per a presentar la sol·licitud</div>
                                <div>del 13 d'agost al 28 de setembre de 2017</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Nombre de beques disponibles per any</div>
                                <div>50.000</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Documentació necessària</div>
                                <div>link</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Miscellaneous Fee</div>
                                <div>$220.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div><b>Total:</b><br/>(inc. VAT)</div>
                                <div><b>$2,270.00</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Sign Up Today<i></i></a>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main ">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <h4>Nursery Fees Structure</h4>

                    <p>Morbi congue nunc sed congue vehicula phasellus erat ligula sedis fringilla sed congue id fermentum at neque nam posuere.</p>

                    <!-- Verical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>School Fees (Per Term)</div>
                                <div>$1,650.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Registration Fee (One-Time Payment)</div>
                                <div>$50.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Development Fee</div>
                                <div>$70.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Miscellaneous Fee</div>
                                <div>$180.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div><b>Total:</b><br/>(inc. VAT)</div>
                                <div><b>$1,950.00</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Sign Up Today<i></i></a>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">
                    <img src="<?= base_url() ?>img/_sample/525x531/2.png" alt=""/>
                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-background-image template-background-image-1">

        <!-- Main -->
        <div class="template-main">

            <!-- White section -->
            <div class="template-section-white">

                <!-- Header and subheader -->
                <div class="template-component-header-subheader">
                    <h2>Existing Students</h2>
                    <h6>We offer a quality children care and friendly atmosphere</h6>
                    <div></div>
                </div>

            </div>

            <!-- Pricing plans -->
            <div class="template-component-pricing-plan template-component-pricing-plan-style-1">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-component-pricing-plan-price">
                            <span>$10</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Morbi Etos Potenti
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Vestibulum convallis ipsum mi laoreet elementum ante.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                        </div>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-component-pricing-plan-price">
                            <span>$25</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Libero Augue Porta
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Praesent euismod massa quam euismod imperdiet velit.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                        </div>		
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-component-pricing-plan-price">
                            <span>$35</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Curabitur Elementum
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Praesent eros urna feugiat non maximus vitae id liberou.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>

    </div>

</div>