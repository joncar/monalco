<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Secretaria</h1>
                <h6>Tot el paperam descarregable</h6>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="template-content">
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
        <div class="template-component-header-subheader">
            <center><img src="<?= base_url('img/icon/llibres.png') ?>" style="width:120px"></center>
            <h2>Llibres de text</h2>
            <div></div>
        </div>
        <!-- Header and subheader -->        
        <div class="template-main ">
            <h4 style="text-align:center">PRIMÀRIA</h4>
            <div class="template-component-tab">
                <!-- Navigation -->
                <ul>
                    <?php foreach($this->db->get_where('cursos_niveles',array('tipo'=>1))->result() as $c): ?>
                        <li>
                            <a href="#template-tab-<?= $c->id ?>"><?= $c->curso ?></a>
                            <span></span>
                        </li>
                    <?php endforeach ?>
                </ul>
                <?php foreach($this->db->get_where('cursos_niveles',array('tipo'=>1))->result() as $c): ?>
                    <!-- Tab #1 -->
                    <div id="template-tab-<?= $c->id ?>">
                        
                        
                        
                        <!-- Layout 50x50 -->
                        <h4><?= $c->curso ?> de Primària</h4>
                                <p></p>
                                <!-- Vertical grid -->
                                <div class="template-component-vertical-grid template-margin-top-3">

                                    <table class="preutable" style="margin-bottom: 50px;">
                                        <tr>
                                            <th style="text-align: left">Llibres</th>
                                            <th style="text-align: left">Editorial</th>
                                            <th style="text-align: left">ISBN</th>
                                            <th style="text-align: left"></th>
                                        </tr>
                                        <?php foreach($this->db->get_where('libros',array('cursos_niveles_id'=>$c->id))->result() as $n=>$l): ?>
                                            <tr <?= $n%2==0?'class="odd"':'' ?>>
                                                <td><?= $l->libro ?></td>
                                                <td><?= $l->editorial ?></td>
                                                <td><?= $l->isbn ?></td>
                                                <td><?= $l->reciclado==0?'':'Reciclat' ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    
                                    </table>                                    
                                </div>
                                <!-- Button -->
                                
                    </div>
                <?php endforeach ?> 
            </div>
        </div>


    
    <!-- Header and subheader -->        
        <div class="template-main ">
            <h4 style="text-align:center">SECUNDÀRIA</h4>
            <div class="template-component-tab">
                <!-- Navigation -->
                <ul>
                    <?php foreach($this->db->get_where('cursos_niveles',array('tipo'=>2))->result() as $c): ?>
                        <li>
                            <a href="#template-tab-<?= $c->id ?>"><?= $c->curso ?></a>
                            <span></span>
                        </li>
                    <?php endforeach ?>
                </ul>
                <?php foreach($this->db->get_where('cursos_niveles',array('tipo'=>2))->result() as $c): ?>
                    <!-- Tab #1 -->
                    <div id="template-tab-<?= $c->id ?>">
                        <h4><?= $c->curso ?></h4>
                                <p></p>
                                <!-- Vertical grid -->
                                <div class="template-component-vertical-grid template-margin-top-3">

                                    <table class="preutable" style="margin-bottom: 50px;">
                                        <tr>
                                            <th style="text-align: left">Llibres</th>
                                            <th style="text-align: left">Editorial</th>
                                            <th style="text-align: left">ISBN</th>
                                            <th style="text-align: left"></th>
                                        </tr>
                                        <?php foreach($this->db->get_where('libros',array('cursos_niveles_id'=>$c->id))->result() as $n=>$l): ?>
                                            <tr <?= $n%2==0?'class="odd"':'' ?>>
                                                <td><?= $l->libro ?></td>
                                                <td><?= $l->editorial ?></td>
                                                <td><?= $l->isbn ?></td>
                                                <td><?= $l->reciclado==0?'':'Reciclat' ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    
                                    </table>                                    
                                </div>
                                <!-- Button -->                                
                    </div>
                <?php endforeach ?> 
            </div>
        </div>


    </div>
    
</div>