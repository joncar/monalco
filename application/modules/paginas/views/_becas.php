<table class="table">
	<?php foreach($documentos->result() as $d): ?>
        <tr>
            <th><?= $d->nombre ?></th>
            <td style="text-align: center; width:14%"><a href="<?= base_url('files/documentos/'.$d->fichero) ?>" target="_new"><img src="<?= base_url('img/pdf.png') ?>" style="width:30px; display: inline-block;"><br/><span style="font-size:12px; text-decoration: underline;">Descarregar PDF</span></a></td>
        </tr>
    <?php endforeach ?>
</table>  