<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Proposta Educativa</h1>
                <h6>Completa des dels 4 mesos fins els 16 anys</h6>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="template-content">
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
            <center><img src="<?= base_url('img/icon/sec.png') ?>" style="width:120px"></center>
            <h2>Secundària</h2>
            <div></div>
        </div>
        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">
            <!-- Left column -->
            <div class="template-layout-column-left">
                <!-- Header -->
                <!-- <h4>Formem persones</h4> -->
                <b>L'etapa d'Educació Secundària Obligatòria, va dels 12 als 16 anys. 
                </b>
                <br>               
                <p>És l'etapa on els alumnes desenvolupen la major part de les habilitats que els permetran ser autònoms i reflexius en el seu aprenentatge. L’adolescent troba en aquesta etapa el seu propi camí. Aprèn a decidir i a especialitzar-se. És necessari consolidar la personalitat i crear unes bases fermes per al futur.
                    <br>Acompanyem els nois i noies en el creixement cap a la vida adulta i en aquest sentit, treballem especialment amb els nostres alumnes la gestió de les emocions i l'autoconeixement.
                    <br>Els guiem per saber, saber fer i saber estar.
                    <br>Integrem els alumnes en projectes d’aprenentatge-serveis a segon cicle d’ESO.
                    <br>Són claus en aquesta etapa unes bones habilitats i hàbits de treball i d'estudi.
                    <br>El tutor/a té una tasca fonamental, la de guiar i acompanyar l’alumne en la tria de decisions coherents amb el futur professional i acadèmic desitjat. A través de la investigació, l'alumne assoleix tota una sèrie de coneixements que li seran claus en un futur.
                    <br>L'assoliment d'un excel·lent nivell de preparació humana i acadèmica per afrontar amb garanties uns estudis superiors és un dels objectius en aquesta etapa.
                    <br>Els nostres alumnes obtenen excel·lents resultats en les proves Competències Bàsiques del Departament d'ensenyament.
                </p>                
            </div>
            <!-- Right column -->
            <div class="template-layout-column-right">
                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/sec1.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/sec1.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/ESO1.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/ESO1.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/eso2.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/eso2.jpg" alt=""/>
                    </div>
                </div>
            </div>
        </div>




        <div class="template-layout-50x50 template-clear-fix template-margin-top-reset">
            <!-- Left column -->
            <div class="template-layout-column-left">                
                <!-- List -->
                <div class="template-component-list template-component-list-style-1">
                    <ul>
                        <li>Introduïm una àrea curricular en anglès (Educacií Física "Physical Education")</li>
                        <li>Manifestar autonomia i iniciativa personal en el camp de l'aprenentatge, en les relacions interpersonals i d'actuació social</li>
                        <li>Mostrar un comportament obert, democràtic, participatiu</li>
                        <li>Tenir assolits uns valors i hàbits que s’han donat al llarg de tota l'escolaritat</li>
                        <li>Manifestar una visió equilibrada de si mateix</li>
                        <li>Respectar el procés maduratiu i la capacitat de l'esforç</li>
                        <li>Potenciar l'autonomia de l'alumne i els hàbits de treball i estudi</li>
                        <li>Mostrar interès i respecte per l'entorn immediat i pels altres</li>
                        <li>Observar i escoltar de manera curiosa, i atenta</li>                        
                    </ul>
                </div>                
            </div>
            <!-- Right column -->
            <div class="template-layout-column-right">
                <!-- List -->
                <div class="template-component-list template-component-list-style-1">
                    <ul>                        
                        <li>Assessorament individualitzat per cada alumne i les seves famílies</li>
                        <li>Comprendre i expressar correctament missatges orals i escrits en català, castellà i anglès</li>
                        <li>Obtenir i tractar informació per a una finalitat immediata i presentar-la als altres de manera útil i entenedora</li>
                        <li>Seguir un procés de raonament lògic. Conèixer que aquest procés és el propi de la ciència</li>
                        <li>Introduir-se en el coneixement de l’entorn tecnològic</li>
                        <li>Assolir un excel·lent nivell de preparació humana i acadèmica per afrontar amb garanties uns estudis superiors</li>
                        <li>Donar eines per encaminar l'orientació a l'ensenyament postobligatori</li>
                        <li>Introduïm una 2a llengua estrangera en el currículum. L’alumne pot escollir entre francès o alemany com a segona llengua estrangera al llarg de tota la Secundària</li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">
        <!-- Main -->
        <div class="template-main">
            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                        <h5>Horaris</h5>
                        <p>de 8 a 13'30h / tardes dilluns, dimarts i dijous de 15 a 17h</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pencil-alt"></div>
                        <h5>Tallers</h5>
                        <p>tallers intercicles</p>
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-pin-alt"></div>
                        <h5>Opcions</h5>
                        <p>àmplia oferta lingüística, intercanvis a l'estranger...</p>
                    </li>
                </ul>
            </div>
            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-toy-alt"></div>
                        <h5>Transport</h5>
                        <p>transport escolar privat</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-flag-alt"></div>
                        <h5>Activitats</h5>
                        <p>colònies, viatge fi de curs, estades a l'estranger...</p>
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-cutlery-alt"></div>
                        <h5>Menjador</h5>
                        <p>servei de menjador escolar i espai migdia</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Section -->
    <div class="template-content-section template-background-image template-background-image-4">
        <div class="template-main">
            <!-- Testimonials -->
            <div class="template-section-white">
                <div class="template-component-testimonial template-component-testimonial-style-2">
                    <ul class="template-layout-100" id="mce_39"><li class="template-layout-column-left"><i></i><p id="mce_40"><br>Volem fomentar la iniciativa, l'esperit crític, la creativitat i el gust per aprendre</p><div><br></div><span id="mce_41"></span></li><li class="template-layout-column-left"><i></i><p id="mce_42"><br>Un model educatiu participatiu, centrat en l'alumne, facilitador en la innovació i perseguint sempre l'excel·lència</p><div><br></div><span id="mce_43"></span></li><li class="template-layout-column-left"><i></i><p id="mce_44"><br>Fomentem el plurilingüisme i apostem per la innovació</p><div><br></div><span id="mce_45"></span></li><li class="template-layout-column-left"><i></i><p id="mce_46"><br>Apostem per la tecnologia com una eina més de l'aprenentatge</p><div><br></div><span id="mce_47"></span></li></ul><div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="template-content-section template-clear-fix template-margin-top-reset template-background-color-2">
        <!-- Section -->
        <div data-id="section-3">
            <div class="template-main">
                
                
                <!-- Layout 50x50 -->
                [_grafico2]                
            </div>
        </div>
    </div>


    <!-- Section -->
    <div class="template-content-section template-main template-padding-bottom-5">
        <!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <h2>L'equip de Secundària</h2>
            <h6>Comptem amb un bon equip humà de professionals ben cohesionats i que treballen en equip al servei dels nostres alumnes i les nostres famílies</h6>
            <div></div>
        </div>
        <!-- Team -->
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix">
                <li class="template-layout-column-left">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/termes.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/termes.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Anna Termes</h6>
                                    <span>Exalumne secundària</span>
                                </div>
                                <p><b>Anna Terms</b>Exalumne secundària</p>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">Porto 15 al Monalco. Una escola que per mi és com una família, plena de valors i companyerisme. On sempre m'he sentit recolzada per tot el professorat</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                   <!--  <li><a href="#" class="template-component-social-icon-mail"></a></li> -->
                                    <!--
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                    -->
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="template-layout-column-right">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/navarro.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/navarro.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Manel Navarro</h6>
                                    <span>Sotsdirector i Professor</span>
                                </div>
                                <p><b>Manel Navarro</b>Sotsdirector i Professor</p>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">Porto més de 20 anys sent professor del Monalco i és un plaer veure com els alumnes tenen tant de poder d'autosuperació. Estem contents de la nostra feina quan veiem els fruits dels nostres ex alumnes.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!-- <li><a href="#" class="template-component-social-icon-mail"></a></li> -->
                                    <!--
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                    -->
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
        <div class="template-main">
            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>El Blog de Secundària</h2>
                <h6>T'informem de les activitats que es realitzen a Secundària</h6>
                <div></div>
            </div>
            <!-- Recen post -->
            <div class="template-component-recent-post template-component-recent-post-style-1">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    [foreach:blog]
                    <li class="template-layout-column-[posicion]" style="visibility: visible;">
                        <div class="template-component-recent-post-date">[fecha]</div>
                        <div class="template-component-image">
                            <a href="[link]" id="mce_266" class="">
                                <img src="[foto]" alt="">
                                <span id="mce_267" class=""><span id="mce_268">
                                    <span id="mce_269">
                                        
                                    </span>
                                </span>
                            </span>
                        </a>
                        
                    </div>
                    <h5 id="mce_270" class="">
                    <a href="[link]" id="mce_271" data-mce-href="#" class="">
                        [titulo]
                    </a>
                    </h5>
                    <p id="mce_272" class="">
                        [descripcion]
                    </p>
                    <ul class="template-component-recent-post-meta template-clear-fix">
                        <li class="template-icon-blog template-icon-blog-author">
                            <a href="[link]" id="mce_273" class="">[user]</a>
                        </li>
                        <li class="template-icon-blog template-icon-blog-category">
                            <a href="[link]" id="mce_274" class="">Events</a>,
                            <a href="[link]" id="mce_275" class="">Fun</a>
                        </li>
                    </ul>
                </li>
                [/foreach]
            </ul>
        </div>
    </div>
</div>
<!-- Section -->
</div>