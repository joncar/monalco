<?php 
      $query = "select * FROM calificaciones INNER JOIN asignaturas ON asignaturas.id = calificaciones.asignaturas_id WHERE asignaturas.curso = '2'";
      $asignaturas = $this->db->query($query.' group by asignaturas.nombre ORDER BY asignaturas.nombre, calificaciones.anyo');
      $anyos = $this->db->query($query.' GROUP BY anyo ORDER BY asignaturas.nombre, calificaciones.anyo');    
      $anyo = $this->db->query("select MAX(calificaciones.anyo) as anyo FROM calificaciones INNER JOIN asignaturas ON asignaturas.id = calificaciones.asignaturas_id WHERE asignaturas.curso = '2'")->row()->anyo;
      $grafico = $this->db->query("select * FROM calificaciones INNER JOIN asignaturas ON asignaturas.id = calificaciones.asignaturas_id WHERE anyo = '".$anyo."' AND asignaturas.curso = '2' group by asignaturas.nombre ORDER BY asignaturas.nombre, calificaciones.anyo ");
      $this->db->limit(4);
      $encuestas = $this->db->get_where('encuestas',array('curso'=>2));
      foreach($encuestas->result() as $n=>$v){
        $encuestas->row($n)->valores = $this->db->get_where('encuestas_detalles',array('encuestas_id'=>$v->id));
      }
    ?>


<div class="template-component-counter-box">
    <center>
        <h3>Enquestes</h3>
        <div class="template-component-divider template-component-divider-style-2"></div>
        <div class="template-component-italic template-margin-top-3">
            Resultats enquesta satisfacció Monalco
        </div>
    </center>
</div>

<div class="template-layout-25x25x25x25 template-clear-fix graficos">
  <div class="template-layout-column-left">
    <div id="donut0"></div>
    <div class="template-component-italic" id="donut0label" style="margin-top: -40px; font-size: 24px;"></div>
  </div>
  <div class="template-layout-column-center-left">
    <div id="donut1"></div>
    <div class="template-component-italic" id="donut1label" style="margin-top: -40px; font-size: 24px;"></div>
  </div>
  <div class="template-layout-column-center-right">
    <div id="donut2"></div>
    <div class="template-component-italic" id="donut2label" style="margin-top: -40px; font-size: 24px;"></div>
  </div>
  <div class="template-layout-column-right">
    <div id="donut3"></div>
    <div class="template-component-italic" id="donut3label" style="margin-top: -40px; font-size: 24px;"></div>
  </div>
</div>

<script>
    /*var $arrColors = [
    <?php foreach($grafico->result() as $g): ?>
    '<?= $g->color ?>',
    <?php endforeach ?>
    '#fffff',
    <?php foreach($grafico->result() as $g): ?>
    '<?= $g->color ?>',
    <?php endforeach ?>
    ];
    Morris.Bar({
      element: 'myfirstchart',
      data: [
        <?php foreach($grafico->result() as $g): ?>
          { y: '', a: <?= $g->monalco ?>,n:'<?= $g->nombre ?>',l:'Monalco'},          
      <?php endforeach ?>
        { y: '', a: 0,l:''},
      <?php foreach($grafico->result() as $g): ?>
          { y: '', a: <?= $g->catalunya ?>,n:'<?= $g->nombre ?>',l:'Catalunya'},          
      <?php endforeach ?>
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Monalco','Catalunya'],
      barColors: function (row, series, type) {
        return $arrColors[row.x];
      }, 
      hoverCallback: function (index, options, content, row) {                  
      return row.n;
    }     
    });*/

    
    <?php if($encuestas->num_rows()>0): ?>
      <?php foreach($encuestas->result() as $n=>$e): ?>
        Morris.Donut({
          element: 'donut<?= $n ?>',
          data: [
            <?php foreach($e->valores->result() as $v): ?>
            {label: "<?= $v->titulo ?>", value: <?= $v->valor ?>},
        <?php endforeach ?>
        ],
        colors:[
          <?php foreach($e->valores->result() as $v): ?>
            '<?= $v->color ?>',
          <?php endforeach ?>
        ],
        formatter:function(y,data){
          return y+'%';
        }
        });

        $("#donut<?= $n ?>label").html("<center><?= $e->titulo ?></center>");
    <?php endforeach ?>
  <?php endif ?>    
</script>