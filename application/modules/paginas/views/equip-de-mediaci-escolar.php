
            
            <!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_13" class="">Serveis</h1>
                <h6 id="mce_14" class="">Tot allò que t'oferim</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
      <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/mediacioicon.png') ?>" style="width:120px"></center>
                <h2>Mediació</h2>                
                <div></div>
                 <p class="template-margin-top-3" id="mce_17">
                  La mediació és un procés educatiu per resoldre els conflictes de convivència a través del diàleg i en presència d’un mediador.<br></span>És voluntària,&nbsp;confidencial i els protagonistes del conflicte prenen les seves pròpies decisions lliurement i de manera responsable.
            </p>
            </div>
       

        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4 id="mce_17" class="">Mediador</h4>

                <p id="mce_18" class="">Intenta donar les eines suficients per redreçar situacions de conflicte. Accepta les diferències, no intenta canviar les persones ni les seves idees. Només contribueix a evitar enfrontament i crear consens.
</p>

                <!-- List -->
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul id="mce_19" class=""><li>ACULL les persones en conflicte</li><li>ESCOLTA activament i ajuda a transformar el problema</li><li>Promou la COMPRENSIÓ mútua</li><li>NO jutja</li><li>NO sanciona</li></ul>
                </div>

                <!-- Vertical grid -->
                <!-- 
<div class="template-component-vertical-grid template-margin-top-3">
                    <ul id="mce_20" class=""><li class="template-component-vertical-grid-line-1n"><div>Preu de 8:00 a 9:00:</div><div>3,50€</div></li><li class="template-component-vertical-grid-line-2n"><div>Preu de 8:30 a 9:00:</div><div>2€</div></li><li class="template-component-vertical-grid-line-1n"><div>Preu fixe mensual de 8h a 9h:</div><div>48€</div></li><li class="template-component-vertical-grid-line-2n"><div>Preu fixe mensual de 8:30h a 9h:</div><div>25€</div></li></ul>
                </div>
 -->

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="[base_url]img/_sample/690x506/med2.jpg" data-thumb="[base_url]img/_sample/690x506/med2.jpg" alt="">
                        <img src="[base_url]img/_sample/690x506/parlem.jpg" data-thumb="[base_url]img/_sample/690x506/parlem.jpg" alt="">
                        <img src="[base_url]img/_sample/690x506/med3.jpg" data-thumb="[base_url]img/_sample/690x506/med3.jpg" alt="">
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-section-style-1 template-clear-fix">
				<div class="template-main">
<!-- Feature -->
<div class="template-component-feature template-component-feature-style-1 template-component-feature-position-top template-component-feature-size-medium" style="display: block;">
	<ul class="template-layout-25x25x25x25 template-clear-fix" id="mce_189"><li class="template-layout-column-left" style="visibility: visible;" data-mce-style="visibility: visible;"><div class="template-icon-feature template-icon-feature-name-bell-alt template-icon-feature-size-medium"><br></div><h5 id="mce_204" class="">Estratègia</h5><p id="mce_205" class="">Serveix per afavorir la convivència. S'intenta donar eines per redreçar situacions de conflicte</p></li><li class="template-layout-column-center-left" style="visibility: visible;" data-mce-style="visibility: visible;"><div class="template-icon-feature template-icon-feature-name-blocks-alt template-icon-feature-size-medium"><br></div><h5 id="mce_206" class="">Bases</h5><p id="mce_207" class="">El diàleg, la cooperació i la responsabilitat de cada persona en mantenir un clima pacífic</p></li><li class="template-layout-column-center-right" style="visibility: visible;" data-mce-style="visibility: visible;"><div class="template-icon-feature template-icon-feature-name-book-alt template-icon-feature-size-medium"><br></div><h5 id="mce_208" class="">Mediador</h5><p id="mce_209" class="">És imparcial i ajuda els participants a assolir l'acord pertinent sense imposar cap solució ni mesura concreta</p></li><li class="template-layout-column-right" style="visibility: visible;" data-mce-style="visibility: visible;"><div class="template-icon-feature template-icon-feature-name-briefcase-alt template-icon-feature-size-medium"><br></div><h5 id="mce_210" class="">Jo i tu</h5><p id="mce_211" class="">Parteix del fet que quan es produeix un conflicte no es tracta de "jo", "tu", sinó de "nosaltres"</p></li></ul>
</div>	
				</div>
			</div>

    <!-- Section -->
    

    <!-- Section -->
    <div class="template-content-section template-main template-padding-top-reset template-padding-bottom-5">

        <!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <br><h2 id="mce_97" class="">Equip de mediació</h2>
            <h6 id="mce_98" class="">Els nostres mediadors són professionals que estan a la vostra disposició per trobar una solució</h6>
            <div></div>
        </div>

        <!-- Team -->
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix" id="mce_99"><li class="template-layout-column-left"><ul class="template-layout-50x50 template-clear-fix" id="mce_100"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/525x560/arbos.jpg" data-fancybox-group="team-2" id="mce_101"> <img src="[base_url]img/_sample/525x560/arbos.jpg" alt=""> <span id="mce_102" class=""><span id="mce_103" class=""><span id="mce_104" class=""><br></span></span></span> </a><div><h6 id="mce_105">Rosa Arbós</h6><span id="mce_106">Professora i Mediadora escolar</span></div><p id="mce_107"><b>Rosa Arbós</b>Professora i Mediadora escolar</p></div></li><li class="template-layout-column-right"><div class="template-component-team-quote"><br></div><p class="template-component-team-description" id="mce_108">Al Monalco volem que tots els alumnes vinguin contents. 
Així el seu aprenentatge serà també més profitós.
Per aixó volem crear un clima de convivència a través del diàleg.</p><!-- <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix"><ul id="mce_109"><li><a href="#" class="template-component-social-icon-mail" id="mce_110"></a></li><li><a href="#" class="template-component-social-icon-facebook" id="mce_111"></a></li><li><a href="#" class="template-component-social-icon-twitter" id="mce_112"></a></li></ul><br></div> --></li></ul></li><li class="template-layout-column-right"><ul class="template-layout-50x50 template-clear-fix" id="mce_113"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/525x560/pol.jpg" data-fancybox-group="team-2" id="mce_114"> <img src="[base_url]img/_sample/525x560/pol.jpg" alt=""> <span id="mce_115" class=""><span id="mce_116"><span id="mce_117"></span></span></span> </a><div><h6 id="mce_118">Pol Jumilla</h6><span id="mce_119">Alumne de Secundària</span></div><p id="mce_120"><b>Pol Jumilla</b>Alumne de Secundària</p></div></li><li class="template-layout-column-right"><div class="template-component-team-quote"><br></div><p class="template-component-team-description" id="mce_121">Quan qualsevol alumne té un problema pot anar al servei de mediació. Els professors sempre ens ajuden i ens aconsellen. Ens sentim recolzats.</p><!-- <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix"><ul id="mce_122"><li><a href="#" class="template-component-social-icon-mail" id="mce_123"></a></li><li><a href="#" class="template-component-social-icon-facebook" id="mce_124"><br></a></li><li><a href="#" class="template-component-social-icon-twitter" id="mce_125"><br></a></li></ul> --></div></li></ul></li></ul>
        </div>

    </div>

    <!-- Section -->
    <!-- 
<div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">
        <div class="template-main">

            <!~~ Call to action ~~>
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!~~ Content ~~>
                <div class="template-component-call-to-action-content">

                    <!~~ Left column ~~>
                    <div class="template-component-call-to-action-content-left">

                        <!~~ Header ~~>
                        <h3 id="mce_129" class="">Vols contactar amb l'equip mediador?</h3>

                    </div>

                    <!~~ Right column ~~>
                    <div class="template-component-call-to-action-content-right">

                        <!~~ Button ~~>
                        <a href="#" class="template-component-button template-component-button-style-1" id="mce_130">Contacte<i></i></a>

                    </div>									

                </div>

            </div>				

   
 -->     </div>

    </div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div>

</div>        