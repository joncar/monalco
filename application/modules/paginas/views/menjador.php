<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Serveis</h1>
                <h6>Tot allò que t'oferim</h6>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="template-content">
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
        <div class="template-main">
            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/menjadoricon.png') ?>" style="width:120px"></center>
                <h2>Menjador</h2>
                <div></div>
            </div>
            <!-- Main -->
            <div class="template-component-recent-post template-component-recent-post-style-1">
                <div style="text-align: right">
                    <div class="template-widget-archive template-widget-archive-style-1">
                        <ul>
                            <?php
                            $this->db->order_by('orden','ASC');
                            $this->db->limit(2);
                            foreach($this->db->get_where('comedor_menu_descarga')->result() as $p):
                            ?>
                            <a class="descargarmenu" href="<?= base_url('files/menjador/'.$p->fichero) ?>" title="<?= $p->titulo ?>" target="_new"><?= $p->titulo ?> <img src="<?= base_url('img/pdf.png') ?>"><br><br></a> 
                            <?php endforeach ?>
                            
                            <br><br><div class="template-component-recent-post template-component-recent-post-style-1">
                                <div style="text-align: left">
                                    <div class="template-widget-archive template-widget-archive-style-1">
                                        <ul>
                                        
                                        
                                            <div>• En el nostre menjador escolar cuidem els menús. Menús equilibrats i supervisats per una nutricionista col·legiada. <br> • Fomentem una dieta equilibrada i variada. <br> • Comptem amb dietes especials (règim, al·lèrgies...) i menús adaptats a cada edat.</div>
                                        </ul>
                                    </div>
                                </div>
                                <ul class="template-layout-33x33x33 template-clear-fix">
                                    <li class="template-layout-column-left menjador" style="visibility: visible;">
                                        <div id="datetimepicker1-inline"></div>
                                    </li>
                                    <li class="template-layout-column-center menjador" style="visibility: visible;">
                                        <div id="datetimepicker2-inline"></div>
                                    </li>
                                    <li class="template-layout-column-right menjador" style="visibility: visible;">
                                        <div id="datetimepicker3-inline"></div>
                                    </li>
                                    <li class="template-layout-column-left menjador" style="visibility: visible;">
                                        <div id="datetimepicker4-inline"></div>
                                    </li>
                                    <li class="template-layout-column-center menjador" style="visibility: visible;">
                                        <div id="datetimepicker5-inline"></div>
                                    </li>
                                    <li class="template-layout-column-right menjador" style="visibility: visible;">
                                        <div id="datetimepicker6-inline"></div>
                                    </li>
                                    <li class="template-layout-column-left menjador" style="visibility: visible;">
                                        <div id="datetimepicker7-inline"></div>
                                    </li>
                                    <li class="template-layout-column-center menjador" style="visibility: visible;">
                                        <div id="datetimepicker8-inline"></div>
                                    </li>
                                    <li class="template-layout-column-right menjador" style="visibility: visible;">
                                        <div id="datetimepicker9-inline"></div>
                                    </li>
                                    <li class="template-layout-column-left menjador" style="visibility: visible;">
                                        <div id="datetimepicker10-inline"></div>
                                    </li>
                                    <li class="template-layout-column-center menjador" style="visibility: visible;">
                                        <div id="datetimepicker11-inline"></div>
                                    </li>
                                    <li class="template-layout-column-right menjador" style="visibility: visible;">
                                        <div id="datetimepicker12-inline"></div>
                                    </li>
                                </ul>
                            </div>
                            <!-- Button -->
                            <?php if($this->user->log): ?>
                            <a href="[base_url]quotes.html" class="template-component-button template-component-button-style-1">Quotes<i></i></a>
                            <?php else: ?>
                            <a href="javascript:quotes()" class="template-component-button template-component-button-style-1">Quotes<i></i></a>
                            <?php endif ?>
                            <div style="margin: 20px 0;">*Menús supervisats per Laboratoris COBAC SA.Dietista i Nutricionista Sra.Sepsi Aiguadé</div>
                        </div>
                    </div>
                </div>