<?php $cat = empty($_GET['cat'])?$galeria->row()->id:$_GET['cat']; ?>
<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom">

            <div class="template-header-bottom-background template-header-bottom-background-img-8 template-header-bottom-background-style-1">
                <div class="template-main">
                    <h1>Galeria fotogràfica</h1>
                    <h6>l'escola en imatges</h6>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">



    <div class="template-content-section template-content-layout template-content-layout-sidebar-left template-main template-clear-fix ">

                <!-- Sidebar -->
                <div class="template-content-layout-column-left">
                       
                       
                    <!-- Widget list -->
                    <ul class="template-widget-list">

                        <!-- Menu widget -->
                        <li>
                            <h6>
                                <span>Categories</span>
                                <span></span>
                            </h6>
                            <div>
                                <div class="template-widget-menu">
                                    <ul>
                                        <?php
                                        $gal =  $this->elements->categoria_fotos_privada();
                                        foreach($gal->result() as $g): ?>
                                        <li><a class="<?= $cat==$g->id?'active':'' ?>" href="<?= base_url('zona-privada') ?>?cat=<?= $g->id ?>" title="<?= $g->nombre ?>"><?= $g->nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </div>
                            </div>
                        </li>

                    </ul>               
                
                </div>

                <!-- Content -->
                <div class="template-content-layout-column-right">

                    <h4><?= @$this->elements->categoria_fotos_privada(array('categoria_fotos_privada.id'=>$cat))->row()->nombre; ?></h4>
                    <?php $fotos = $this->elements->galeria_privada(array('categoria_galeria_id'=>$cat)); ?>
                    <!-- Gallery -->
                    <div class="template-component-gallery template-margin-top-3 <?= $fotos->num_rows()>15?'galeriaZonaPrivada':'' ?>">
                        <ul class="template-layout-33x33x33 template-clear-fix">
                                                    

                            <?php $x = 0; ?>
                            <?php foreach($fotos->result() as $n=>$f): ?>
                            <?php 
                                switch($x){
                                    case 0: $posicion = 'left'; break;                                  
                                    case 1: $posicion = 'center'; break;
                                    case 2: $posicion = 'right'; break;
                                }
                                $x++; 
                                $x = $x>2?0:$x;
                            ?>
                                <!-- Left column -->
                                <li class="template-layout-column-<?= $posicion ?>">
                                    <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                        <a href="<?= $f->foto ?>" data-fancybox-group="gallery-1">
                                            <img src="<?= $f->foto ?>" alt="" />
                                            <span><span><span></span></span></span>
                                        </a>
                                        <div>
                                            <h6><?= @explode('|',$f->titulo)[0] ?></h6>
                                            <span><?= @explode('|',$f->titulo)[1] ?></span>
                                        </div>                                  
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>  
                    <?php if($fotos->num_rows()>15): ?>
                        <div style="text-align:center; margin:20px 0">
                            <a href="javascript:void(0)" onclick="$(this).parent().remove();$('.galeriaZonaPrivada').removeClass('galeriaZonaPrivada');" class="template-component-button template-component-button-style-1" >Veure totes</a>
                        </div>        
                    <?php endif ?>

                </div>

            </div>
    

    </div>

</div>