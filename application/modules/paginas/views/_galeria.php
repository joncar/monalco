
<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom">

            <div class="template-header-bottom-background template-header-bottom-background-img-8 template-header-bottom-background-style-1">
                <div class="template-main">
                    <h1>Galeria fotogràfica</h1>
                    <h6>l'escola en imatges</h6>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">

        <!-- Tabs -->
        <div class="template-component-tab">

            <!-- Navigation -->
            <ul>
            	<?php foreach($galeria->result() as $g): ?>
	                <li>
	                    <a href="#template-tab-<?= $g->id ?>"><?= $g->nombre ?></a>
	                    <span></span>
	                </li>
            	<?php endforeach ?>
            </ul>
            <?php foreach($galeria->result() as $g): ?>
	            <!-- Tab #1 -->
	            <div id="template-tab-<?= $g->id ?>">
	                <!-- Gallery -->
	                <div class="template-component-gallery">
	                    <!-- Layout 50x50 -->
	                    <ul class="template-layout-50x50 template-clear-fix">	                    	
	                    	<?php $x = 0; ?>
	                    	<?php foreach($g->fotos->result() as $f): ?>
	                    	<?php 
	                    		switch($x){
	                    			case 0: $posicion = 'left'; break;	                    			
	                    			case 1: $posicion = 'right'; break;
	                    		}
	                    		$x++; 
	                    		$x = $x>1?0:$x;
	                    	?>
	                        <!-- Left column -->
	                        <li class="template-layout-column-<?= $posicion ?>">
	                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
	                                <a href="<?= base_url('img/galeria/'.$f->foto) ?>" data-fancybox-group="gallery-1">
	                                    <img src="<?= base_url('img/galeria/'.$f->foto) ?>" alt="" />
	                                    <span><span><span></span></span></span>
	                                </a>
	                                <div>
	                                    <h6><?= @explode('|',$f->titulo)[0] ?></h6>
	                                    <span><?= @explode('|',$f->titulo)[1] ?></span>
	                                </div>	                                
	                            </div>
	                        </li>
	                    	<?php endforeach ?>
	                    </ul>
	                </div>	
	            </div>
        	<?php endforeach ?>

            </div>

        </div>				

    </div>

    <!-- Section -->
    <!-- 
<div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Call to action ~~>
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!~~ Content ~~>
                <div class="template-component-call-to-action-content">

                    <!~~ Left column ~~>
                    <div class="template-component-call-to-action-content-left">

                        <!~~ Header ~~>
                        <h3 style="font-size: 38px;">Vols informació de la nostra escola?</h3>

                    </div>

                    <!~~ Right column ~~>
                    <div class="template-component-call-to-action-content-right">

                        <!~~ Button ~~>
                        <a href="#" class="template-component-button template-component-button-style-1">Demanar informació<i></i></a>

                    </div>		
 -->							

                </div>

            </div>				

        </div>

    </div>

</div>