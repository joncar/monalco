<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    
<div class="template-header-top">

    <div class="template-main template-clear-fix">

        <div class="template-header-top-logo">
            <a href="[base_url]">
                <img src="[base_url]img/logo_header.png" alt="" />
            </a>
        </div>

        <div class="template-header-top-menu template-clear-fix">	

            <nav class="template-component-menu-default">

                <ul class="sf-menu template-clear-fix">

                    <li class="sf-mega-enable-0">
                        <a href="[base_url]"><span class="template-icon-menu template-icon-menu-home"></span>Inici</a>                        
                    </li>
                    <li class="sf-mega-enable-1">
                        <a href="#"><span class="template-icon-menu template-icon-menu-people"></span>L'escola</a>
                        <div class="sf-mega template-layout-25x25x25x25 template-clear-fix">
                            <div class="template-layout-column-left">
                                <span class="sf-mega-header">L'ESCOLA</span>
                                <ul>
                                    <li><a href="[base_url]historia.html">Història</a></li>
                                    <li><a href="[base_url]quotes.html">Quotes</a></li>
                                    <li><a href="[base_url]instalacions.html">Instal·lacions</a></li>
                                    <li><a href="[base_url]pec.html">PEC</a></li>
                                </ul>
                            </div>
                            <div class="template-layout-column-center-left">
                                <span class="sf-mega-header">PROPOSTA EDUCATIVA</span>
                                <ul>
                                    <li><a href="[base_url]llar-d-infants.html">Llar d'infants</a></li>
                                    <li><a href="[base_url]infantil.html">Infantil</a></li>
                                    <li><a href="[base_url]primaria.html">Primària</a></li>
                                    <li><a href="[base_url]secundaria.html">Secundària</a></li>
                                </ul>												
                            </div>
                            <div class="template-layout-column-center-right">
                                <span class="sf-mega-header">ORGANITZACIÓ</span>
                                <ul>
                                    <li><a href="[base_url]organigrama.html">Organigrama</a></li>
                                    <li><a href="[base_url]calendari-escolar.html">Calendari Escolar</a></li>
                                    <li><a href="[base_url]horaris.html">Horaris</a></li>
                                    <li><a href="[base_url]consell-escolar.html">Consell Escolar</a></li>
                                    <li><a href="[base_url]ampa.html">AMPA</a></li>
                                </ul>												
                            </div>

                            <div class="template-layout-column-right">
                                <span class="sf-mega-header">SECRETARIA</span>
                                <ul>
                                    <li><a href="[base_url]documents.html">Documents</a></li>
                                    <li><a href="[base_url]libres-de-text.html">Llibres de Text</a></li>
                                    <li><a href="[base_url]beques-i-subvencions.html">Beques i subvencions</a></li>
                                    <li><a href="[base_url]autorizacions.html">Autoritzacions</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-1">
                        <a href="#"><span class="template-icon-menu template-icon-menu-printer"></span>Activitats i serveis</a>
                        <div class="sf-mega template-layout-50x50 template-clear-fix">
                            <div class="template-layout-column-left">
                                <span class="sf-mega-header">ACTIVITATS</span>
                                <ul>
                                                                            <li><a href="[base_url]servicios/1-acollida-i-ludoteca.html">Acollida i ludoteca</a></li>
                                                                            <li><a href="[base_url]servicios/2-menjador.html">Menjador</a></li>
                                                                            <li><a href="[base_url]servicios/3-extraescolars.html">Extraescolars</a></li>
                                                                            <li><a href="[base_url]servicios/4-esports.html">Esports</a></li>
                                                                            <li><a href="[base_url]servicios/5-casal.html">Casal</a></li>
                                                                            <li><a href="[base_url]servicios/8-equip-psicopedag-gic.html">Equip psicopedagògic</a></li>
                                                                            <li><a href="[base_url]servicios/9-equip-de-mediaci--escolar.html">Equip de mediació escolar</a></li>
                                                                            <li><a href="[base_url]servicios/10-nataci-.html">Natació</a></li>
                                                                            <li><a href="[base_url]servicios/11-transport-escolar.html">Transport Escolar</a></li>
                                                                            <li><a href="[base_url]servicios/12-aula-de-la-calma.html">Aula de la calma</a></li>
                                                                            <li><a href="[base_url]servicios/13-xarxes.html">Xarxes</a></li>
                                                                    </ul>
                            </div>
                            <div class="template-layout-column-right">
                                <span class="sf-mega-header">DOCUMENTS PLA ANUAL</span>
                                <ul>
                                                                            <li><a href="[base_url]servicios/6-projectes-centre.html">Projectes centre</a></li>
                                                                            <li><a href="[base_url]servicios/7-projectes-tr-ms.html">Projectes Tr@ms</a></li>
                                                                    </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-1">
                        <a href="#"><span class="template-icon-menu template-icon-menu-projector"></span>Projectes</a>
                        <div class="sf-mega template-layout-50x50 template-clear-fix">
                            <div class="template-layout-column-left">
                                <span class="sf-mega-header">PROJECTES CENTRE</span>
                                <ul>
                                                                            <li><a href="[base_url]proyectos/1-escola-verda.html">Escola Verda</a></li>
                                                                            <li><a href="[base_url]proyectos/3-comenius.html">Comenius</a></li>
                                                                            <li><a href="[base_url]proyectos/4-pla-lector.html">Pla Lector</a></li>
                                                                            <li><a href="[base_url]proyectos/5-multiling-e.html">Multilingüe</a></li>
                                                                            <li><a href="[base_url]proyectos/6-pla-tac.html">Pla TAC</a></li>
                                                                            <li><a href="[base_url]proyectos/7-clickedu.html">ClickEdu</a></li>
                                                                            <li><a href="[base_url]proyectos/8-projecte-tots.html">Projecte TOTS</a></li>
                                                                            <li><a href="[base_url]proyectos/9-english-is-fun.html">English is Fun</a></li>
                                                                            <li><a href="[base_url]proyectos/10-ci-ncies-en-angl-s.html">Ciències en anglès</a></li>
                                                                            <li><a href="[base_url]proyectos/11-conviv-ncia.html">Convivència</a></li>
                                                                            <li><a href="[base_url]proyectos/12-educaci--emocional.html">Educació Emocional</a></li>
                                                                            <li><a href="[base_url]proyectos/13-escolta-m.html">Escolta'm</a></li>
                                                                            <li><a href="[base_url]proyectos/14-digui-digui-.html">Digui! Digui!</a></li>
                                                                            <li><a href="[base_url]proyectos/15-mediaci--escola.html">Mediació Escola</a></li>
                                                                            <li><a href="[base_url]proyectos/16-pati-espai-educatiu.html">Pati Espai Educatiu</a></li>
                                                                            <li><a href="[base_url]proyectos/17-ztra-buhing.html">Ztra Buhing</a></li>
                                                                            <li><a href="[base_url]proyectos/18-obertament.html">Obertament</a></li>
                                                                            <li><a href="[base_url]proyectos/19-aps-aprenentatge-i-servei-.html">APS (Aprenentatge i Servei)</a></li>
                                                                            <li><a href="[base_url]proyectos/20-emprenedoria-a-l-escola.html">Emprenedoria a l'Escola</a></li>
                                                                            <li><a href="[base_url]proyectos/21-sona-escola.html">Sona Escola</a></li>
                                                                            <li><a href="[base_url]proyectos/22-sarda-tic.html">Sarda Tic</a></li>
                                                                            <li><a href="[base_url]proyectos/23-ipads-tac.html">iPads TAC</a></li>
                                                                    </ul>
                            </div>
                            <div class="template-layout-column-right">
                                <span class="sf-mega-header">PROJECTES TR@AM</span>
                               <ul>
                                                                           <li><a href="[base_url]proyectos/24-m-endevines-p4-.html">M'endevines (P4)</a></li>
                                                                            <li><a href="[base_url]proyectos/25-tic-tac-estic-amagat-p5-.html">Tic-Tac, estic amagat (P5)</a></li>
                                                                            <li><a href="[base_url]proyectos/26-guess-the-animal-p5-.html">Guess the animal (P5)</a></li>
                                                                            <li><a href="[base_url]proyectos/27-contesnet-2n-.html">Contesnet (2n)</a></li>
                                                                            <li><a href="[base_url]proyectos/28-videoconfer-ncia-en-angl-s-2n-.html">Videoconferència en anglès (2n)</a></li>
                                                                            <li><a href="[base_url]proyectos/29-literactua-3r-.html">Literactua (3r)</a></li>
                                                                            <li><a href="[base_url]proyectos/30-correu-electr-nic-xs-drive-.html">Correu electrònic-XS (Drive)</a></li>
                                                                            <li><a href="[base_url]proyectos/31-fem-xarxes-amb-mr-ethics-5--.html">Fem xarxes amb Mr. Ethics (5è)</a></li>
                                                                            <li><a href="[base_url]proyectos/32-problem-tiques-6--.html">Problemàtiques (6è)</a></li>
                                                                            <li><a href="[base_url]proyectos/33-littlecontes-6--.html">Littlecontes (6è)</a></li>
                                                                    </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a href="[base_url]"><span class="template-icon-menu template-icon-menu-speaker"></span>Blog</a>
                        <ul>
                                                            <li><a href="[base_url]blog/categorias/1-llar-d-infants.html">Llar d'infants</a></li>
                                                            <li><a href="[base_url]blog/categorias/2-infantil.html">Infantil</a></li>
                                                            <li><a href="[base_url]blog/categorias/3-prim-ria.html">Primària</a></li>
                                                            <li><a href="[base_url]blog/categorias/4-secund-ria.html">Secundària</a></li>
                                                            <li><a href="[base_url]blog/categorias/5-escola-verda.html">Escola Verda</a></li>
                                                            <li><a href="[base_url]blog/categorias/6-ampa.html">AMPA</a></li>
                                                            <li><a href="[base_url]blog/categorias/7-esports.html">Esports</a></li>
                                                    </ul>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a href="[base_url]"><span class="template-icon-menu template-icon-menu-gallery"></span>Galeria</a>
                        <ul>
                            <li><a href="[base_url]galeria.html">Fotografies</a></li>
                            <li><a href="[base_url]videos.html">Vídeos</a></li>
                        </ul>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a href="[base_url]contact.html"><span class="template-icon-menu template-icon-menu-envelope"></span>Contacte</a>
                    </li>
                </ul>

            </nav>

            <nav class="template-component-menu-responsive">

                <ul class="template-clear-fix">

                    <li>
                        <a href="#">Inici<span></span></a>
                        <ul>
                            <li>
                                <a href="[base_url]">Inici</a>
                            </li>
                            <li>
                                <a href="[base_url]qui-som.html">Qui Som</a>
                            </li>
                            <li>
                                <a href="[base_url]cicles.html">Cicles</a>
                            </li>
                            <li>
                                <a href="[base_url]serveis.html">Serveis</a>
                            </li>
                            <li>
                                <a href="[base_url]blog.html">Blog</a>
                            </li>
                            <li>
                                <a href="[base_url]contact.html">Contacte</a>
                            </li>

                        </ul>

                    <li>

                </ul>

            </nav>
        </div>

    </div>
</div>    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>AMPA</h1>
                <h6>Què és i què fem</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
		<div class="template-content">
					        <div class="template-content">

			<!-- Section -->
			<div class="template-content-section template-padding-bottom-5 template-main">
				
				<!-- Gallery -->
				<div class="template-component-gallery">
					<ul class="template-layout-50x50 template-clear-fix">
<li class="template-layout-column-left">
							<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
								<a href="media/image/_sample/1050x770/5.jpg" data-fancybox-group="gallery-1">
									<img src="media/image/_sample/690x506/5.jpg" alt=""><span><span><span></span></span></span>
								</a>
								<div>
									<h6>Classes in Writing</h6>
									<span>Shining Stars Class</span>
								</div>
								<p><b>Classes in Writing</b> Shining Stars Class</p>
							</div>
						</li>
						<li class="template-layout-column-right">
							<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
								<a href="media/image/_sample/1050x770/6.jpg" data-fancybox-group="gallery-1">
									<img src="media/image/_sample/690x506/6.jpg" alt=""><span><span><span></span></span></span>
								</a>
								<div>
									<h6>Classes in Painting</h6>
									<span>Shining Stars Class</span>
								</div>
								<p><b>Classes in Painting</b> Shining Stars Class</p>
							</div>			
						</li>
						<li class="template-layout-column-left">
							<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
								<a href="media/image/_sample/1050x770/7.jpg" data-fancybox-group="gallery-1">
									<img src="media/image/_sample/690x506/7.jpg" alt=""><span><span><span></span></span></span>
								</a>
								<div>
									<h6>Drawing and Painting Lessons</h6>
									<span>Tenderhearts Class</span>
								</div>
								<p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
							</div>
						</li>
						<li class="template-layout-column-right">
							<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
								<a href="media/image/_sample/1050x770/1.jpg" data-fancybox-group="gallery-1">
									<img src="media/image/_sample/690x506/1.jpg" alt=""><span><span><span></span></span></span>
								</a>
								<div>
									<h6>Outdoor Activity During Recess</h6>
									<span>Tenderhearts Class</span>
								</div>
								<p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
							</div>			
						</li>
					</ul>
</div>
				
			</div>

			<!-- Section -->
			<div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">
				
				<!-- Main -->
				<div class="template-main">
							
					<!-- Call to action -->
					<div class="template-component-call-to-action template-component-call-to-action-style-2">

						<!-- Content -->
						<div class="template-component-call-to-action-content">

							<!-- Left column -->
							<div class="template-component-call-to-action-content-left">

								<!-- Header -->
								<h3>How to Enroll Your Child to a Class?</h3>

							</div>

							<!-- Right column -->
							<div class="template-component-call-to-action-content-right">

								<!-- Button -->
								<a href="#" class="template-component-button template-component-button-style-1">Learn More<i></i></a>

							</div>									

						</div>

					</div>				
				
				</div>
				
			</div>

		</div>		    		</div>