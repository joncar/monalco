<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Serveis</h1>
                <h6>Tot allò que oferim</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
        <div class="template-main">
                    <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/acollidaicon.png') ?>" style="width:120px"></center>
            <br><br><h2>Acollida i Ludoteca</h2>
            <!-- <h6>Servei d'acollida i ludoteca</h6> -->
            <div></div>
        </div>

        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4></h4>

                <p>El servei d'acollida matinal que s'ofereix a l'escola durant tot el curs escolar està orientat per alumnes d'Infantil i Primària, per tal de facilitar la conciliació de la vida laboral amb els horaris escolars.

<br>L'acollida té un ampli horari, de 7 a 9 del matí; de 13 a 13:30 al migdia i de 17 a 19 h a la tarda. 
</p>

                <!-- List -->
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Esmorzar/berenar</li>
                        <li>Jocs i entreteniment</li>
                        <li>Manualitats</li>
                        <li>Acompanyament a les aules</li>											
                    </ul>
                </div>

                <!-- Vertical grid -->
                <!-- 
<div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu de 8:00 a 9:00:</div>
                            <div>3,50€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu de 8:30 a 9:00:</div>
                            <div>2€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu fixe mensual de 8h a 9h:</div>
                            <div>48€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu fixe mensual de 8:30h a 9h:</div>
                            <div>25€</div>
                        
                        													
                    </ul>
                </div>
 -->

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/10.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/llar4.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/llar4.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/111.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/111.jpg" alt=""/>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>