<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Proposta Educativa</h1>
                <h6>Completa des dels 4 mesos fins els 16 anys</h6>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="template-content">
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
            <center><img src="<?= base_url('img/icon/llar.png') ?>" style="width:120px"></center>
            <h2>Llar d'Infants</h2>
            <div></div>
        </div>
        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">
            <!-- Left column -->
            <div class="template-layout-column-left">
                <!-- Header -->
                <!-- <h4>La Llar...</h4> -->
                <p>
                    <b>El primer cicle d'Educació Infantil queda definit com l'etapa que va dels 4 mesos als 3 anys.</b><br>Volem aconseguir un model de persones competents i compromeses; creatives, sociables i respectuoses envers els altres i el seu entorn.<br>
                    A la nostra Llar, els nens creixen en un ambient familiar, acollidor, afectuós i relaxat. El principal objectiu dels educadors és vetllar pel respecte, pels  valors i per la satisfacció de les necessitats vitals de l’infant.
                    Fem una acollida personalitzada per a cada nen/a, segons les seves necessitats, amb un horari flexible per a cada família i infant.<br>
                    La cura en les relacions entre iguals i l’acompanyament en el procés de creixement personal formen part del nostre dia a dia. <br>
                    Els espais estan pensats per ser acollidors, amplis, funcionals, innovadors i adequats per facilitar l’adquisició dels aprenentatges de manera viva i motivadora.<br>
                    Per això fem present en el dia a dia un  seguit de valors de manera transversal i des de qualsevol àmbit de l’aprenentatge, que permetin desenvolupar la tolerància, el respecte, l’entorn i treballar les emocions per créixer en el coneixement d’un mateix i en l’apropament als altres.
                    <br>Volem que els infants s’emocionin aprenent i que a poc a poc assoleixin hàbits, responsabilitats, autonomia i coneixements en un ambient acollidor i molt proper. Volem que els nens i les nenes se sentin com a casa en un ambient tranquil i familiar.
                </p>
                <!-- List -->
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Fer l'infant protagonista del seu propi aprenentatge</li>
                        <li>Donar valor a l'activitat autònoma basada en la iniciativa del nen/a</li>
                        <li>Respectar els diferents ritmes d'aprenentatge</li>
                        <li>Cuidar el benestar emocional de cada infant</li>
                        <li>Mantenir el desig per descobrir i per aprendre</li>
                        <li>Vetllar per les necessitats bàsiques de cada alumne</li>
                        <li>Atenció personalitzada per a cada infant</li>
                        <li>Estimulació primerenca</li>
                        <li>Aprenentatge proper i vivencial</li>
                        <li>Treball per ambients d'aprenentatge i tallers</li>
                    </ul>
                </div>
                <!-- Vertical grid -->
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>P0 - Marietes:</div>
                            <div>4-16 mesos</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>P1 - Cargols i Papallones:</div>
                            <div>1-2 anys</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>P2 - Pollets i Esquirols:</div>
                            <div>2-3 anys</div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Right column -->
            <div class="template-layout-column-right">
                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/llar22.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/llar22.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/llar1.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/llar1.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/llar80.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/llar80.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/llar41.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/llar41.jpg" alt=""/>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">
        <!-- Main -->
        <div class="template-main">
            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                        <h5>Horari</h5>
                        <p>de 7 a 19h </p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pin-alt"></div>
                        <h5>Funcionament</h5>
                        <p>de setembre a juliol inclosos</p>
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-cutlery-alt"></div>
                        <h5>Menjador</h5>
                        <p>amb educadores de suport</p>
                    </li>
                </ul>
            </div>
            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-poll-alt"></div>
                        <h5>Especialistes</h5>
                        <p>de psicomotricitat i de llengua anglesa</p>
                        
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-toy-alt"></div>
                        <h5>Transport</h5>
                        <p>transport escolar privat
                        </p>
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-people-alt"></div>
                        <h5>Famílies</h5>
                        <p>relació directa i diària amb les famílies</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Section -->
    <div class="template-content-section template-background-image template-background-image-4">
        <div class="template-main">
            <!-- Testimonials -->
            <div class="template-section-white">
                <div class="template-component-testimonial template-component-testimonial-style-2">
                    <ul class="template-layout-100">
                        <li class="template-layout-column-left">
                            <i></i>
                            <p><br>Som una escola amb voluntat d'innovació pedagògica que aposta de forma clara per incorporar les llengües estrangeres, l'educació  emocional i l'estimulació de les habilitats artístiques, de forma transversal a les diverses etapes educatives</p>
                            <div></div>
                            <span></span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p><br>Com a escola multilingüe que som, oferim la llengua anglesa des de P1, amb educadores especialitzades</p>
                            <div></div>
                            <span></span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p><br>Volem que els infants s'emocionin aprenent i que a poc a poc assoleixin hàbits, responsabilitats, autonomia i coneixements en un ambient acollidor i molt proper</p>
                            <div></div>
                            <span></span>
                        </li>
                        
                    </ul>
                    <div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Section -->
    <div class="template-content-section template-main template-padding-top-reset template-padding-bottom-5">
        <div class="template-component-counter-box template-margin-top-5">
            <center>
            <h3>Enquestes</h3>
            <div class="template-component-divider template-component-divider-style-2"></div>
            <div class="template-component-italic template-margin-top-3">
                Resultats enquesta satisfacció Monalco
            </div>
            </center>
        </div>
        [encuesta1]
        <!-- Header and subheader -->
        <div class="template-component-header-subheader template-margin-top-5">
            <br><h2>L'equip de la Llar</h2>
            <h6>Comptem amb un bon equip humà de professionals ben cohesionats i que treballen en equip al servei dels nostres alumnes i les nostres famílies</h6>
            <div></div>
        </div>
        <!-- Team -->
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix">
                <li class="template-layout-column-left">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/teresa.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/teresa.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Teresa Queralt</h6>
                                    <span>Coordinadora de la Llar i Infantil</span>
                                </div>
                                <p><b>Teresa Queralt</b> Coordinadora de la Llar</p>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">Els mestres, professors i educadors del Col.legi Monalco estimem la nostra feina i la fem amb una dedicació plena. Volem que els nostres infants siguin feliços.
                            </p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!-- <li><a href="#" class="template-component-social-icon-mail"></a></li> -->
                                    <!--
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                    -->
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="template-layout-column-right">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/sonia.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/sonia.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Sònia Palmés</h6>
                                    <span>Professora de la Llar</span>
                                </div>
                                <p><b>Sònia Palmés</b> Professora de la Llar</p>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">Vaig començar en aquesta gran tasca amb els pares i mares de molts dels actuals alumnes de la Llar. <br> És una sort poder participar dels inicis</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!--  <li><a href="#" class="template-component-social-icon-mail"></a></li> -->
                                    <!--
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                    -->
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- Header and subheader -->
    <div class="template-component-header-subheader">
        <h2>El Blog de la Llar</h2>
        <h6>T'informem de les activitats que es realitzen a la Llar d'Infants</h6>
        <div></div>
    </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
        <div class="template-main">
            <!-- Recen post -->
            <div class="template-component-recent-post template-component-recent-post-style-1">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    [foreach:blog]
                    <li class="template-layout-column-[posicion]" style="visibility: visible;">
                        <div class="template-component-recent-post-date">[fecha]</div>
                        <div class="template-component-image">
                            <a href="[link]" id="mce_266" class="">
                                <img src="[foto]" alt="">
                                <span id="mce_267" class=""><span id="mce_268">
                                    <span id="mce_269">
                                        
                                    </span>
                                </span>
                            </span>
                        </a>                        
                    </div>
                    <h5 id="mce_270" class="">
                    <a href="[link]" id="mce_271" data-mce-href="#" class="">
                        [titulo]
                    </a>
                    </h5>
                    <p id="mce_272" class="">
                        [descripcion]
                    </p>
                    <ul class="template-component-recent-post-meta template-clear-fix">
                        <li class="template-icon-blog template-icon-blog-author">
                            <a href="[link]" id="mce_273" class="">[user]</a>
                        </li>
                        <li class="template-icon-blog template-icon-blog-category">
                            <a href="[link]" id="mce_274" class="">Events</a>,
                            <a href="[link]" id="mce_275" class="">Fun</a>
                        </li>
                    </ul>
                </li>
                [/foreach]
            </ul>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>