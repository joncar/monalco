
            
            
            <!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_0" class="">Organització</h1>
                <h6 id="mce_13" class="">Entenem una mica l'escola</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-content-layout template-content-layout-sidebar-right template-main template-clear-fix">

                <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/organ.png') ?>" style="width:120px"></center>
                <h2>Organigrama</h2>                
                <div></div>
            </div>

		<!-- Content -->
		<div class="template-content-layout">

			<div data-id="section-1">
                <!-- Accordion -->
                <div class="template-component-accordion">
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Equip Directiu</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Gerent</th>
                                <td>Josep Mª Vicente</td>
                            </tr>
                            <tr>
                                <th>Directora</th>
                                <td>Susana Vicente</td>
                            </tr>
                            <tr>
                                <th>Sotsdirector</th>
                                <td>Manel Navarro</td>
                            </tr>
                            <tr>
                                <th>Coordinadora de la Llar i d'Infantil</th>
                                <td>Teresa Queralt</td>
                            </tr>
                            <tr>
                                <th>Cap d'Estudis de Primària</th>
                                <td>Ramona Turmo</td>
                            </tr>
                            <tr>
                                <th>Cap d'Estudis de Secundària</th>
                                <td>Alicia Corral</td>
                            </tr>
                            
                        </table>                        
                    </div>

                     <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Administració i Serveis</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Secretaria acadèmica</th>
                                <td>Martí Salanova</td>
                            </tr>
                            <tr>
                                <th>Auxiliar administrativa</th>
                                <td>Gemma Casals</td>
                            </tr>
                            <tr>
                                <th>Auxiliar secretaria</th>
                                <td>Mireia Vicente</td>
                            </tr>
                            <tr>
                                <th>Departament psicopedagògic</th>
                                <td>Aurora Picas, Rosa Gleyal i Rosa Arbós</td>
                            </tr>
                            <tr>
                                <th>Menjador</th>
                                <td>Teresa Queralt, Neus Bosch</td>
                            </tr>
                            <tr>
                                <th>Manteniment</th>
                                <td>Josep Mª Hormigo i Lucia Shirokova</td>
                            </tr>
                            <tr>
                                <th>Extraescolar</th>
                                <td>Jaume Vidal</td>
                            </tr>
                            <tr>
                                <th>Transport</th>
                                <td>Mireia Vicente, Jaume Vidal i Jose Mª Hormigo</td>
                            </tr>
                        </table>                        
                    </div>
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Secundària</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>1r</th>
                                <td>Rosa Arbós, Meritxell Piñol, Dolors Riba</td>
                            </tr>
                            <tr>
                                <th>2n</th>
                                <td>Manoli Capel, Geni Solà, Laia Rubio</td>
                            </tr>
                            <tr>
                                <th>3r</th>
                                <td>Regina Gabarró, Anna Maria Insa, Dolors Solé</td>
                            </tr>
                            <tr>
                                <th>4t</th>
                                <td>Manel Navarro, Alicia Corral, Tània Casellas</td>
                            </tr>
                                                        <tr>
                                <th>Especialistes/Professorat</th>
                                <td>Susana Vicente, Josep Saus, Marc Paré, Marc Brufau i Laura Parrado</td>
                            </tr>
                            
                        </table>                        
                    </div>
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Primària</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Coordinadora CI</th>
                                <td>Gemma Morist</td>
                            </tr>
                            <tr>
                                <th>1r</th>
                                <td>Gemma Morist</td>
                            </tr>
                            <tr>
                                <th>1r</th>
                                <td>Carles Palau</td>
                            </tr>
                            <tr>
                                <th>2n</th>
                                <td>Clàudia Miramunt</td>
                            </tr>
                            <tr>
                                <th>2n</th>
                                <td>Natalia Durán</td>
                            </tr>
                            <tr>
                                <th>Coordinadora CM</th>
                                <td>Victòria Camacho</td>
                            </tr>
                            <tr>
                                <th>3r</th>
                                <td>Victòria Camacho</td>
                            </tr>
                            <tr>
                                <th>3r</th>
                                <td>Roser Carulla</td>
                            </tr>
                            <tr>
                                <th>4t</th>
                                <td>Agustina Valls</td>
                            </tr>
                            <tr>
                                <th>4t</th>
                                <td>Lali Vicente</td>
                            </tr>
                            <tr>
                                <th>Coordinadora CS</th>
                                <td>Ramona Turmo</td>
                            </tr>
                            <tr>
                                <th>5è</th>
                                <td>Antònia Comaposada</td>
                            </tr>
                            <tr>
                                <th>5è</th>
                                <td>Eva Feliu</td>
                            </tr>
                            <tr>
                                <th>6è</th>
                                <td>Teresa Palacios</td>
                            </tr>
                            <tr>
                                <th>6è</th>
                                <td>Ramona Turmo</td>
                            </tr>
                            <tr>
                                <th>Especialistes/Reforç</th>
                                <td>Mercè Ortínez, Núria Bou, Sandra Cantero, Jordi Banqué, Judit Solano, Lourdes Rubí, Meritxell Freixes, Montse Vilà i Teresa Piqué</td>
                            </tr>
                            
                        </table>                        
                    </div>
<!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Infantil</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Coordinadora</th>
                                <td>Teresa Queralt</td>
                            </tr>
                            <tr>
                                <th>P3</th>
                                <td>Montse Leonés</td>
                            </tr>
                            <tr>
                                <th>P3</th>
                                <td>Eva Duran</td>
                            </tr>
                            <tr>
                                <th>P4</th>
                                <td>Lídia Alcoberro</td>
                            </tr>
                            <tr>
                                <th>P4</th>
                                <td>Anna Duran</td>
                            </tr>
                            <tr>
                                <th>P5</th>
                                <td>Mariona Jané</td>
                            </tr>
                            <tr>
                                <th>P5</th>
                                <td>Maria Muñoz</td>
                            </tr>
                            <tr>
                                <th>Especialistes/Reforç</th>
                                <td>Eva Fernandez, Montse Viladot, Sandra Cantero, Jordi Banqué, Teresa Queralt, Dolors Torralbas</td>
                            </tr>
                            
                        </table>                        
                    </div>
                     <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Llar d'Infants</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Coordinadora</th>
                                <td>Teresa Queralt</td>
                            </tr>
                            <tr>
                                <th>P0</th>
                                <td>Noelia Loarte</td>
                            </tr>
                            <tr>
                                <th>P1</th>
                                <td>Aida Hernández</td>
                            </tr>
                            <tr>
                                <th>P1</th>
                                <td>Sònia Palmés</td>
                            </tr>
                            <tr>
                                <th>P2</th>
                                <td>Meritxell Freixes</td>
                            </tr>
                            <tr>
                                <th>P2</th>
                                <td>Sara Gil</td>
                            </tr>
                            <tr>
                                <th>Especialistes/Reforç</th>
                                <td>Lorena Pérez, Teresa Queralt, Judit Ballesteros, Neus Bosch i Anna Rodríguez</td>
                            </tr>
                            
                        </table>                        
                    </div>
                    
                    
                    
                    
                    
                    
                   
                </div>
            </div>
         </div>
     </div>
</div>
        