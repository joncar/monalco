<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Avís legal i Política de Privacitat</h1>
                <h6>Som una web segura que ens preocupem per la teva privacitat</h6>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="template-content" style="margin-bottom: 0px;">
    <!-- Section -->
    <div class="template-content-section template-main template-clear-fix ">
        
        <!-- Post -->
        <div class="template-post">
            <!-- Content -->
            <div class="template-post-section-content">
                <div class="template-post-content">
                    <h3 class="template-margin-top-3">Avís Legal</h3>
                    <h5 class="template-margin-top-3">1. DADES D'IDENTIFICACIÓ</h5>
                    <p class="template-padding-reset">
                        Vostè està visitant la pàgina Web http://www.monalco.cat titularitat de MONALCO, S.L. (d'ara endavant L'EMPRESA).
                        Domicili Social: C/CAPELLADES, 2 08700 IGUALADA  (Barcelona - Espanya)
                        CIF: B08880031
                        Telèfon: 93 8031577
                        E-mail: info@monalco.cat
                        Dades mercantils: Societat Limitada, inscrita en el Registre Mercantil de Barcelona, volum 20927, foli 16, fulla B-14089
                    </p>
                    <h5 class="template-margin-top-3">2. ACCEPTACIÓ DE L'USUARI</h5>
                    <p class="template-padding-reset">
                        Aquest Avís Legal regula l'accés i utilització de la pàgina web www.monalco.cat (d'ara endavant la “Web”) que L'EMPRESA posa a la disposició dels usuaris d'Internet. S'entén per usuari la persona que accedeixi, navegui, utilitzi o participi en els serveis i activitats de la Web.
                        L'accés i navegació d'un usuari per la Web implica l'acceptació sense reserves del present Avís Legal.
                        L'EMPRESA pot oferir a través de la pàgina web serveis que podran trobar-se sotmesos a unes condicions particulars pròpies sobre les quals s'informarà a l'Usuari en cada cas concret.
                    </p>
                    <h5 class="template-margin-top-3">3. UTILITZACIÓ DE LA WEB</h5>
                    <p class="template-padding-reset">
                        3.1. L'usuari es compromet a utilitzar la Web, els continguts i serveis de conformitat amb la Llei, el present Avís Legal, els bons costums i l'ordre públic. De la mateixa manera l'Usuari s'obliga a no utilitzar el Web o els serveis que es prestin a través d'ell amb finalitats o efectes il·lícits o contraris al contingut del present Avís Legal, lesius dels interessos o drets de tercers, o que de qualsevol forma pugui danyar, inutilitzar o deteriorar el Web o els seus serveis o impedir un normal gaudi del Web per altres Usuaris.
                        <br>3.2. L'usuari es compromet expressament a no destruir, alterar, inutilitzar o, de qualsevol altra forma, danyar les dades, programes o documents electrònics i altres que es trobin al Web.
                        <br>3.3. L'usuari es compromet a no obstaculitzar l'accés a altres usuaris al servei d'accés mitjançant el consum massiu dels recursos informàtics a través dels quals L'EMPRESA presta el servei, així com realitzar accions que danyin, interrompin o generin errors en aquests sistemes o serveis.
                        <br>3.4. L'usuari es compromet a no introduir programes, virus, macros, applets, controls ActiveX o qualsevol altre dispositiu lògic o seqüència de caràcters que causin o siguin susceptibles de causar qualsevol tipus d'alteració en els sistemes informàtics de l'EMPRESA o de tercers.
                        <br>3.5. L'usuari es compromet a no obtenir informacions, missatges, gràfics, dibuixos, arxius de so i/o imatge, fotografies, enregistraments, programari i en general, qualsevol classe de material accessible a través del Web o dels serveis oferts en el mateix.
                        <br>3.6. S'entén que l'accés o utilització del Web per part de l'usuari implica l'acceptació per aquest de l'Avís Legal que L'EMPRESA tingui publicades al moment de l'accés, les quals estaran sempre disponibles per als usuaris.
                        <br>3.7. L'usuari es compromet a fer un ús adequat dels continguts i serveis (com per exemple comentaris en el blog) que L'EMPRESA ofereix a la seva seu Web i a no emprar-los per incórrer en activitats il·lícites o contràries a la bona fe i a l'ordenament legal; difondre continguts o propaganda de caràcter racista, xenòfob, pornogràfic-il·legal, d'apologia del terrorisme o atemptatori contra els drets humans.
                    </p>
                    <h5 class="template-margin-top-3">4. ACCÉS AL WEB I CONTRASENYES</h5>
                    <p class="template-padding-reset">
                        4.1. L'accés al Web per part dels usuaris té caràcter lliure i gratuït.
                        <br>4.2. Quan sigui necessari que l'usuari es registri o aporti dades personals per poder accedir a algun dels serveis, la recollida i el tractament de les dades personals dels usuaris serà aplicable el que es disposa a l'apartat 9 d'aquest Avís Legal, així com el que es disposa en les Condicions de Registre.
                        <br>4.3. Si per a la utilització d'un servei al Web, l'usuari hagués de procedir al seu registre, aquest serà responsable d'aportar informació veraç i lícita. Si com a conseqüència del registre, es genera una contrasenya per a l'usuari, aquest es compromet a fer un ús diligent i a mantenir en secret la contrasenya per accedir a aquests serveis. Els usuaris són responsables de l'adequada custòdia i confidencialitat de qualsevol identificador i/o contrasenya que li sigui subministrat per l'EMPRESA i es comprometen a no cedir el seu ús a tercers, ja sigui temporal o permanent, ni a permetre el seu accés a persones alienes. Serà responsabilitat de l'usuari la utilització dels serveis per qualsevol tercer il·legítim que empri a aquest efecte una contrasenya a causa d'una utilització no diligent o de la pèrdua de la mateixa per l'usuari.
                        <br>4.4. L'usuari està obligat a notificar de forma immediata a l'EMPRESA qualsevol fet que permeti l'ús indegut dels identificadors i/o contrasenyes, tals com el robatori, pérdua, o l'accés no autoritzat als mateixos, amb la finalitat de procedir a la seva immediata cancel·lació. Mentre no es comuniquin tals fets, L'EMPRESA quedarà eximida de qualsevol responsabilitat que pogués derivar-se de l'ús indegut dels identificadors i/o contrasenyes per tercers no autoritzats.
                    </p>
                    <h5 class="template-margin-top-3">5. MODIFICACIÓ DE LES CONDICIONS DE L'AVÍS LEGAL</h5>
                    <p class="template-padding-reset">
                    L'EMPRESA es reserva expressament el dret a modificar el present Avís Legal. L'usuari reconeix i accepta que és la seva responsabilitat revisar el Web i el present Avís Legal.  </p>
                    <h5 class="template-margin-top-3">6. LIMITACIÓ DE GARANTIES I RESPONSABILITATS</h5>
                    <p class="template-padding-reset">
                        6.1. L'EMPRESA es compromet a realitzar els seus millors esforços per evitar qualsevol error en els continguts que poguessin aparèixer al Web. En qualsevol cas, L'EMPRESA estarà exempta de qualsevol responsabilitat derivada d'eventuals errors en els continguts que poguessin aparèixer al Web, sempre que no li siguin imputables.
                        <br>6.2. L'EMPRESA no garanteix que el Web i el servidor estiguin lliures de virus i no es fa responsable dels possibles danys o perjudicis que es puguin derivar d'interferències, omissions, interrupcions, virus informàtics, avaries telefòniques o desconnexions en el funcionament operatiu d'aquest sistema electrònic, motivat per causes alienes a l'EMPRESA, de retards o bloquejos en l'ús d'aquest sistema electrònic causats per deficiències de línies telefòniques o sobrecàrregues en el sistema d'Internet o en altres sistemes electrònics, així com també de danys que puguin ser causats per terceres persones mitjançant intromissions il·legítimes fos del control de l'EMPRESA.
                    </p>
                    <h5 class="template-margin-top-3">7. PROPIETAT INTEL·LECTUAL I INDUSTRIAL. TOTS ELS DRETS RESERVATS</h5>
                    <p class="template-padding-reset">
                        7.1. Tots els drets de Propietat Industrial i Intel·lectual del Web www.monalco.cat i dels seus continguts (textos, imatges, sons, àudio, vídeo, dissenys, creativitats, programari) pertanyen, com a autor d'obra col·lectiva o com a cessionària, a l'EMPRESA o, si escau, a terceres persones.
                        <br>7.2. Queden expressament prohibides la reproducció, la distribució i la comunicació pública, inclosa la seva modalitat de posada a disposició, de la totalitat o part dels continguts d'aquesta pàgina Web, amb qualsevol fi, en qualsevol suport i per qualsevol mitjà tècnic, sense l'autorització de l'EMPRESA. L'usuari es compromet a respectar els drets de Propietat Industrial i Intel·lectual titularitat de l'EMPRESA i de tercers.
                        <br>7.3. L'usuari pot visualitzar tots els elements, imprimir-los, copiar-los i emmagatzemar-los en el disc dur del seu ordinador o en qualsevol altre suport físic sempre que sigui, única i exclusivament, per al seu ús personal i privat, quedant, per tant, terminantment prohibida la seva utilització amb altres finalitats, la seva distribució, així com la seva modificació, alteració o descompilació.
                        <br>7.4. L'EMPRESA proporciona l'accés a tot tipus d'informacions o dades en Internet que poden pertànyer a terceres persones, en aquest cas L'EMPRESA no es fa responsable d'aquests continguts ni de quantes reclamacions puguin derivar-se de la qualitat, fiabilitat, exactitud o correcció dels mateixos.
                    </p>
                    <h5 class="template-margin-top-3">8. ENLLAÇOS DE TERCERS</h5>
                    <p class="template-padding-reset">
                        8.1. En el cas que a la Web es disposessin enllaços o hipervíncles cap a altres llocs d'Internet, L'EMPRESA no exercirà cap tipus de control sobre aquests llocs i continguts. En cap cas L'EMPRESA assumirà responsabilitat alguna pels continguts d'algun enllaç pertanyent a un lloc Web aliè, ni garantirà la disponibilitat tècnica, qualitat, fiabilitat, exactitud, amplitud, veracitat, validesa i constitucionalitat de qualsevol material o informació continguda al capdavant dels hipervíncles o altres llocs d'Internet.
                        <br>8.2. Aquests enllaços es proporcionen únicament per informar a l'Usuari sobre l'existència d'altres fonts d'informació sobre un tema concret, i la inclusió d'un enllaç no implica l'aprovació de la pàgina web enllaçada per part de l'EMPRESA.
                        <br>8.3. L'EMPRESA tan sols autoritza esments als seus continguts en altres Webs, amb el tractament que aquestes considerin, sempre que sigui respectuós, compleixi la legislació vigent i en cap cas reprodueixin, sense la deguda autorització, els continguts de l'EMPRESA.
                    </p>
                    <h5 class="template-margin-top-3">9. PROTECCIÓ DE DADES PERSONALS</h5>
                    <p class="template-padding-reset">
                        9.1. L'EMPRESA adopta les mesures tècniques i organitzatives necessàries per garantir la protecció de les dades de caràcter personal i evitar la seva alteració, pèrdua, tractament i/o accés no autoritzat, tenint en compte de l'estat de la tècnica, la naturalesa de les dades emmagatzemades i els riscos al fet que estan exposats, tot això, conforme a l'establert per la legislació espanyola de Protecció de Dades de Caràcter Personal.
                        <br>9.2. L'usuari podrà proporcionar a l'EMPRESA les seves dades de caràcter personal a través dels diferents formularis que a aquest efecte apareixen a la Web. Aquests formularis incorporen un text legal en matèria de protecció de dades personals que dóna compliment a les exigències establertes en el REGLAMENT (UE) 2016/679 del Parlament Europeu i del Consell de 27 d'abril de 2016 relatiu a la protecció de les persones físiques pel que fa al tractament de dades personals i a la lliure circulació d'aquestes dades (Reglament General de Protecció de Dades).
                        De la mateixa manera, L'EMPRESA podrà obtenir dades de caràcter personal originats per la seva navegació i/o ús de la Web, d'acord amb la nostra Política de Privadesa.
                        <br>9.3. Preguem llegeixi atentament els textos legals, en particular nostra Política de Privadesa, abans de facilitar les seves dades de caràcter personal.
                    </p>
                    <h5 class="template-margin-top-3">10. LLEI I JURISDICCIÓ</h5>
                    <p class="template-padding-reset">
                        Totes les qüestions que se suscitin entre L'EMPRESA i l'usuari relatives a la interpretació, compliment i validesa del present Avís Legal es regiran per les seves pròpies clàusules i, en el que en elles no estigués previst, d'acord amb la legislació espanyola, sotmetent-se expressament les parts a la jurisdicció dels Jutjats i Tribunals del domicili de l'EMPRESA
                    </p>
                    <h3 class="template-margin-top-3">Política de Privacitat</h3>
                    <h5 class="template-margin-top-3"></h5>
                    <p class="template-padding-reset">
                        La confidencialitat i la seguretat són valors primordials de MONALCO, S.L. i, en conseqüència, assumim el compromís de garantir la privadesa de l'Usuari a tot moment i de no recaptar informació innecessària. A continuació, li proporcionem tota la informació necessària sobre la nostra Política de Privadesa en relació amb les dades personals que recaptem, explicant-li:
                    </p>
                    <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                        <ul>
                            <li>Qui és el responsable del tractament de les seves dades</li>
                            <li>Para quines finalitats recaptem les dades que li sol·licitem</li>
                            <li>Quin és la legitimació per al seu tractament</li>
                            <li>Durant quant temps els conservem</li>
                            <li>A quins destinataris es comuniquen les seves dades</li>
                            <li>Quins són els seus drets</li>
                        </ul>
                        <h5 class="template-margin-top-3">1. RESPONSABLE</h5>
                        <p class="template-padding-reset">
                            Veure dades Avís Legal
                        </p>
                        <h5 class="template-margin-top-3">2. FINALITATS, LEGITIMACIÓ I CONSERVACIÓ </h5>
                        <p class="template-padding-reset">
                            dels tractaments de les dades enviades a través de:
                            <br>Formulari de Contacte.
                            <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                                <ul>
                                    <li>Finalitat: Facilitar-li un mitjà perquè pugui posar-se en contacte amb nosaltres i contestar a les seves sol·licituds d'informació, així com enviar-li comunicacions dels nostres productes, serveis i activitats, inclusivament per mitjans electrònics (correu electrònic, SMS, whatsApp), només si marca la casella corresponent atorgant el consentiment exprés.
                                    </li>
                                    <li>Legitimació: El consentiment de l'usuari en sol·licitar-nos informació a través del nostre formulari de contactes i en marcar la casella d'acceptació d'enviament d'informació.
                                    </li>
                                    <li>Conservació: Una vegada resulta contestada la seva petició per correu electrònic, si no ha generat un nou tractament. En el cas de rebre el seu CV, les seves dades podran ser conservats durant TRES anys per a futurs processos de selecció.
                                    </li>
                                </ul>
                                
                            </div>
                            <h5 class="template-margin-top-3"></h5>
                            <p class="template-padding-reset">
                                Enviament de correus electrònics.
                                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                                    <ul>
                                        <li>Finalitat: Contestar a les seves sol·licituds d'informació, atendre les seves peticions i respondre les seves consultes o dubtes. En cas de rebre el seu Currículum vitae, les seves dades personals i curriculars podran formar part de les nostres bases de dades per participar en els nostres processos de selecció presents i futurs.
                                        </li>
                                        <li>Legitimació: El consentiment de l'usuari en sol·licitar-nos informació a través de l'adreça de correu electrònic o enviar-nos les seves dades i CV per participar en els nostres processos de selecció.
                                        </li>
                                        <li>Conservació: Una vegada resolta la seva sol·licitud per mitjà del nostre formulari o contestada per correu electrònic, si no ha generat un nou tractament, i en cas d'haver acceptat rebre enviaments comercials, fins que sol·liciti la baixa dels mateixos.
                                        </li>
                                    </ul>
                                </div>
                            </p>
                            <h5 class="template-margin-top-3"></h5>
                    <p class="template-padding-reset">
                        Obligació de facilitar-nos les seves dades personals i conseqüències de no fer-ho.

<br><br>El subministrament de dades personals requereix una edat mínima de 13 anys, o si escau, disposar de capacitat jurídica suficient per contractar.

<br><br>Les dades personals sol·licitades són necessaris per gestionar les seves sol·licituds, donar-li d'alta com a usuari i/o prestar-li els serveis que pugui contractar, per la qual cosa, si no ens els facilita, no podrem atendre-li correctament ni prestar-li el servei que ha sol·licitat.

<br><br>En tot cas, ens reservem el dret de decidir sobre la incorporació o no de les seves dades personals i altra informació a les nostres bases de dades.
                    </p>
                    <h5 class="template-margin-top-3">3. DESTINATARIS DE LES SEVES DADES</h5>
                    <p class="template-padding-reset">
                        Les seves dades són confidencials i no es cediran a tercers, tret que existeixi obligació legal.
                    </p>
                    <h5 class="template-margin-top-3">4. DRETS EN RELACIÓ AMB LES SEVES DADES PERSONALS</h5>
                    <p class="template-padding-reset">
                        Qualsevol persona pot retirar el seu consentiment a qualsevol moment, quan el mateix s'hagi atorgat per al tractament de les seves dades. En cap cas, la retirada d'aquest consentiment condiciona l'execució del contracte de subscripció o les relacions generades amb anterioritat.

                    </p>
                    <h5 class="template-margin-top-3"></h5>
                            <p class="template-padding-reset">
                                Igualment, pot exercir els següents drets:
                                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                                    <ul>
                                        <li>Sol·licitar l'accés a les seves dades personals o la seva rectificació quan siguin inexactes.

                                        </li>
                                        <li>Sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessaris per a les finalitats pels quals van ser recollits.
                                        </li>
                                        <li>Sol·licitar la limitació del seu tractament en determinades circumstàncies.
                                        </li>
                                        <li>Sol·licitar l'oposició al tractament de les seves dades per motius relacionats amb la seva situació particular.
                                        </li>
                                        <li>Sol·licitar la portabilitat de les dades en els casos previstos en la normativa.
                                        </li>
                                        <li>Altres drets reconeguts en les normatives aplicables.
                                        </li>
                                    </ul>
                                </div>
                            </p>
                            <h5 class="template-margin-top-3"></h5>
                    <p class="template-padding-reset">
                        On i com sol·licitar els seus Drets: Mitjançant un escrit dirigit al Delegat de Protecció de Dades a la seva adreça postal o electrònica (indicades a l'apartat A), indicant la referencia “Dades Personals”, especificant el dret que es vol exercir i respecte a quines dades personals.

<br><br>En cas de divergències amb l'empresa en relació amb el tractament de les seves dades, pot presentar una reclamació davant l'Agència de Protecció de Dades (www.agpd.es).


                    </p>
                    <h5 class="template-margin-top-3">5. SEGURETAT DE LES SEVES DADES PERSONALS</h5>
                    <p class="template-padding-reset">
                        Amb l'objectiu de salvaguardar la seguretat de les seves dades personals, li informem que hem adoptat totes les mesures d'índole tècnica i organitzativa necessàries per garantir la seguretat de les dades personals subministrades de la seva alteració, pèrdua i tractaments o accessos no autoritzats.
                    </p>
                    <h5 class="template-margin-top-3">6. ACTUALITZACIÓ DE LES SEVES DADES</h5>
                    <p class="template-padding-reset">
                        És important que perquè puguem mantenir les seves dades personals actualitzades, ens informi sempre que hi hagi hagut alguna modificació en ells, en cas contrari, no responem de la veracitat dels mateixos.

<br><br>No ens fem responsables de la política de privadesa respecte a les dades personals que pugui facilitar a tercers per mitjà dels enllaços disponibles a la nostra pàgina web.

<br><br>La present Política de Privadesa pot ser modificada per adaptar-les als canvis que es produeixi a la nostra web, així com modificacions legislatives o jurisprudencials sobre dades personals que vagin apareixent, per la qual cosa exigeix la seva lectura, cada vegada que ens faciliti les seves dades a través d'aquesta Web.

<br><br>D. RESPONSABILITATS

<br><br>En posar a la disposició de l'usuari aquesta pàgina Web volem oferir-li un servei de qualitat, utilitzant la màxima diligència en la prestació del mateix, així com en els mitjans tecnològics utilitzats. No obstant això, no respondrem de la presència de virus i altres elements que d'alguna manera puguin danyar el sistema informàtic de l'usuari.

<br><br>No garantim que la disponibilitat del servei sigui contínua i ininterrompuda.

<br><br>L'USUARI té prohibit qualsevol tipus d'acció sobre el nostre portal que origini una excessiva sobrecàrrega de funcionament als nostres sistemes informàtics, així com la introducció de virus, o instal·lació de robots, o programari que alteri el normal funcionament de la nostra web, o en definitiva pugui causar danys als nostres sistemes informàtics.

<br><br>L'USUARI assumeix tota la responsabilitat derivada de l'ús de la nostra pàgina web.

<br><br>L'USUARI reconeix que ha entès tota la informació respecte a les condicions d'ús del nostre portal, i reconeix que són suficients per a l'exclusió de l'error en les mateixes, i per tant, les accepta integra i expressament.</p>
                        </div>
                        
                        
                    </div>
                </div>
                
            </div>
        </div>