<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>L'Escola</h1>
                <h6>Coneix el nostre centre</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/historia.png') ?>" style="width:120px"></center>
                <h2>Història</h2>                
                <div></div>
            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3><strong>D'on venim?</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            MONALCO deriva de Montessori (MON), Almi (AL) i Cots (CO), que eren les escoles predecessores de l’actual, situades també a Igualada.
                        </div>

                        <p class="template-margin-top-3">
                            La nostra escola té l'esperit i voluntat de donar uns serveis de qualitat a les famílies que ens dipositen la seva confiança. <br> El fundador va ser el Sr. Pere Vicente Rodríguez. <br>L’origen del Col·legi MONALCO és l’Acadèmia Cots que s’inaugurà a Igualada pels volts del 1930 en un local situat en el núm. 30 del carrer d’Òdena.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">1930-1946 </i></a>

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/10.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/cots2.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Façana de l'Acadèmia Cots</h6>
                                        <span>Anys 30</span>
                                    </div>
                                    <p><b>Façana de l'Acadèmia Cots</b> Anys 30</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/40.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/41.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Unificació</h6>
                                        <span>Anys 40</span>
                                    </div>
                                    <p><b>Unificació</b>Anys 40</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3><strong>Unificació</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            L’any 1946, el fundador de la nostra escola, el Sr. Pere Vicente va pensar a fer el primer ensenyament amb els alumnes més petits. S’hagué de buscar un nou local que fou el situat al carrer de Sant Magí, 58; se li va posar el nom de Col·legi Verdaguer.

                        </div>

                        <p class="template-margin-top-3">
                            Però aviat el nombre d’alumnes va anar augmentant fins que no hi van cabre. La solució va ser passar els alumnes més grans a l’Acadèmia Cots del carrer Nou on, durant un temps, a més de l’ensenyament comercial, es van donar classes d’ensenyament primari. Mentrestant, al local del carrer de Sant Magí es continuava ensenyant als més petits. Aquest serà el local que es convertirà en l’Acadèmia Almi.

                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">1946-1960 </i></a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3><strong>Montessori</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            Amb el temps, ambdues escoles es feren petites. Cap als anys 60, el Sr. Vicente havia comprat un terreny a l’Avinguda Barcelona, a on va traslladar una secció de pàrvuls a la qual va donar el nom de Montessori, impartint la metodologia de Maria Montessori.
                        </div>

                        <p class="template-margin-top-3">
                            Tenint ja tres locals: el del carrer Nou (Cots), el del carrer Sant Magí (Almi) i el de l’avinguda Barcelona (Montessori) va arribar el moment de fer una escola més unificada i concentrada en un mateix lloc.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Anys 60 </i></a>

                    </div>

                </div>
                

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/montessori.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/mon.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Montessori</h6>
                                        <span>Anys 60</span>
                                    </div>
                                    <p><b>Montessori</b>Anys 60</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/aeri2.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/aeri1.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Vista aèrea de l'escola</h6>
                                        <span>Any 1980</span>
                                    </div>
                                    <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3><strong>On som?</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            El nostre fundador va començar a construir i traslladar tot el complex escolar que dirigia, als terrenys de l’Avinguda Barcelona-Carrer Capellades, on actualment es troba el Col·legi Monalco.
                        </div>

                        <p class="template-margin-top-3">
                            L’actual escola es realitzà en diverses fases i s’ha anat ampliant al llarg dels anys. I sempre està en constant renovació. Actualment està en procés d'innovació de noves aules i espais.
<br>L’escola fou construïda d’acord amb la idea que l’arquitectura escolar ha de ser com
l’educació: comunitària, oberta i harmònica, i dotada, alhora, de les condicions
adequades per a una formació integral dels nostres alumnes.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">1999 - Actualitat </i></a>

                    </div>

                </div>

            </div>

        </div> </div>

    <!-- Section -->
   <!-- 
 <div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">
 -->

        <!-- Main -->
       <!--  <div class="template-main template-section-white"> -->

            <!-- Feature -->
            <!-- 
<div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
                <ul class="template-layout-25x25x25x25 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-teddy-alt"></div>
                        <h5>Morbi Etos</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                    <li class="template-layout-column-center-left">
                        <div class="template-icon-feature template-icon-feature-name-heart-alt"></div>
                        <h5>Congue Gravida</h5>
                        <p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna etos interdum vitae est.</p>
                    </li>	
                    <li class="template-layout-column-center-right">
                        <div class="template-icon-feature template-icon-feature-name-graph-alt"></div>
                        <h5>Maecenas Node</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-globe-alt"></div>
                        <h5>Placerat Etos</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                </ul>
            </div>

        </div>
 -->

    </div>

    <!-- Section -->
   <!-- 
 <div class="template-content-section template-padding-bottom-5 template-padding-top-reset">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Header and subheader ~~>
            <div class="template-component-header-subheader">
                <h2>Notícies</h2>
                <h6>El nostre blog t'informarà de tot el que fem</h6>
                <div></div>
            </div>	

            <!~~ Recent post ~~>
            <div class="template-component-recent-post template-component-recent-post-style-1">

                <!~~ Layout 33x33x33 ~~>
                <ul class="template-layout-33x33x33 template-clear-fix">

                    <!~~ Left column ~~>
                    <li class="template-layout-column-left">
                        <div class="template-component-recent-post-date">October 03, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/7.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">12</div>
                        </div>
                        <h5><a href="#">Drawing and Painting Lessons</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Events</a>,
                                <a href="#">Fun</a>
                            </li>
                        </ul>
                    </li>

                    <!~~ Center column ~~>
                    <li class="template-layout-column-center">
                        <div class="template-component-recent-post-date">October 03, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/2.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">4</div>
                        </div>
                        <h5><a href="#">Fall Parents Meeting Day</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Dance</a>,
                                <a href="#">Education</a>
                            </li>
                        </ul>			
                    </li>

                    <!~~ Right column ~~>
                    <li class="template-layout-column-right">
                        <div class="template-component-recent-post-date">September 20, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/9.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">4</div>
                        </div>
                        <h5><a href="#">Birthday in Kindergarten</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Games</a>,
                                <a href="#">General</a>
                            </li>
                        </ul>			
                    </li>
 -->

                </ul>

            </div>

        </div>

    </div>

</div>