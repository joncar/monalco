<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Serveis</h1>
                <h6>Tot allò que t'oferim</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/psicoicon.png') ?>" style="width:120px"></center>
                <h2>Servei psicopedagògic</h2>                
                <div></div>
                <p class="template-margin-top-3" id="mce_17">
                L’escola disposa d’un equip psicopedagògic amb l’objectiu principal d’assessorar mestres i famílies en el procés d’aprenentatge dels alumnes.
                Es pretén un acompanyament personalitzat que atengui la diversitat i la singularitat, prioritzant els alumnes que presenten més dificultats d’aprenentatge, elaborant eines específiques que permetin un progrés adequat. 
                Assessorament en l’orientació acadèmica i professional. 

            </p>
            </div>

        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4>Funcions</h4>

                <!-- 
<p>Al matí, els infants van arribant a l'acollida, on tenen opció d'esmorzar si a casa encara no ho han fet, i tenen diverses propostes: material de dibuix i manualitats, jocs de taula, jocs de construcció, trens, cotxes, animals, etc. A partir de les 8:30, els que vulguin poden sortir al jardí amb un dels acompanyants i poden fer ús de les instal·lacions: sorral, pista, caseta, etc. A les 8:55h acompanyem als infants als ambients.</p>
 -->

                <!-- List -->
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Valoració de necessitats educatives de l’alumnat en l’àmbit acadèmic i escolar i concreció de propostes d’intervenció educativa (dificultats acadèmiques, trastorns d’aprenentatge, retards en el desenvolupament, etc.). 

</li>
                        <li>Oferir orientació, suport i recursos en relació amb els processos d’ensenyament i aprenentatge</li>
                        <li>Atenció directa a l’alumne amb necessitats educatives específiques a dins i fora de l’aula, en petits grups, d’acord amb el criteri de prioritat establert per l’escola</li>
                        <li>Desenvolupament de programes preventius i de detecció i elaboració de protocols i materials adients</li>
                        <li>Coordinació amb la Comissió d’Atenció a la Diversitat (CAD)</li>			
                        <li>Coordinació de les actuacions dels serveis externs:	Professionals que intervenen en l’atenció a l’alumnat (psicòlegs, psicopedagogs, etc.) i coordinació amb els Serveis Educatius del Departament d’Ensenyament: EAP, SEETDIC, SEEM, CREDA, etc.
</li>														
                    </ul>
                </div>

                <!-- 
Vertical grid                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu de 8:00 a 9:00:</div>
                            <div>3,50€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu de 8:30 a 9:00:</div>
                            <div>2€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>Preu fixe mensual de 8h a 9h:</div>
                            <div>48€</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Preu fixe mensual de 8:30h a 9h:</div>
                            <div>25€</div>
                        </li>
                    </ul>
                </div>
 -->

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/ps1.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/ps1.jpg" alt=""/>
                        <!-- 
<img src="<?= base_url() ?>img/_sample/690x506/5.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/5.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/11.jpg" alt=""/>
 -->
                    </div>
                </div>

            </div>

        </div>

    </div>

<!-- 
    Section    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        Main        <div class="template-main">

            Feature            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-app-alt"></div>
                        <h5>Magento Care</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pin-alt"></div>
                        <h5>Nunc Blandit</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-piano-alt"></div>
                        <h5>Mauris Solutide</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>
                </ul>
            </div>
 -->
<!-- 

            <!~~ Feature ~~>
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-people"></div>
                        <h5>Quisque Morbi</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pencil"></div>
                        <h5>Libro Retrum</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-paintbrush"></div>
                        <h5>Phasellus Novum</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>
                </ul>
            </div>

        </div>
 -->

    </div>

    <!-- Section -->
  <!-- 
  <div class="template-content-section template-background-image template-background-image-4">
        <div class="template-main">

            <!~~ Testimonials ~~>
            <div class="template-section-white">
                <div class="template-component-testimonial template-component-testimonial-style-2">
                    <ul class="template-layout-100">
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>Fable Kindergarten is a great place for my daughter to start her schooling experience. It’s welcoming and safe and my daughter loves being there.</p>
                            <div></div>
                            <span>Fredric Greene</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>I have a 1 year old and a 5 year old who have been attending for a year now. I can not tell you how much I adore and appreciate all of the wonderful staff.</p>
                            <div></div>
                            <span>Patricia Morgan</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>I have to say that I have 2 children ages 5 and 2 and have used various daycare’s in Kindergartens and this is by far the very best I have ever used.</p>
                            <div></div>
                            <span>Joann Simms</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>Fable Kindergarten is a great place for my daughter to start her schooling experience. It’s welcoming and safe and my daughter loves being there.</p>
                            <div></div>
                            <span>Shelia Perry</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>This letter is to recognize you and your staff for doing an excellent job teaching my son. His skill level is significantly better since attending Fable.</p>
                            <div></div>
                            <span>Tony I. Robinette</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>I have to say that I have 2 children ages 5 and 2 and have used various daycare’s in Kindergartens and this is by far the very best I have ever used.</p>
                            <div></div>
                            <span>Claire Willmore</span>
                        </li>
                    </ul>
                    <div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>			
 -->

  <!-- 
      </div>
    </div>

    <!~~ Section ~~>
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Layout 50x50 ~~>
            <div class="template-layout-50x50 template-clear-fix">

                <!~~ Left column ~~>
                <div class="template-layout-column-left">

                    <!~~ Accordion ~~>
                    <div class="template-component-accordion">
                        <h6><a href="#">Educational Field Trips and Presentations</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Reporting on Individual Achievement</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>										
                        <h6><a href="#">Writing and Reading Classes</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Scientific Investigation Opportunities</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>
                    </div>
 -->
<!-- 

                </div>

                <!~~ Right column ~~>
                <div class="template-layout-column-right">

                    <!~~ Counters list ~~>
                    <div class="template-component-counter-list">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Art Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">165</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Writing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">125</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Drawing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">52</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left template-margin-bottom-reset">
                                <span class="template-component-counter-list-label">Yoga Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">75</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                        </ul>
                    </div>

                </div>
 -->

            </div>

        </div>

    </div>
<!-- 

    <!~~ Section ~~>
    <div class="template-content-section template-main template-padding-top-reset template-padding-bottom-5">

        <!~~ Header and subheader ~~>
        <div class="template-component-header-subheader">
            <h2>Class Educators</h2>
            <h6>With education and experience in early childhood care</h6>
            <div></div>
        </div>

        <!~~ Team ~~>
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix">
                <li class="template-layout-column-left">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/11.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/11.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Linda Moore</h6>
                                    <span>Teacher</span>
                                </div>
                                <p><b>Linda Moore</b> Teacher</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">I hold a degree in Early Childhood Education and an advanced English language certificate. I have been working as a kindergarten teacher since 2002.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                </ul>
                            </div>					
                        </li>
                    </ul>			
                </li>
                <li class="template-layout-column-right">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/2.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/2.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Claire Willmore</h6>
                                    <span>Teacher</span>
                                </div>
                                <p><b>Claire Willmore</b> Teacher</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">I have just finished my studies in Early Childhood Education, and I am also the kid’s yoga teacher here at Fable. I enjoy cooking, swimming and bike riding in my free time.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                </ul>
                            </div>						
                        </li>
                    </ul>				
                </li>
            </ul>
        </div>

    </div>
 -->

    <!-- Section -->
    <!-- 
<div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">
        <div class="template-main">

            <!~~ Call to action ~~>
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!~~ Content ~~>
                <div class="template-component-call-to-action-content">

                    <!~~ Left column ~~>
                    <div class="template-component-call-to-action-content-left">

                        <!~~ Header ~~>
                        <h3>Vols contactar amb l'equip psicopedagògic</h3>

                    </div>

                    <!~~ Right column ~~>
                    <div class="template-component-call-to-action-content-right">

                        <!~~ Button ~~>
                        <a href="#" class="template-component-button template-component-button-style-1">Contactar aquí<i></i></a>

                    </div>									

                </div>

            </div>				

        </div>

    </div>
 -->

</div>