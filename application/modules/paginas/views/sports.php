<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Serveis</h1>
                <h6>Tot allò que t'oferim</h6>                
            </div>
        </div>

    </div>
</div>
<div class="template-content">

			<!-- Section -->
			<div class="template-content-section template-padding-bottom-5 template-main">

				<div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/esporticon.png') ?>" style="width:120px"></center>
                <h2>Equips</h2>                
                <div></div>
            </div>
				
				<!-- Gallery -->
				<div class="template-component-gallery">
					<ul class="template-layout-50x50 template-clear-fix">
						
						[foreach:sports]
						<li class="template-layout-column-[position]">
							<div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
								<a href="[foto]" data-fancybox-group="gallery-1">
									<img src="[foto]" alt="" />
									<span><span><span></span></span></span>
								</a>
								<div>
									<h6 style="float:left; margin-top: 16px;">[nombre]</h6>
									<div style="float:right; text-align: left">
										[enlace1]
										[enlace2]
										[enlace3]
									</div>
								</div>
								<p><b>[nombre]</b></p>
							</div>
						</li>
						[/foreach]

					</ul>
				</div>
				
			</div>

			<!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <h2>Notícies d'Esports</h2>
            <h6>T'informem de les activitats que es realitzen a Esports</h6>
            <div></div>
        </div>
    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">
            <div class="template-main">
		            <!-- Recen post -->
		            <div class="template-component-recent-post template-component-recent-post-style-1">
		                <ul class="template-layout-33x33x33 template-clear-fix">
		                    [foreach:blog]
		                    <li class="template-layout-column-[posicion]" style="visibility: visible;">
		                        <div class="template-component-recent-post-date">[fecha]</div>
		                        <div class="template-component-image">
		                            <a href="[link]" id="mce_266" class="">
		                                <img src="[foto]" alt=""> 
		                                <span id="mce_267" class=""><span id="mce_268">
		                                        <span id="mce_269">
		                                            
		                                        </span>
		                                    </span>
		                                </span>
		                            </a>		                            
		                        </div>
		                        <h5 id="mce_270" class="">
		                            <a href="[link]" id="mce_271" data-mce-href="#" class="">
		                                [titulo]
		                            </a>
		                        </h5>
		                        <p id="mce_272" class="">
		                            [descripcion]
		                        </p>
		                        <ul class="template-component-recent-post-meta template-clear-fix">
		                            <li class="template-icon-blog template-icon-blog-author">
		                                <a href="[link]" id="mce_273" class="">[user]</a>
		                            </li>
		                            <li class="template-icon-blog template-icon-blog-category">
		                                <a href="[link]" id="mce_274" class="">Events</a>,
		                                <a href="[link]" id="mce_275" class="">Fun</a>
		                            </li>
		                        </ul>
		                    </li>
		                    [/foreach]
		                 </ul>
		             </div>  

		        </div>

		    </div>

		    </div>

		</div>
        