
<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>About Us I</h1>
                <h6>About Fable</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/menjador.png') ?>" style="width:120px"></center>
                <h2>Escola Verda</h2>                
                <div></div>
            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3>Més de 80 anys aprenent junts</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            Una escola familiar, acollidora, innovadora, inclusiva, competencial.

                        </div>

                        <p class="template-margin-top-3">
                            Praesent arcu gravida a vehicula est node maecenas loareet maecenas morbi dosis luctus mode. Urna eget lacinia eleifend molibden dosis et gravida dosis sit amet terminal.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-centre">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/1.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/1.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Play Time In Kindergarten</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Play Time In Kindergarten</b> Tenderhearts Class</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/7.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/7.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Drawing and Painting Lessons</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3>Friendly atmosphere plus <strong>quality children care.</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            Nulla adiscipling elite forte, nodis est advance pulvinar maecenas est dolor, novum elite lacina.
                        </div>

                        <p class="template-margin-top-3">
                            Praesent arcu gravida a vehicula est node maecenas loareet maecenas morbi dosis luctus mode. Urna eget lacinia eleifend molibden dosis et gravida dosis sit amet terminal.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3>Dedicated classrooms with <strong>top skilled educators.</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            Nulla adiscipling elite forte, nodis est advance pulvinar maecenas est dolor, novum elite lacina.
                        </div>

                        <p class="template-margin-top-3">
                            Praesent arcu gravida a vehicula est node maecenas loareet maecenas morbi dosis luctus mode. Urna eget lacinia eleifend molibden dosis et gravida dosis sit amet terminal.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/1.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/1.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Outdoor Activity During Recess</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
                <ul class="template-layout-25x25x25x25 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-teddy-alt"></div>
                        <h5>Morbi Etos</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                    <li class="template-layout-column-center-left">
                        <div class="template-icon-feature template-icon-feature-name-heart-alt"></div>
                        <h5>Congue Gravida</h5>
                        <p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna etos interdum vitae est.</p>
                    </li>	
                    <li class="template-layout-column-center-right">
                        <div class="template-icon-feature template-icon-feature-name-graph-alt"></div>
                        <h5>Maecenas Node</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-globe-alt"></div>
                        <h5>Placerat Etos</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                </ul>
            </div>

        </div>

    </div>

    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Tab -->
            <div class="template-component-tab">
                <ul>
                    <li>
                        <a href="#template-tab-1">Class Teachers</a>
                        <span></span>
                    </li>
                    <li>
                        <a href="#template-tab-2">Class Leaders</a>
                        <span></span>
                    </li>
                    <li>
                        <a href="#template-tab-3">From The Blog</a>
                        <span></span>
                    </li>
                </ul>
                <div id="template-tab-1">

                    <!-- Team -->
                    <div class="template-component-team template-component-team-style-3">
                        <ul class="template-layout-25x25x25x25 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/525x560/11.jpg" data-fancybox-group="team-2">
                                        <img src="<?= base_url() ?>img/_sample/525x560/11.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Ruth Richie</h6>
                                        <span>Teacher</span>
                                    </div>
                                    <p><b>Ruth Richie</b> Teacher</p>
                                </div>
                                <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                    <ul>
                                        <li><a href="#" class="template-component-social-icon-skype"></a></li>
                                        <li><a href="#" class="template-component-social-icon-soundcloud"></a></li>
                                        <li><a href="#" class="template-component-social-icon-spotify"></a></li>
                                    </ul>
                                </div>	
                            </li>
                            <li class="template-layout-column-center-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/525x560/2.jpg" data-fancybox-group="team-2">
                                        <img src="<?= base_url() ?>img/_sample/525x560/2.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Katie Willmore</h6>
                                        <span>Assistant Teacher</span>
                                    </div>
                                    <p><b>Katie Willmore</b> Assistant Teacher</p>
                                </div>		
                                <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                    <ul>
                                        <li><a href="#" class="template-component-social-icon-stumbleupon"></a></li>
                                        <li><a href="#" class="template-component-social-icon-technorati"></a></li>
                                        <li><a href="#" class="template-component-social-icon-tumblr"></a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="template-layout-column-center-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/525x560/12.jpg" data-fancybox-group="team-2">
                                        <img src="<?= base_url() ?>img/_sample/525x560/12.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Angelica Watson</h6>
                                        <span>Lead Teacher</span>
                                    </div>
                                    <p><b>Angelica Watson</b> Lead Teacher</p>
                                </div>	
                                <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                    <ul>
                                        <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                        <li><a href="#" class="template-component-social-icon-vimeo"></a></li>
                                        <li><a href="#" class="template-component-social-icon-wykop"></a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/525x560/13.jpg" data-fancybox-group="team-2">
                                        <img src="<?= base_url() ?>img/_sample/525x560/13.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Angela Lynn</h6>
                                        <span>Teacher</span>
                                    </div>
                                    <p><b>Angela Lynn</b> Teacher</p>
                                </div>	
                                <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                    <ul>
                                        <li><a href="#" class="template-component-social-icon-behance"></a></li>
                                        <li><a href="#" class="template-component-social-icon-xing"></a></li>
                                        <li><a href="#" class="template-component-social-icon-youtube"></a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>	

                </div>
                <div id="template-tab-2">

                    <!-- Team -->
                    <div class="template-component-team template-component-team-style-2">
                        <ul class="template-layout-50x50 template-clear-fix">
                            <li class="template-layout-column-left">
                                <ul class="template-layout-50x50 template-clear-fix">
                                    <li class="template-layout-column-left">
                                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                            <a href="<?= base_url() ?>img/_sample/525x560/11.jpg" data-fancybox-group="team-2">
                                                <img src="<?= base_url() ?>img/_sample/525x560/11.jpg" alt="" />
                                                <span><span><span></span></span></span>
                                            </a>
                                            <div>
                                                <h6>Ruth Richie</h6>
                                                <span>Teacher</span>
                                            </div>
                                            <p><b>Ruth Richie</b> Teacher</p>
                                        </div>					
                                    </li>
                                    <li class="template-layout-column-right">
                                        <div class="template-component-team-quote"></div>
                                        <p class="template-component-team-description">I have just finished my studies in Early Childhood Education, and I am also the kid’s yoga teacher here at Fable. I enjoy cooking, swimming and bike riding in my free time.</p>
                                        <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                            <ul>
                                                <li><a href="#" class="template-component-social-icon-forrst"></a></li>
                                                <li><a href="#" class="template-component-social-icon-foursquare"></a></li>
                                                <li><a href="#" class="template-component-social-icon-friendfeed"></a></li>
                                            </ul>
                                        </div>					
                                    </li>
                                </ul>			
                            </li>
                            <li class="template-layout-column-right">
                                <ul class="template-layout-50x50 template-clear-fix">
                                    <li class="template-layout-column-left">
                                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                            <a href="<?= base_url() ?>img/_sample/525x560/2.jpg" data-fancybox-group="team-2">
                                                <img src="<?= base_url() ?>img/_sample/525x560/2.jpg" alt="" />
                                                <span><span><span></span></span></span>
                                            </a>
                                            <div>
                                                <h6>Katie Willmore</h6>
                                                <span>Assistant Teacher</span>
                                            </div>
                                            <p><b>Katie Willmore</b> Assistant Teacher</p>
                                        </div>					
                                    </li>
                                    <li class="template-layout-column-right">
                                        <div class="template-component-team-quote"></div>
                                        <p class="template-component-team-description">My name is Katie. I grew up and studied in Canada. This is my second year at Fable and love every minute of it, making the children’s learning experience fun.</p>
                                        <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                            <ul>
                                                <li><a href="#" class="template-component-social-icon-googleplus"></a></li>
                                                <li><a href="#" class="template-component-social-icon-instagram"></a></li>
                                                <li><a href="#" class="template-component-social-icon-linkedin"></a></li>
                                            </ul>
                                        </div>						
                                    </li>
                                </ul>				
                            </li>
                            <li class="template-layout-column-left">
                                <ul class="template-layout-50x50 template-clear-fix">
                                    <li class="template-layout-column-left">
                                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                            <a href="<?= base_url() ?>img/_sample/525x560/12.jpg" data-fancybox-group="team-2">
                                                <img src="<?= base_url() ?>img/_sample/525x560/12.jpg" alt="" />
                                                <span><span><span></span></span></span>
                                            </a>
                                            <div>
                                                <h6>Angelica Watson</h6>
                                                <span>Lead Teacher</span>
                                            </div>
                                            <p><b>Angelica Watson</b> Lead Teacher</p>
                                        </div>					
                                    </li>
                                    <li class="template-layout-column-right">
                                        <div class="template-component-team-quote"></div>
                                        <p class="template-component-team-description">I hold a degree in Early Childhood Education and an advanced English language certificate. I have been working as a kindergarten teacher since 2002.</p>
                                        <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                            <ul>
                                                <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                                <li><a href="#" class="template-component-social-icon-myspace"></a></li>
                                                <li><a href="#" class="template-component-social-icon-picasa"></a></li>
                                            </ul>
                                        </div>						
                                    </li>
                                </ul>				
                            </li>
                            <li class="template-layout-column-right">
                                <ul class="template-layout-50x50 template-clear-fix">
                                    <li class="template-layout-column-left">
                                        <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                            <a href="<?= base_url() ?>img/_sample/525x560/13.jpg" data-fancybox-group="team-2">
                                                <img src="<?= base_url() ?>img/_sample/525x560/13.jpg" alt="" />
                                                <span><span><span></span></span></span>
                                            </a>
                                            <div>
                                                <h6>Angela Lynn</h6>
                                                <span>Teacher</span>
                                            </div>
                                            <p><b>Angela Lynn</b> Teacher</p>
                                        </div>					
                                    </li>
                                    <li class="template-layout-column-right">
                                        <div class="template-component-team-quote"></div>
                                        <p class="template-component-team-description">I have completed a Graduate Diploma in Early Childhood Teaching. I have worked with children aged from six weeks to eight years. This is my second year at Fable.</p>
                                        <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                            <ul>
                                                <li><a href="#" class="template-component-social-icon-pinterest"></a></li>
                                                <li><a href="#" class="template-component-social-icon-reddit"></a></li>
                                                <li><a href="#" class="template-component-social-icon-rss"></a></li>
                                            </ul>
                                        </div>						
                                    </li>
                                </ul>				
                            </li>
                        </ul>
                    </div>	

                </div>
                <div id="template-tab-3">

                    <!-- Recen post -->
                    <div class="template-component-recent-post template-component-recent-post-style-1">
                        <ul class="template-layout-33x33x33 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-recent-post-date">October 03, 2014</div>
                                <div class="template-component-image">
                                    <a href="#">
                                        <img src="<?= base_url() ?>img/_sample/690x414/7.jpg" alt=""/>
                                    </a>
                                    <div class="template-component-recent-post-comment-count">12</div>
                                </div>
                                <h5><a href="#">Drawing and Painting Lessons</a></h5>
                                <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                                <ul class="template-component-recent-post-meta">
                                    <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                                    <li class="template-icon-blog template-icon-blog-category">
                                        <a href="#">Events</a>,
                                        <a href="#">Fun</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="template-layout-column-center">
                                <div class="template-component-recent-post-date">October 03, 2014</div>
                                <div class="template-component-image">
                                    <a href="#">
                                        <img src="<?= base_url() ?>img/_sample/690x414/2.jpg" alt=""/>
                                    </a>
                                    <div class="template-component-recent-post-comment-count">4</div>
                                </div>
                                <h5><a href="#">Fall Parents Meeting Day</a></h5>
                                <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                                <ul class="template-component-recent-post-meta">
                                    <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                                    <li class="template-icon-blog template-icon-blog-category">
                                        <a href="#">Dance</a>,
                                        <a href="#">Education</a>
                                    </li>
                                </ul>			
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-component-recent-post-date">September 20, 2014</div>
                                <div class="template-component-image">
                                    <a href="#">
                                        <img src="<?= base_url() ?>img/_sample/690x414/9.jpg" alt=""/>
                                    </a>
                                    <div class="template-component-recent-post-comment-count">4</div>
                                </div>
                                <h5><a href="#">Birthday in Kindergarten</a></h5>
                                <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                                <ul class="template-component-recent-post-meta">
                                    <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                                    <li class="template-icon-blog template-icon-blog-category">
                                        <a href="#">Games</a>,
                                        <a href="#">General</a>
                                    </li>
                                </ul>			
                            </li>
                        </ul>
                    </div>	

                </div>
            </div>

        </div>

    </div>

</div>