<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom">

            <div class="template-header-bottom-background template-header-bottom-background-img-8 template-header-bottom-background-style-1">
                <div class="template-main">
                    <h1>Vídeos</h1>
                    <h6>Recull multimèdia de la nostra escola</h6>
                </div>
            </div>

        </div>

    </div>
</div>

<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        
        <!-- Tabs -->
        <div class="template-component-tab">

            <!-- Navigation -->
            <ul>
                <?php foreach($this->db->get_where('categoria_videos')->result() as $n): ?>
                    <li>
                        <a href="#cat<?= $n->id ?>"><?= $n->nombre ?></a>
                        <span></span>
                    </li>
                <?php endforeach ?>
                
            </ul>

            <?php foreach($this->db->get_where('categoria_videos')->result() as $n): ?>

                <!-- Tab #1 -->
                <div id="cat<?= $n->id ?>">

                    <!-- Gallery -->
                    <div class="template-component-gallery">

                        <!-- Layout 50x50 -->
                        <ul class="template-layout-50x50 template-clear-fix">
                            <?php $this->db->order_by('orden','ASC'); ?>
                            <?php foreach($this->db->get_where('videos',array('categoria_videos_id'=>$n->id))->result() as $nn=>$v): ?>
                            <!-- Left column -->
                                <li class="template-layout-column-<?= $nn%2==0?'left':'right' ?>">
                                    <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                        <a href="https://www.youtube.com/embed/5wNlTQnv9A0?rel=0&amp;controls=0&amp;showinfo=0" data-fancybox-group="gallery-1" data-fancybox-type="iframe" >
                                            <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" alt="" />
                                            <span><span><span></span></span></span>
                                        </a>
                                        <div>
                                            <h6><?= $v->titulo ?></h6>
                                            <span><?= $v->subtitulo ?></span>
                                        </div>
                                        <p><b><?= $v->titulo ?></b></p>
                                    </div>
                                </li>
                             <?php endforeach ?>

                        </ul>

                    </div>	

                </div>
            <?php endforeach ?>


        </div>				

    </div>

    <!-- Section -->
    <!-- 
<div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Call to action ~~>
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!~~ Content ~~>
                <div class="template-component-call-to-action-content">

                    <!~~ Left column ~~>
                    <div class="template-component-call-to-action-content-left">

                        <!~~ Header ~~>
                        <h3>Vols contactar amb la nostra escola?</h3>

                    </div>

                    <!~~ Right column ~~>
                    <div class="template-component-call-to-action-content-right">

                        <!~~ Button ~~>
                        <a href="#" class="template-component-button template-component-button-style-1">Contacta aquí<i></i></a>

                    </div>									
 -->

                </div>

            </div>				

        </div>

    </div>

</div>