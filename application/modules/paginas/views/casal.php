<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1><?= l('casal_serveis') ?></h1>
                <h6><?= l('casal_serveis2') ?></h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
        <div class="template-content">
            <div class="template-content">

            <!-- Section -->
            <div class="template-content-section template-padding-bottom-5 template-main">
                <div class="template-component-header-subheader">
                <center>
                    <img src="<?= base_url('img/icon/casalicon.png') ?>" style="width:120px">
                </center>
                <h2><?= l('casal_serveis3') ?></h2>                
                <div></div>
            </div>

                

                <!-- Layout 50x50 -->
                <div class="template-layout-50x50 template-clear-fix">

                    <!-- Left column -->
                    <div class="template-layout-column-left">
                     <h2 id="mce_136" class=""><?= l('casal_serveis4') ?></h2>

                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left" style="visibility: visible;">
                                <p class="template-component-dropcap template-component-dropcap-style-1">
                               
                                    <?= l('casal_serveis5') ?>
                                </p>
                            </li>
                        </ul>

                        <!-- Header -->
                        <?php 
                            $precios = $this->db->get_where('casal_precios');
                            if($precios->num_rows()>0): 
                        ?>
                            <h4 id="mce_138" class=""><?= l('casal_serveis6') ?></h4>
                            
                            <table class="preutable">
                                <thead>
                                    <tr class="odd">
                                        <th></th>
                                        <th><b></b><?= l('casal_serveis7') ?></th>
                                        <!-- <th><b>MATINS O TARDES</b></th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($precios->result() as $p): ?>
                                        <tr>
                                            <td><?= $p->nombre ?></td>
                                            <td style="text-align: center"><?= $p->precio ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>

                        <?php endif ?>

                    </div>

                    <?php 
                        $this->db->order_by('orden','ASC');
                        $precios = $this->db->get_where('casal_fotos');
                        if($precios->num_rows()>0): 
                    ?>
                        <!-- Right column -->
                        <div class="template-layout-column-right">
                            
                            <!-- Nivo slider -->
                            <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                                <div>
                                    <?php foreach($precios->result() as $p): ?>
                                        <img src="<?= base_url() ?>img/_sample/690x506/<?= $p->foto ?>" data-thumb="<?= base_url() ?>img/_sample/690x506/<?= $p->foto ?>" alt="">
                                <?php endforeach ?>
                                </div>
                            </div>
                            
                        </div>
                    <?php endif ?>

                </div>

            </div>

            <!-- Section -->
            

            <!-- Section -->
     <!-- Section -->
     <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">            

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-4 template-component-feature-position-top template-component-feature-size-large" style="display: block;">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left" style="visibility: visible;">
                        <div class="template-icon-feature template-icon-feature-name-teddy-alt template-icon-feature-size-large" style="background:url([base_url]img/icon/feature/tiny/casal3.png); background-size:100%"></div>
                        <?= l('casal_serveis10') ?>
                    </li>
                    <li class="template-layout-column-center" style="visibility: visible;">
                        <div class="template-icon-feature template-icon-feature-name-blocks-alt template-icon-feature-size-large" style="background:url([base_url]img/icon/feature/tiny/casal2.png); background-size:100%"></div>
                        <?= l('casal_serveis11') ?>
                    </li>   
                    <li class="template-layout-column-right" style="visibility: visible;">
                        <div class="template-icon-feature template-icon-feature-name-globe-alt template-icon-feature-size-large" style="background:url([base_url]img/icon/feature/tiny/casal1.png); background-size:100%"></div>
                        <?= l('casal_serveis12') ?>
                    </li>
                </ul>
            </div>                          

        </div>

    </div>

            <!-- Section -->
            

            <!-- Section -->
            

            




            <!-- Section -->
            <div class="template-content-section template-content-layout template-content-layout-sidebar-right template-main template-clear-fix">

                <!-- Content -->
                <div>

                    <!-- Header -->
                    <h5><?= l('casal_serveis13') ?></h5>

                    <!-- Section -->
                    <div data-id="section-1">
                        

                        <!-- Tabs -->
        <div class="template-component-tab">

            [_cursos]



        </div>  








                    </div>                    
                </div>
            </div>
        </div>         