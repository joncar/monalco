
<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom">

            <div class="template-header-bottom-background template-header-bottom-background-img-8 template-header-bottom-background-style-1">
                <div class="template-main">
                    <h1>Galeria fotogràfica</h1>
                    <h6>l'escola en imatges</h6>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">

        <div class="template-layout-50x50">
            <!-- Tabs -->
            <div class="template-layout-column-left">

                <!-- Navigation -->
                <ul>
                	<?php
                        $gal =  $this->elements->categoria_fotos_privada();
                        foreach($gal->result() as $g): ?>
    	                <li>
    	                    <a href="<?= base_url('zona-privada') ?>?cat=<?= $g->id ?>"><?= $g->nombre ?></a>
    	                    <span></span>
    	                </li>
                	<?php endforeach ?>
                </ul>

            </div>

            
            <div class="template-layout-column-right">
                <div class="template-component-gallery">
                    <ul class="template-layout-25x25x25 template-clear-fix">
                        <?php $x = 0; $cat = empty($_GET['cat'])?$gal->row()->id:$_GET['cat']; ?>
                        <?php foreach($this->elements->galeria_privada(array('categoria_galeria_id'=>$cat))->result() as $f): ?>
                        <?php 
                            switch($x){
                                case 0: $posicion = 'left'; break;                                  
                                case 1: $posicion = 'center'; break;
                                case 2: $posicion = 'right'; break;
                            }
                            $x++; 
                            $x = $x>2?0:$x;
                        ?>
                        <!-- Left column -->
                        <li class="template-layout-column-<?= $posicion ?>">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= $f->foto ?>" data-fancybox-group="gallery-1">
                                    <img src="<?= $f->foto ?>" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6><?= @explode('|',$f->titulo)[0] ?></h6>
                                    <span><?= @explode('|',$f->titulo)[1] ?></span>
                                </div>                                  
                            </div>
                        </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Section -->
    <!-- 
<div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Call to action ~~>
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!~~ Content ~~>
                <div class="template-component-call-to-action-content">

                    <!~~ Left column ~~>
                    <div class="template-component-call-to-action-content-left">

                        <!~~ Header ~~>
                        <h3 style="font-size: 38px;">Vols informació de la nostra escola?</h3>

                    </div>

                    <!~~ Right column ~~>
                    <div class="template-component-call-to-action-content-right">

                        <!~~ Button ~~>
                        <a href="#" class="template-component-button template-component-button-style-1">Demanar informació<i></i></a>

                    </div>									

                </div>

            </div>				

        </div>
 -->

    </div>

</div>