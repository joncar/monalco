<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Resultats de la búsqueda</h1>                
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- 
<div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/menjador.png') ?>" style="width:120px"></center>
                <h2>Escola Verda</h2>                
                <div></div>
            </div>
 -->

        	<?php foreach($resultados->result() as $r): ?>

        		<div class="template-component-notice template-component-notice-style-1" style="margin-bottom: 20px">
					<!-- Content -->
					<div class="template-component-notice-content">
						<!-- Left column -->
						<div class="template-component-notice-content-left">
							<!-- Icon -->
							<div class="template-icon-feature template-icon-feature-size-medium template-icon-feature-name-tick-alt"></div>
						</div>
						<!-- Right column -->
						<div class="template-component-notice-content-right">
							<!-- Header -->
							<h6><a href="<?= base_url($r->url.'/'.toUrl($r->id.'-'.$r->texto1)) ?>" style="color:#e5027d"><?= str_replace($q,'<span  style="background:#c52a17; color:white">'.$q.'</span>',$r->texto1) ?></a></h6>
							<!-- Text -->
                            <?php 
                                $texto2 = strip_tags($r->texto2);
                                $texto2 = substr($texto2,0,255);
                            ?>
							<p><?= str_replace($q,'<span style="background:#c52a17; color:white">'.$q.'</span>',$texto2) ?></p>
							<!-- Close link -->
							<a href="<?= base_url($r->url.'/'.toUrl($r->id.'-'.$r->texto1)) ?>">Veure més</a>
						</div>		
					</div>
				</div>
        	<?php endforeach ?>

        </div>

    </div>

    

    </div><div style="text-align:center;"></div><div style="text-align:center;"></div>


</div>
        