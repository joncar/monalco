<div class="template-header">
    <!-- Top header -->
    
<div class="template-header-top">

    <div class="template-main template-clear-fix">

        <div class="template-header-top-logo">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url() ?>img/logo_header.png" alt="" />
            </a>
        </div>

        <div class="template-header-top-menu template-clear-fix">   
            [menu]
        </div>

    </div>
</div></div>
<!-- Content-->
<div class="template-content" style="margin-bottom: 0px;">

    <!-- Slider -->
			<div class="template-content-section template-padding-reset">
								
				<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">

					<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.1.6">

						<ul>	
							<?php 
								$this->db->order_by('orden','ASC');
								$this->db->where('activo',1);
								foreach($this->db->get_where('banner')->result() as $n=>$b): ?>
								<li data-index="rs-<?= $n ?>" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?= base_url('img/'.$b->foto) ?>" data-rotate="0" data-saveperformance="off" class="fable_slide_1_class" id="fable_slide_1_id" data-title="fable_slide_1" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
									<img src="<?= base_url('img/'.$b->foto) ?>" alt="" data-bgposition="<?= $b->bg_position ?>" data-bgfit="<?= $b->tipo_background ?>" class="rev-slidebg" data-no-retina="" width="100%" height="600">
									<div class="tp-caption tp-font-lato-black tp-resizeme" id="slide-1-layer-4" data-x="<?= $b->align_x ?>" data-hoffset="0" data-y="<?= $b->align_y ?>" data-voffset="<?= $b->voffset ?>" data-width="auto" data-height="auto" data-transform_idle="" data-transform_in="x:0;y:150;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" data-start="1600"  data-responsive_offset="on" data-elementdelay="0.1" data-endelementdelay="0.1" style="<?= $b->style ?>"><?= $b->texto ?></div>
								</li>
							<?php endforeach ?>

						</ul>

						<div class="tp-bannertimer tp-bottom" style="visibility:hidden !important;"></div>	

					</div>

					<script type="text/javascript">

						var setREVStartSize=function()
						{
							try
							{
								var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
								e.c=jQuery('#rev_slider_1_1');
								e.gridwidth=[1250];
								e.gridheight=[600];

								e.sliderLayout="fullwidth";
								if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
							}
							catch(d)
							{
								console.log("Failure at Presize of Slider:"+d)
							}
						};

						setREVStartSize();
						function revslider_showDoubleJqueryError(sliderID) 
						{
							var errorMessage="Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
							errorMessage+="<br> This includes make eliminates the revolution slider libraries, and make it not work.";
							errorMessage+="<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
							errorMessage+="<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
							errorMessage="<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
							jQuery(sliderID).show().html(errorMessage);
						}

						var tpj=jQuery;

						tpj.noConflict();

						var revapi1;

						tpj(document).ready(function() 
						{
							if(tpj("#rev_slider_1_1").revolution==undefined)
							{
								revslider_showDoubleJqueryError("#rev_slider_1_1");
							}
							else
							{
								revapi1=tpj("#rev_slider_1_1").show().revolution(
								{
									sliderType							:	"standard",
									sliderLayout						:	"fullwidth",
									dottedOverlay						:	"none",
									delay:6000,
									navigation: 
									{
										keyboardNavigation				:	"off",
										keyboard_direction				:	"horizontal",
										mouseScrollNavigation			:	"off",
										onHoverStop						:	"off",
										touch							:
										{
											touchenabled				:	"on",
											swipe_threshold				:	75,
											swipe_min_touches			:	1,
											swipe_direction				:	"horizontal",
											drag_block_vertical			:	false
										},
										arrows: 
										{
											style						:	"hesperiden",
											enable						:	false,
											hide_onmobile				:	false,
											hide_onleave				:	false,
											tmp							:	'',
											left						:
											{
												h_align					:	"left",
												v_align					:	"center",
												h_offset				:	20,
												v_offset				:	0
											},
											right: 
											{
												h_align					:	"right",
												v_align					:	"center",
												h_offset				:	20,
												v_offset				:	0
											}
										},
										bullets:
										{
											enable						:	true,
											hide_onmobile				:	false,
											style						:	"hesperiden",
											hide_onleave				:	false,
											direction					:	"horizontal",
											h_align						:	"center",
											v_align						:	"bottom",
											h_offset					:	0,
											v_offset					:	30,
											space						:	10,
											tmp							:	''
										}
									},
									visibilityLevels					:	[1240,1024,778,480],
									gridwidth							:	1250,
									gridheight							:	600,
									lazyType							:	"none",
									shadow								:	0,
									spinner								:	"spinner2",
									stopLoop							:	"off",
									stopAfterLoops						:	-1,
									stopAtSlide							:	-1,
									shuffle								:	"off",
									autoHeight							:	"off",
									disableProgressBar					:	"on",
									hideThumbsOnMobile					:	"off",
									hideSliderAtLimit					:	0,
									hideCaptionAtLimit					:	0,
									hideAllCaptionAtLilmit				:	0,
									debugMode							:	false,
									fallbacks							: 
									{
										simplifyAll						:	"off",
										nextSlideOnWindowFocus			:	"off",
										disableFocusListener			:	false
									}
								});
							}
						});

					</script>

				</div>						
			
			</div>

    <!-- Section -->
    <div class="template-content-section template-padding-reset template-background-image">

        <!-- Main -->
        <div class="template-main">



            <!-- Call to action -->

            <div data-id="section-2">
                
            </div>
        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <br><br><h1 class="editablesection" id="mce_0">Benvinguts a Monalco</h1>
                <h6 id="mce_89" class="">Més de 80 anys aprenent junts</h6>
                <div></div>
            </div>	

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-4 template-component-feature-position-top template-component-feature-size-large" style="display: block;">
                <ul class="template-layout-25x25x25x25 template-clear-fix">
                    <li class="template-layout-column-left" style="visibility: visible;">
                        <div class="template-icon-feature template-icon-feature-name-teddy-alt template-icon-feature-size-large icono1"></div>
                        <h5 id="mce_90" class=""><a href="<?= base_url('llar-d-infants.html') ?>">Llar d'infants</a></h5>
                        <p id="mce_91" class="">Volem que els infants s'emocionin aprenent dins un ambient acollidor i molt proper. I que poc a poc assoleixin hàbits, responsabilitats i autonomia.</p>
                    </li>
                    <li class="template-layout-column-center-left" style="visibility: visible;">
                        <div class="template-icon-feature template-icon-feature-name-blocks-alt template-icon-feature-size-large icono2"></div>
                        <h5 id="mce_92" class=""><a href="<?= base_url('infantil.html') ?>">Infantil</a></h5>
                        <p id="mce_93" class="">A Infantil treballem per posar unes bones bases i valors que possibilitin a l'infant desenvolupar totes les seves capacitats.</p>		
                    </li>	
                    <li class="template-layout-column-center-right" style="visibility: visible;">
                        <div class="template-icon-feature template-icon-feature-name-globe-alt template-icon-feature-size-large icono3"></div>
                        <h5 id="mce_94" class=""><a href="<?= base_url('primaria.html') ?>">Primària</a></h5>
                        <p id="mce_95" class="">A Primària eduquem alumnat per ser, per conèixer, per fer i per conviure. Aprendre a aprendre és la base d'aquest cicle.</p>			
                    </li>	
                    <li class="template-layout-column-right" style="visibility: visible;">
                        <div class="template-icon-feature template-icon-feature-name-keyboard-alt template-icon-feature-size-large icono4"></div>
                        <h5 id="mce_96" class=""><a href="<?= base_url('secundaria.html') ?>">Secundària</a></h5>
                        <p id="mce_97" class="">A Secundària volem formar persones competents i compromeses en el seu dia a dia.</p>			
                    </li>	
                </ul>
            </div>							

        </div>

    </div>

    <!-- Section -->
    

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2 id="mce_168" class="">Projectes destacats de l'escola</h2>
                <h6 id="mce_169" class="">Acompanyem l'alumnat de manera personalitzada. Obrim els joves a la societat amb una formació humana i amb maduresa intel·lectual</h6>
                <div></div>
            </div>

            <!-- Class -->
            <div class="template-component-class template-clear-fix proyectos" >

                <!-- Layout 50x50 -->
                <ul class="template-layout-50x50">
                	[foreach:proyectos]
	                    <!-- Left column -->
	                    <li class="template-layout-column-[posicion] [class] projectelefthome">
	                        <!-- Layout flex 50x50 -->
	                        <div class="backgroundPeque visible-xs" [foto]></div>
	                        <div class="template-layout-flex-50x50">
	                            <!-- Left column -->	                            
	                            <div class="cuadroNegroMain">
	                                <!-- Header -->
	                                <h5 id="mce_172" class="">[proyectos_nombre]</h5>
	                                <!-- Subheader -->
	                                <span id="mce_173" class="">[descripcion_corta]</span>
	                                <!-- Layout flex 50x50 -->
	                                <div class="template-layout-flex-50x50">
	                                    <div>
	                                        <span id="mce_174" class="" name="mce_174" type="hidden"><span id="mce_175" class="" type="hidden"> <span id="mce_177" class="" type="hidden"> </span></span></span>
	                                	</div>
	                                </div>
	                            </div>
	                            <!-- Right column -->
	                            <div class="template-component-class-background-1" [foto]>
	                                <a href="[link]" class="template-component-button template-component-button-style-1" id="mce_178">llegir més<i></i></a>
	                            </div>
	                        </div>
	                    </li>
                    [/foreach]
                </ul>
            </div>
            <div class="template-align-center">

                <!-- Button -->
                <!-- <a href="#" class="template-component-button template-component-button-style-1" id="vermasproyectos">Més projectes<i></i></a> -->
                <a href="#" class="template-component-button template-component-button-style-1" id="vermenosproyectos" style="display: none">Tancar<i></i></a>

            </div>

        </div>

    </div>



<!-- Section -->
	<div class="template-background-image template-background-image-1">

		<!-- Main -->
		<div class="template-main">
					
			<div class="template-section-white">
				<div class="template-component-counter-box template">
					<ul class="template-layout-33x33x33 template-clear-fix">
						<li class="template-layout-column-left">
							<span class="template-component-counter-box-counter mce-edit-focus" id="mce_24"><span class="template-component-counter-box-counter-value" id="mce_25">672</span></span>
							<h5 id="mce_26" class="">Alumnes</h5>
							<p id="mce_27" class="">Acompanyem els nostres alumnes en tota la seva vida acadèmica</p>
							<span class="template-component-counter-box-timeline template-state-hidden" id="mce_28"><span id="mce_29"></span></span>
						</li>
						<li class="template-layout-column-center">
							<span class="template-component-counter-box-counter" id="mce_30"><span class="template-component-counter-box-counter-value" id="mce_31">53</span></span>
							<h5 id="mce_32" class="">Professors</h5>
							<p id="mce_33" class="">Comptem amb un equip professional de primer nivell</p>
							<span class="template-component-counter-box-timeline template-state-hidden" id="mce_34"><span id="mce_35"></span></span>
						</li>
						<li class="template-layout-column-right">
							<span class="template-component-counter-box-counter" id="mce_36"><span class="template-component-counter-box-counter-value" id="mce_37">470</span></span>
							<h5 id="mce_38" class="">Famílies</h5>
							<p id="mce_39" class="">Mantenim un contacte proper amb les famílies</p>
							<span class="template-component-counter-box-timeline template-state-hidden" id="mce_40"><span id="mce_41"></span></span>
						</li>
						
					</ul>
					<div class="template-pagination template-pagination-style-1"></div>
				</div>
			</div>				
		</div>

	</div>

    <!-- Section -->
    

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left" style="visibility: visible;">

                    <h4 id="mce_203" class="">Com treballem</h4>

                    <!-- 
<p id="mce_204" class="mce-content-body" style="position: relative;" spellcheck="false" >Praesent arcu gravida vehicula est node maecenas loareet morbi a dosis luctus. Urna eget lacinia eleifend praesent luctus a arcu quis facilisis venenatis aenean interdum.</p><input name="mce_204" type="hidden">
 -->

                    <!-- List -->
                    <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                        <ul>
                            <li>Amb itineraris interdisciplinaris i en equips cooperatius per potenciar l'aprenentatge significatiu basat en els interessos dels alumnes</li>
                            <li>Fomentant l'experimentació</li>
                            <li>Potenciant el Pla Lector de Centre</li>
                            <li>Coneixent i utilitzant les TAC i les noves tecnologies</li>
                            <li>Utilitzant pissarres digitals, Ipads, Chromebooks i llibres</li>
                            <li>Desdoblant en anglès, matemàtiques, català, castellà, ciències i tecnologia</li>
                            <li>Amb mestres de reforç a totes les aules per una millor atenció als nostres alumnes</li>
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right" style="visibility: visible;">

                    <!-- Feature -->
                    <div class="template-component-list template-component-list-style-1 template-margin-top-3"><br>
                        <ul>
                            <li>Fomentant l'educació en valors, l'autoestima, l'esforç i el respecte</li>
                            <li>Fent un acompanyament individualitzat</li>
                            <li>Desenvolupant les competències bàsiques</li>
                            <li>Fomentant l'educació emocional de l'alumne</li>
                            <li>Ajudant els nostres alumnes en la seva orientació acadèmica</li>
                            <li>Grups reduïts en laboratori i tallers de tecnologia</li>
                            <li>Oferta de francès i alemany com a 2a llengua estrangera</li>
                            <li>Oferint un ampli ventall de tallers (optatives) que ajuden al desenvolupament del currículum</li>
                        </ul>
                    </div>
                        
                    </div>									

                </div>

            </div>

        </div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div><div style="text-align:center;"></div>

    </div>

    <div class="template-content-section template-padding-reset" style="position: relative;">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <br><br><h2 id="mce_216" class="">Últimes fotos</h2>
                <h6 id="mce_217" class="">Troba aquí un recull d'imatges de les activitats que fem</h6>
            </div>

        </div>

        <!-- Gallery -->
        <div class="template-component-gallery">

            <!-- Layout 25x25x25x25 -->
            <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix">

				<?php 
					$x = 0;
					foreach($this->db->get_where('galeria_home')->result() as $g):
					switch($x){
						case '0':$pos = 'left'; break;
						case '1':$pos = 'center-left'; break;
						case '2':$pos = 'center-right'; break;
						case '3':$pos = 'right'; break;
					}
					$x = $x<3?$x+1:0;
				?>
                <!-- Left column -->
                <li class="template-layout-column-<?= $pos ?>" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="<?= base_url('img/galeria/'.$g->foto) ?>" data-fancybox-group="gallery-2" id="mce_218" class=""><img src="<?= base_url('img/galeria/'.$g->foto) ?>" alt=""><span id="mce_223" class=""><span id="mce_224"><span id="mce_225"></span></span></span></a>
                    </div>
                </li>
        		<?php endforeach ?>

                

            </ul>

        </div>
    </div>

    <div class="template-content-section template-padding-reset" style="position: relative;">

        <!-- Main -->
        <div class="template-main">
            <!-- Call to action -->
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!-- Content -->
                <div class="template-component-call-to-action-content">

                    <!-- Left column -->
                    <div class="template-component-call-to-action-content-left">

                        <!-- Header -->
                        <h3 id="mce_253" class=""><b>Consulta la</b> nostra galeria de fotos!</h3>

                    </div>

                    <!-- Right column -->
                    <div class="template-component-call-to-action-content-right">

                        <!-- Button -->
                        <a href="<?= base_url('galeria') ?>" class="template-component-button template-component-button-style-1" id="mce_254">Veure Galeria<i></i></a>

                    </div>

                </div>

            </div>

    </div>

    <!-- Section -->




    </div>

    <div class="template-content-section template-content-section-video" style="position: relative;">

        <!-- Box video -->
        <div class="template-component-box-video">

            <!-- Video -->
            <iframe style="width:100%;" src="https://www.youtube.com/embed/5wNlTQnv9A0?rel=0&amp;controls=0&amp;showinfo=0" allow="autoplay; encrypted-media" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>

            <!-- Overlay -->
            <div></div>

        </div>


        <!-- Main -->
        <div class="template-main">

            <!-- White section -->
            <div class="template-section-white template-align-center" style="padding-top:100px;">

                <!-- Header -->
                <h5 class="template-text-uppercase" id="mce_258"></h5>

                <!-- Divider -->
                <!--<div class="template-component-divider template-component-divider-style-3 template-margin-top-reset .template-margin-bottom-3"></div>-->

                <!-- Button -->
                <div class="template-fancybox">
                	<a href="https://www.youtube.com/embed/5wNlTQnv9A0?rel=0&amp;controls=0&amp;showinfo=0" data-fancybox-type="iframe" data-fancybox-group="gallery-1" class="template-component-button template-component-button-style-3" id="mce_259">Mira aquest video<i></i></a>
            	</div>
<br><br>
                <a href="[base_url]videos" id="mce_260" class="">Veure tots els vídeos</a>


            </div>

        </div>

    </div>



    <!-- Section -->
    <div class="template-content-section template-padding-top-reset" style="position: relative;">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <br><br><h2 id="mce_264" class="">Notícies</h2>
                <h6 id="mce_265" class="">
                	<ul id="portfolio-filter" class="filter" data-related-grid="portfolio-grid">
		                <li class="active">
		                    <a href="#" data-filter="*" id="allSocial">
		                        <i class="fa fa-globe fa-2x"></i> <br/>
		                        Totes
		                    </a>
		                </li>
		                <li>
		                    <a data-filter=".instagram" href="#" title="Feed Instagram">
		                        <i class="fa fa-2x fa-instagram"></i><br>
		                        Instagram
		                    </a>
		                </li>
		                <li>
		                    <a data-filter=".twitter" href="#" title="Feed Twitter">
		                        <i class="fa fa-2x fa-twitter"></i><br>
		                        Twitter
		                    </a>
		                </li>
		                <li>
		                    <a data-filter=".facebook" href="#" title="Feed Facebook">
		                        <i class="fa fa-2x fa-facebook-square"></i><br/>
		                        Facebook
		                    </a>
		                </li>
		            </ul>
                </h6>
                <div></div>
            </div>

            <!-- Recent posts -->
            <div class="template-component-recent-post template-component-recent-post-style-1">

                <div id="portfolio-grid" class="social-feed-container isotope-grid gallery-container style-column-4 isotope-spaced clearfix">
	                
	                
	            </div>

            </div>

        </div>

    </div>
<?php if(!empty($this->db->get('ajustes')->row()->popup)): ?>
    <script>
        window.onload = function(){
            jQuery.get('paginas/frontend/popup',function(data){
                    jQuery.fancybox(
                    data,
                    {
                        'autoDimensions'    : false,
                        'width'                 : 600,
                        'closeClick'  : false,
                        'height'                : 'auto',
                        'transitionIn'      : 'none',
                        'transitionOut'     : 'none'
                    }
                );
            });
        };
    </script>
<?php endif ?>