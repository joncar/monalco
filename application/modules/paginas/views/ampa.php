<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Organització</h1>
                <h6>Entenem una mica l'escola</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 ">

        <!-- Main -->
        <div class="template-main">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/ampa.png') ?>" style="width:120px"></center>
                <h1>AMPA</h1>                
                <div></div>
            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column --> 
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="editablesection template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader mce-content-body" id="mce_79"><a href="[base_url]img/_sample/1050x770/ampa.jpg" data-fancybox-group="gallery-1" id="mce_80"> <img src="[base_url]img/_sample/690x506/ampa.jpg" alt=""> <span id="mce_81"><span id="mce_82"><span id="mce_83"></span></span></span> </a><div><h6 id="mce_84">Activitat AMPA</h6> <span id="mce_85">Curs 2017-2018</span></div><p id="mce_86"><b>Activitat AMPA</b></p></div>
                            </li>
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset editablesection mce-content-body mce-edit-focus" id="mce_87"><div class="template-align-center"><h3 id="mce_88">Benvinguts a <strong>AMPA Monalco</strong></h3><div class="template-component-divider template-component-divider-style-2"><br></div><div class="template-component-italic template-margin-top-3">L'AMPA Col·legi Monalco és un grup de mares i pares que treballa i col·labora conjuntament amb l'escola per a la millor educació dels nostres fills.</div><p class="template-margin-top-3" id="mce_89">L'AMPA està organitzada per una junta directiva i diferents departaments que organitzen activitats com el cros, el 3x3 de bàsquet, els tres tombs, espectacle de màgia en anglès,...</p><!--  <a href="#" <!~~ class="template-component-button template-component-button-style-1 template-margin-top-3" id="mce_90">Conèix més de l'AMPA ~~>  --><i></i></a></div></div>

            </div>

        </div>

    </div>
    

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="editablesection template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium mce-content-body" id="mce_94"><ul class="template-layout-33x33x33 template-clear-fix"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-gallery-alt"><br></div><h5 id="mce_95">Dóna suport</h5><p id="mce_96">Dona suport a l'escola en tot allò que contribueixi a la millora de la qualitat de l'ensenyament.</p></li><li class="template-layout-column-center"><div class="template-icon-feature template-icon-feature-name-flag-alt"><br></div><h5 id="mce_97">Participa</h5><p id="mce_98">Participa conjuntament amb l'equip directiu i mestres ajudant i col·laborant amb activitats que es duen a terme al llarg del curs.</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-fastfood-alt"><br></div><h5 id="mce_99">Administra</h5><p id="mce_100">Administra i gestiona els recursos que aporten els associats.</p></li></ul></div>

        </div>

    </div>





    <!-- Section -->
    <div class="template-content-section template-content-layout template-content-layout-sidebar-right template-clear-fix template-background-color-2">
        <div class="template-main">
                <div class="template-component-header-subheader">
                    <h2>Organigrama</h2>
                <center></center>
                                
                <div></div>
            </div>

        <!-- Content -->
        <div class="template-content-layout">

            <div data-id="section-1">
                <!-- Accordion -->
                <div class="template-component-accordion">
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Presidència</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>President</th>
                                <td>Jordi Viles Abella</td>
                            </tr>
                            
                                                        
                        </table>                        
                    </div>

                     <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Tresoreria</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Tresorera</th>
                                <td>Anna Àvila Rodríguez</td>
                            </tr>
                            <tr>
                                <th>Tresorer</th>
                                <td>Manel Navarro Salgado</td>
                            </tr>
                        </table>                        
                    </div>

                     <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Secretaria</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Secretària</th>
                                <td>Ester Ramon Robles</td>
                            </tr>
                            <tr>
                                <th>Secretària</th>
                                <td>Sandra Cantero Cubiló</td>
                            </tr>
                            
                            
                        </table>                        
                    </div>
                    
                    <!-- Header 1 -->
                    <h6>
                        <a href="#"><h5>Vocals</h5></a>
                    </h6>
                    <!-- Content 1 -->
                    <div>
                        <table class="table">

                            <tr>
                                <th>Vocal</th>
                                <td>Toni Mensa Díaz</td>
                            </tr>
                            <tr>
                                <th>Vocal</th>
                                <td>Gemma Casals Claret</td>
                            </tr>
                            <tr>
                                <th>Vocal</th>
                                <td>Jordi Banqué Amat</td>
                            </tr>
                            <tr>
                                <th>Vocal</th>
                                <td>Silvia Rovira Carrera</td>
                            </tr>
                            <tr>
                                <th>Vocal</th>
                                <td>Imma Soteras Bartrolí</td>
                            </tr>
                            <tr>
                                <th>Vocal</th>
                                <td>Meritxell Aguilera Córdoba</td>
                            </tr>
                            
                            
                        </table>                        
                    </div>
                    
                   
                </div>
            </div>
         </div>
     </div>
 </div>





<div class="template-content-section template-padding-bottom-5">
        <div class="template-main">
            <!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <h2>Notícies de l'AMPA</h2>
            <h6>T'informem de les activitats que realitza l'AMPA</h6>
            <div></div>
        </div>
    <!-- Section -->
            <!-- Recen post -->
            <div class="template-component-recent-post template-component-recent-post-style-1">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    [foreach:blog]
                    <li class="template-layout-column-[posicion]" style="visibility: visible;">
                        <div class="template-component-recent-post-date">[fecha]</div>
                        <div class="template-component-image">
                            <a href="[link]" id="mce_266" class="">
                                <img src="[foto]" alt=""> 
                                <span id="mce_267" class=""><span id="mce_268">
                                        <span id="mce_269">
                                            
                                        </span>
                                    </span>
                                </span>
                            </a>                            
                        </div>
                        <h5 id="mce_270" class="">
                            <a href="[link]" id="mce_271" data-mce-href="#" class="">
                                [titulo]
                            </a>
                        </h5>
                        <p id="mce_272" class="">
                            [descripcion]
                        </p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author">
                                <a href="[link]" id="mce_273" class="">[user]</a>
                            </li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="[link]" id="mce_274" class="">Events</a>,
                                <a href="[link]" id="mce_275" class="">Fun</a>
                            </li>
                        </ul>
                    </li>
                    [/foreach]
                 </ul>
             </div>  

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-reset template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Call to action -->
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!-- Content -->
                <div class="template-component-call-to-action-content">

                    <!-- Left column -->
                    <div class="template-component-call-to-action-content-left">

                        <!-- Header -->
                        <h3 id="mce_228" class="mce-content-body">Vols contactar amb l'AMPA?</h3>

                    </div>

                    <!-- Right column -->
                    <div class="template-component-call-to-action-content-right">

                        <!-- Button -->
                        <!--<a href="<?= base_url('contacte.html') ?>" class="template-component-button template-component-button-style-1 mce-content-body" id="mce_229">Contactar aquí<i></i></a>-->
                            <a href="mailto:ampa@monalco.cat" class="template-component-button template-component-button-style-1 mce-content-body" id="mce_229">Contactar aquí<i></i></a>

                    </div>									

                </div>

            </div>				

        </div>

    </div><div style="text-align:center;"></div><div style="text-align:center;"></div>


</div>
        