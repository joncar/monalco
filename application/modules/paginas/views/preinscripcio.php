
            
            
            
            
            
            
            
            <!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_13" class="">Preinscripció</h1>
                <h6 id="mce_14" class="">2020-2021</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/pec.png') ?>" style="width:120px"></center>
                <h2></h2>
                <div></div>
            <p class="template-margin-top-3" id="mce_17">Aquí podeu consultar la informació actualitzada pel Departament d’Educació a data 7 de maig de 2020.</p></div>
        <!-- Header and subheader -->                
                

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-left">

                        <h3 id="mce_15" class="">Dates Preinscripció <strong>Curs 2020-2021 </strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            Llar d’Infants:
                        </div>

                        <p class="template-margin-top-3" id="mce_16">Matrícula oberta durant tot el curs. Podeu posar-vos en contacte amb l’escola en qualsevol moment per formalitzar la matrícula.</p>
<div class="template-component-italic template-margin-top-3">
                            Educació infantil de segon cicle (P3, P4 i P5), Educació Primària i Educació Secundària Obligatòria:
                        </div>

                        <p class="template-margin-top-3" id="mce_16">Del 13 al 22 de maig de 2020.</p>

                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix" id="mce_17"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/pec1c.jpg" data-fancybox-group="gallery-1" id="mce_18"> <img src="[base_url]img/_sample/690x506/pec1c.jpg" alt=""> <span id="mce_19" class=""><span id="mce_20" class=""><span id="mce_21" class=""><br></span></span></span> </a><div><h6 id="mce_22">Molt més que un projecte educatiu</h6><span id="mce_23">Un projecte educatiu pluridimensional</span></div><p id="mce_24"><b>Play Time In Kindergarten</b> Tenderhearts Class</p></div></li></ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <!-- Gallery -->
                    <!-- 
<div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix" id="mce_28"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/pec2b.jpg" data-fancybox-group="gallery-1" id="mce_29" class=""><img src="[base_url]img/_sample/690x506/pec2.jpg" alt=""> <span id="mce_30" class=""><span id="mce_31"><span id="mce_32"></span></span></span></a><div><h6 id="mce_33" class="">L'alumne cerca, raona, investiga....</h6><span id="mce_34" class="">Escola Vivencial</span></div><p id="mce_35" class=""><b>L'alumne cerca, raona, investiga....</b>Escola Vivencial</p></div></li></ul>
                    </div>
 -->

                </div>

                <!-- Right column -->
                <div class="template-layout-column-center template-margin-bottom-reset">

                    <div class="template-align-left">

                        <h3 id="mce_36" class="">Procés de <strong>preinscripció</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        

                        <p class="template-margin-top-3" id="mce_37">El tràmit es podrà fer tot online, a través del portal <a href="http://queestudiar.gencat.cat/ca/preinscripcio" target="_blank">preinscripcio.gencat.cat</a>. S’elimina la necessitat de presentar la sol·licitud i la documentació en paper al centre, i queda substituïda per utilitzar el formulari telemàtic i enviar la documentació necessària -escanejada o fotografiada- mitjançant un correu a la bústia electrònica oficial del centre demanat en primera opció.

La documentació s’haurà d’enviar a: <a href="mailto:info@monalco.cat">info@monalco.cat</a><br>

La documentació escanejada o fotografiada que cal adjuntar és la següent:<br><br>

    1. Resguard de la sol·licitud de preinscripció obtinguda a través del portal.<br>
    2. Fotocòpia del DNI (pare i/o mare i alumne/a).<br>
    3. Fotocòpia de la targeta sanitària.<br>
    4. Fotocòpia del llibre de família.<br>
    5. Fotocòpia del llibre de vacunes.<br>
    6. Padró (en cas de que el domicili del DNI no sigui d’Igualada).<br>
    7. Certificat d’empresa si pare/mare treballa a Igualada (només en cas que el domicili estigui fora d’Igualada).<br>
    8. Fotocòpia del carnet de família nombrosa o monoparental.<br>
    9. Documentació que acrediti ajuda de renda garantida.<br>
    10. Certificat de discapacitat igual o superior al 33% de l’alumne o d’un familiar de primer grau.<br><br>

Aquelles famílies que no disposin dels mitjans per dur a terme el procediment telemàtic, i amb l’objectiu de garantir l’equitat, es preveu que disposin d’uns dies per poder lliurar les seves sol·licituds de forma presencial. Així, la preinscripció presencial estarà disponible a partir del 19 de maig i fins el 22 de maig inclòs. Per a fer-ho, caldrà demanar cita prèvia al centre que les famílies hagin escollit en primera opció trucant per telèfon o  a través d’Internet.</p>

                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-left">

                        <h3 id="mce_41" class="">Dates per realitzar <strong>la matrícula</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            Llar d’Infants:
                        </div>

                        <p class="template-margin-top-3" id="mce_42">Matrícula oberta durant tot el curs. Podeu posar-vos en contacte amb l’escola en qualsevol moment per formalitzar la matrícula.</p>
<div class="template-component-italic template-margin-top-3">
                            Educació infantil de segon cicle (P3, P4 i P5), Educació Primària i Educació Secundària Obligatòria:
                        </div>

                        <p class="template-margin-top-3" id="mce_42">Del 13 al 17 de juliol de 2020.</p>

                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix" id="mce_43"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/pec3b.jpg" data-fancybox-group="gallery-1" id="mce_44" class=""><img src="[base_url]img/_sample/690x506/pec3.jpg" alt=""> <span id="mce_45" class=""><span id="mce_46" class=""><span id="mce_47" class=""><br></span></span></span></a><div><h6 id="mce_48" class="">Escola activa, familiar, innovadora i acollidora</h6><span id="mce_49" class="">Participació dels pares amb les activitats</span></div><p id="mce_50" class=""><b>Escola activa, familiar, innovadora i acollidora</b> Participació dels pares amb les activitats</p></div></li></ul>
                    </div>

                </div>

            </div>

        </div>

    </div>
<div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
               <br>
                 <br><br>
                 <br>
                 <br><br>
                 <br>
                 <br><br>
                 <br>
                 <br>
                 <br>
            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

				<!-- Content -->
				<div class="template-main">

					<!-- Header -->
					<h3 id="mce_41" class="template-align-center">Informació <strong>Cita prèvia</strong></h3><div class="template-component-divider template-component-divider-style-2"></div>
					
					<!-- Section -->
					<div data-id="section-1">
<!-- Dropcap -->
<div class="template-component-italic template-margin-top-3 template-margin-bottom-3">

Per demanar informació sobre la preinscripció o sol·licitar cita prèvia es poden utilitzar els canals habituals de l’escola:


				</div>

			</div>
					
					</div>
					

					<!-- Preformatted text -->
					<div class="template-component-preformatted-text" data-id-rel="section-1">
						
						<pre></pre>
						
					</div>
					

					<!-- Header -->
					

					<!-- Section -->
					

					
						
					</div>							

				</div>
<div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">


                        
            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">


                        
                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        

                        

                        
                        <!-- 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 -->

                    </div>

                </div>

                <!-- Right column -->
                <!-- Content -->
<div class="template-content">

    <!-- Main -->
    <div class="template-content-section template-main">
        

        <!-- Feature -->
        <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">

            <!-- Layout 33x33x33 -->
            <ul class="template-layout-33x33x33 template-clear-fix">

                <!-- Left column -->
                <li class="template-layout-column-left">
                    <div class="template-icon-feature template-icon-feature-name-envelope-alt"></div>
                    <h5>Adreça</h5>
                    <p>
                        Carrer Capellades, 2<br/>
                        08700 Igualada<br/>
                        Centre Concertat GC
                    </p>
                </li>

                <!-- Center column -->
                <li class="template-layout-column-center">
                    <div class="template-icon-feature template-icon-feature-name-mobile-alt"></div>
                    <h5>Telèfon i email</h5>
                    <p>
                        Tel: 93 803 15 77<br/>
                        Mòbil: 608 188 004<br/>
                        <a href="mailto:office@fable.com">info@monalco.cat</a>
                    </p>
                </li>

                <!-- Right column -->
                <li class="template-layout-column-right template-margin-bottom-5">
                    <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                    <h5>Horaris Secretaria</h5>
                    <p>
                        Dilluns a divendres<br/>
                        8h – 19h<br/>
                        Cap de setmana tancat
                    </p>
                </li>

            </ul>

        </div>
        <!-- Contact form -->
        <div class="template-component-contact-form" id="contactenosform">

            <form action="<?= base_url('paginas/frontend/contacto') ?>" method="post">
                
                <!-- Layout 50x50 -->
                <div class="template-layout-50x50 template-clear-fix">

                    <!-- Left column -->
                    <div class="template-layout-column-left">

                        <!-- Name -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-name">Nom *</label>
                            <input type="text" name="nombre" id="contact-form-name" />
                        </div>

                        <!-- E-mail -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-email"> E-mail *</label>
                            <input type="text" name="email" id="contact-form-email" />
                        </div>

                        <!-- Subject -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-subject">Curs</label>
                            <input type="text" name="tema" id="contact-form-subject" />
                        </div>

                    </div>

                    <!-- Right column -->
                    <div class="template-layout-column-right">
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-message">Missatge *</label>
                            <textarea rows="1" cols="1" name="message" id="contact-form-message"></textarea>
                        </div>
                    </div>

                </div>
                <div style="margin:10px">
                    <?php 
                        echo @$_SESSION['msj'];
                        unset($_SESSION['msj']);
                    ?>
                </div>
                <div class="template-form-line template-align-center">
                    <div style="display:inline-block; width:auto;">
                        <div class="g-recaptcha" data-sitekey="6LcjXm0UAAAAAMO1NqE3fW0ITNfL1l0xxSz6uXeN"></div>
                    </div>
                </div>

                <div class="template-form-line template-state-block" style="text-align:center; margin:20px;">
                    <input name="politicas" id="contact-form-subject" placeholder="Curs i informació sol·licitud matrícula" type="checkbox"> He llegit i accepto la <a href="<?= base_url('aviso-legal.html') ?>" target="_new">política de privacitat*</a>
                </div>

                <div class="template-form-line template-form-line-submit template-align-center">

                    <div class="template-state-block">

                        <!-- Submit button -->
                        <input class="template-component-button template-component-button-style-1" type="submit" value="enviar missatge" name="contact-form-submit" id="contact-form-submit"/>

                    </div>

                </div>

            </form>

        </div>


            </div>


            </div>

        </div>

    </div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div>

				<!-- Sidebar -->
				<div class="template-content-layout-column-right">

					

				</div>

			</div><div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
                <ul class="template-layout-25x25x25x25 template-clear-fix" id="mce_54"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-teddy-alt"><br></div><h5 id="mce_55" class="">Potenciar</h5><p id="mce_56" class="">Totes les persones tenim talents que cal descobrir i potenciar</p></li><li class="template-layout-column-center-left"><div class="template-icon-feature template-icon-feature-name-heart-alt"><br></div><h5 id="mce_57" class="">Ajudar</h5><p id="mce_58">Totes les persones necessitem ajuda davant les dificultats</p></li><li class="template-layout-column-center-right"><div class="template-icon-feature template-icon-feature-name-graph-alt"><br></div><h5 id="mce_59">Orientar</h5><p id="mce_60" class="">Totes les persones, amb una bona orientació, podem assolir els nostres propòsits</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-globe-alt"><br></div><h5 id="mce_61" class="">Acompanyar</h5><p id="mce_62">L'objectiu de la nostra escola és una formació integral de l'alumne i posem eines per aconseguir-ho</p></li></ul>
            </div>

        </div>

    </div>

    <!-- Section -->

                        