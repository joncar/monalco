<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Organització</h1>
                <h6>Entenem una mica l'escola</h6>
            </div>
        </div>

    </div>
</div>
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-main">
        <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/horari.png') ?>" style="width:120px"></center>
                <h2>Horaris</h2>                
                <div></div>
            </div>

        <!-- Team -->
        <div class="template-component-team template-component-team-style-1">
            <ul class="template-layout-100 template-clear-fix">
                <li class="template-layout-column-left">
                    <ul class="template-layout-33x66 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/horar1.jpg" data-fancybox-group="team-1">
                                    <img src="<?= base_url() ?>img/_sample/525x560/horar1.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Escola</h6>
                                </div>
                                <p><b>Entrada C/Capellades</b></p>
                            </div>	
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!-- 
<li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
 -->
                                </ul>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <h3 class="template-component-team-name">Escola</h3>
                            <span class="template-component-team-position"></span>
                            <p class="template-component-team-description">L'escola és oberta de 7 del matí a 19h.</p>
                            <h6 class="template-margin-top-3"></h6>
                            <div class="template-component-vertical-grid">
                                <ul>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Accés al recinte a les 7h:</div>
                                        <div>Per la porta de l'Avinguda Barcelona</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>Accés al recinte a les 8h:</div>
                                        <div>Per la porta del Carrer Capellades</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Accés en horari escolar:</div>
                                        <div>Per la porta del Carrer Capellades</div>
                                    </li>
                                    <!-- 
<li class="template-component-vertical-grid-line-1n">
                                        <div>My Languages:</div>
                                        <div>Italian, Dutch</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>My Hobby:</div>
                                        <div>Family and Friends, Skiing</div>
                                    </li>
 -->
                                </ul>
                            </div>											
                        </li>
                    </ul>
                </li>
                <li class="template-layout-column-left">
                    <ul class="template-layout-33x66 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/hr3.jpg" data-fancybox-group="team-1">
                                    <img src="<?= base_url() ?>img/_sample/525x560/hr3.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Acollida</h6>
                                    <span></span>
                                </div>
                                <p><b>Acollida</b></p>
                            </div>	
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                   <!-- 
 <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
 -->
                                </ul>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <h3 class="template-component-team-name">Acollida</h3>
                            <span class="template-component-team-position"></span>
                            <p class="template-component-team-description">Al matí, quan els infants arriben, tenen l’opció d'esmorzar si a casa encara no ho han fet. També tenen diverses propostes: material de dibuix i manualitats, jocs de taula, jocs de construcció, lectura, etc. </p>
                            <h6 class="template-margin-top-3"></h6>
                            <div class="template-component-vertical-grid">
                                <ul>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Horari d'acolllida matí:</div>
                                        <div>de 7 a 9 del matí</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>Horari d'acolllida migdia:</div>
                                        <div>de 13 a 13:30h</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Horari d'acolllida tarda:</div>
                                        <div>de 17 a 19h</div>
                                    </li>
                                    <!-- 
<li class="template-component-vertical-grid-line-2n">
                                        <div>Class Educator:</div>
                                        <div>Bouncy Bears</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>My Languages:</div>
                                        <div>Italian, English</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>My Hobby:</div>
                                        <div>Music, Dancing, Skiing</div>
                                    </li>
 -->
                                </ul>
                            </div>											
                        </li>
                    </ul>
                </li>
                <li class="template-layout-column-left">
                    <ul class="template-layout-33x66 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/hrllar.jpg" data-fancybox-group="team-1">
                                    <img src="<?= base_url() ?>img/_sample/525x560/hrllar.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Llar d'Infants</h6>
                                    <span></span>
                                </div>
                                <p><b>Llar d'Infants</b></p>
                            </div>	
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                   <!-- 
 <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
 -->
                                </ul>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <h3 class="template-component-team-name">Llar d'Infants</h3>
                            <span class="template-component-team-position"></span>
                            <p class="template-component-team-description">L'horari de la Llar d'Infants és totalment flexible. </p>
                            <h6 class="template-margin-top-3"></h6>
                            <div class="template-component-vertical-grid">
                                <ul>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Horari:</div>
                                        <div>de 7h a 19h</div>
                                    </li>
                                    
                                    <!-- 
<li class="template-component-vertical-grid-line-2n">
                                        <div>Class Educator:</div>
                                        <div>Bouncy Bears</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>My Languages:</div>
                                        <div>Italian, English</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>My Hobby:</div>
                                        <div>Music, Dancing, Skiing</div>
                                    </li>
 -->
                                </ul>
                            </div>											
                        </li>
                    </ul>
                </li>
                <li class="template-layout-column-left">
                    <ul class="template-layout-33x66 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/hr2.jpg" data-fancybox-group="team-1">
                                    <img src="<?= base_url() ?>img/_sample/525x560/hr2.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Infantil i Primària</h6>
                                    <span></span>
                                </div>
                                <p><b>Infantil i Primària</b></p>
                            </div>	
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!-- 
<li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
 -->
                                </ul>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <h3 class="template-component-team-name">Infantil i Primària</h3>
                            <span class="template-component-team-position"></span>
                            <p class="template-component-team-description">L'horari escolar d'Infantil i Primària és sempre el mateix. En cas de variació excepcional, s'informarà als pares amb antel·lació.</p>
                            <h6 class="template-margin-top-3"></h6>
                            <div class="template-component-vertical-grid">
                                <ul>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Matí:</div>
                                        <div>de 9h a 13h</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>Tarda:</div>
                                        <div>de 15h a 17h</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Menjador/Espai Migdia:</div>
                                        <div>de 13h a 15h</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>Acollida:</div>
                                        <div>Matí de 7h a 9h / Tarda de 17h a 19h</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Extraescolars:</div>
                                        <div>de 17h a 19h</div>
                                    </li>
                                    <!-- 
<li class="template-component-vertical-grid-line-2n">
                                        <div>My Hobby:</div>
                                        <div>Collecting Stamps, Playing Basketball</div>
                                    </li>
 -->
                                </ul>
                            </div>											
                        </li>
                    </ul>
                </li>
<li class="template-layout-column-left">
                    <ul class="template-layout-33x66 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/hreso.jpg" data-fancybox-group="team-1">
                                    <img src="<?= base_url() ?>img/_sample/525x560/hreso.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Secundària</h6>
                                    <span></span>
                                </div>
                                <p><b>Secundària</b></p>
                            </div>	
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <!-- 
<li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
 -->
                                </ul>
                            </div>
                        </li>
                        <li class="template-layout-column-right">
                            <h3 class="template-component-team-name">Secundària</h3>
                            <span class="template-component-team-position"></span>
                            <p class="template-component-team-description">L'horari escolar de Secundària és sempre el mateix. En cas de variació excepcional, s'informarà als pares amb antel·lació.</p>
                            <h6 class="template-margin-top-3"></h6>
                            <div class="template-component-vertical-grid">
                                <ul>
                                    <li class="template-component-vertical-grid-line-1n">
                                        <div>Matí:</div>
                                        <div>de 8h a 13:30h</div>
                                    </li>
                                    <li class="template-component-vertical-grid-line-2n">
                                        <div>Tarda:</div>
                                        <div>Dilluns, dimarts i dijous de 15h a 17h</div>
                                    </li>
                                    
                                    <!-- 
<li class="template-component-vertical-grid-line-2n">
                                        <div>My Hobby:</div>
                                        <div>Collecting Stamps, Playing Basketball</div>
                                    </li>
 -->
                                </ul>
                            </div>											
                        </li>
                    </ul>
                </li>								
            </ul>
        </div>
    </div>

    <!-- Section -->
    <!-- 
<div class="template-content-section template-background-image template-background-image-2">

        <!~~ White section ~~>
        <div class="template-main template-section-white">

            <!~~ Twitter User Timeline ~~>
            <div class="template-component-twitter-user-timeline template-component-twitter-user-timeline-style-2">

                <ul class="template-layout-100">

                    <li class="template-layout-column-left">
                        <i></i>
                        <p>We've released new version (v10.8) of our GymBase #Gym #Fitness #WordPress Theme. Please check at #ThemeForest https://t.co/qsCxtz4sNw</p>
                        <div></div>
                        <span>QuanticaLabs</span>
                    </li>

                    <li class="template-layout-column-left">
                        <i></i>
                        <p>We've released new version (v2.6) of our Pressroom #News #Magazine #WordPress Theme. Please check at #ThemeForest https://t.co/Nfols0XXmY</p>
                        <div></div>
                        <span>QuanticaLabs</span>
                    </li>

                    <li class="template-layout-column-left">
                        <i></i>
                        <p>We've released new version (v8.1) of our MediCenter #Medic #Health #WordPress Theme. Please check at #ThemeForest https://t.co/J3cZMZzhrN</p>
                        <div></div>
                        <span>QuanticaLabs</span>
                    </li>

                    <li class="template-layout-column-left">
                        <i></i>
                        <p>We've released new version (v1.9) of our #Car Service #Mechanic #Auto #Shop #WordPress Theme. Check at #ThemeForest https://t.co/ZBAqruI2le</p>
                        <div></div>
                        <span>QuanticaLabs</span>
                    </li>

                    <li class="template-layout-column-left">
                        <i></i>
                        <p>We have released new version (v2.0) of our Renovate #Construction #Renovation #HTML #Template Check at #ThemeForest https://t.co/9xZmqggm3Y</p>
                        <div></div>
                        <span>QuanticaLabs</span>
                    </li>

                </ul>
                <div class="template-pagination template-pagination-style-1"></div>

            </div>

        </div>

    </div>

    <!~~ Section ~~>
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!~~ Main ~~>
        <div class="template-main">

            <!~~ Layout 50x50 ~~>
            <div class="template-layout-50x50 template-clear-fix">

                <!~~ Left column ~~>
                <div class="template-layout-column-left">

                    <h5>Questions and Answers</h5>

                    <!~~ Accordion ~~>
                    <div class="template-component-accordion">
                        <h6><a href="#">Educational Field Trips and Presentations</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Reporting on Individual Achievement</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>										
                        <h6><a href="#">Writing and Reading Classes</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Scientific Investigation Opportunities</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>
                    </div>

                </div>

                <!~~ Right column ~~>
                <div class="template-layout-column-right">

                    <h5>Class Hours Per Year</h5>

                    <!~~ Counters list ~~>
                    <div class="template-component-counter-list">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Art Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">165</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Writing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">125</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Drawing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">52</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left template-margin-bottom-reset">
                                <span class="template-component-counter-list-label">Yoga Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">75</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!~~ Section ~~>
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-main">

        <!~~ Header and subheader ~~>
        <div class="template-component-header-subheader">
            <h2>Our Core Skills</h2>
            <h6>Feel free to explore our core skills</h6>
            <div></div>
        </div>

        <!~~ Feature ~~>
        <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
            <ul class="template-layout-33x33x33 template-clear-fix">
                <li class="template-layout-column-left">
                    <div class="template-icon-feature template-icon-feature-name-app-alt"></div>
                    <h5>Magento Care</h5>
                    <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                </li>
                <li class="template-layout-column-center">
                    <div class="template-icon-feature template-icon-feature-name-pin-alt"></div>
                    <h5>Nunc Blandit</h5>
                    <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                </li>	
                <li class="template-layout-column-right">
                    <div class="template-icon-feature template-icon-feature-name-piano-alt"></div>
                    <h5>Mauris Solutide</h5>
                    <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                </li>
            </ul>
        </div>

        <!~~ Feature ~~>
        <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium">
            <ul class="template-layout-33x33x33 template-clear-fix">
                <li class="template-layout-column-left">
                    <div class="template-icon-feature template-icon-feature-name-people"></div>
                    <h5>Quisque Morbi</h5>
                    <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                </li>
                <li class="template-layout-column-center">
                    <div class="template-icon-feature template-icon-feature-name-pencil"></div>
                    <h5>Libro Retrum</h5>
                    <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                </li>	
                <li class="template-layout-column-right">
                    <div class="template-icon-feature template-icon-feature-name-paintbrush"></div>
                    <h5>Phasellus Novum</h5>
                    <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                </li>
            </ul>
        </div>						

    </div>

</div>
 -->