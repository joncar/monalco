<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Projectes</h1>
                <h6>Coneix els nostres projectes</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">
        <div class="template-component-header-subheader">
                <center>[icono]</center>
                <h2>[proyectos_nombre]</h2>
                <div></div>
        </div>

        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    [descripcion_corta] <br/>
                    [descripcion]
                </div>
                

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">                    
                    <div>
                        <img src="[portada]" data-thumb="[portada]" alt=""/>
                        [foreach:fotos]
                        <img src="[foto]" data-thumb="[foto]" alt=""/>
                        [/foreach]                        
                    </div>
                </div>

            </div>

        </div>

        [descripcion2]

    </div>

                

</div>