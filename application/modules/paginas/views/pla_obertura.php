
            <!-- Header -->
<div class="template-header">
    <!-- Top header -->
    [header]
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1 id="mce_13" class="">Pla d'obertura</h1>
                <h6 id="mce_14" class="">Juny 2020</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <div class="template-component-header-subheader">
                <center><img src="<?= base_url('img/icon/pec.png') ?>" style="width:120px"></center>
                <h2></h2>
                <div></div>
            <p class="template-margin-top-3" id="mce_17">Aquí podeu consultar la informació actualitzada sobre l'obertura de l'escola en Fase 2.</p></div>


                <!-- Right column -->
                <!-- 
<div class="template-layout-column-center template-margin-bottom-reset">

                    <div class="template-align-left">

                        <h3 id="mce_36" class=""> <strong></strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        

                        <p class="template-margin-top-3" id="mce_37">

                        <!~~ 
<a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Learn More <i></i></a>
 ~~>

                    </div>

                </div>

            </div>

        </div>

    </div>
 -->

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-left">

                        <h3 id="mce_41" class="">Documents <strong>per descarregar</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            <table class="table">
		                        <tbody>
		                        	<?php 
                                        $this->db->order_by('orden','ASC');
                                        foreach($this->db->get_where('documentos',['categorias_documentos_id'=>11])->result() as $d): 
                                    ?>
			                        	<tr>
			                            	<th style="vertical-align: middle"><?= $d->nombre ?></th>
			                            	<td style="text-align: center; width:14%">
			                            		<a href="<?= base_url() ?>files/documentos/<?= $d->fichero ?>" target="_new">
			                            		<img src="<?= base_url() ?>img/pdf.png" style="width:30px; display: block;margin: 0 auto;"><br>
			                            		<span style="font-size:12px; text-decoration: underline;display: block;">Descarregar</span></a></td>
			                        	</tr>
		                        	<?php endforeach ?>
		                    	</tbody>
		                	</table>
                        </div>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix" id="mce_43"><li class="template-layout-column-left"><div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader"><a href="[base_url]img/_sample/1050x770/pec3b.jpg" data-fancybox-group="gallery-1" id="mce_44" class=""><img src="[base_url]img/_sample/690x506/pec3.jpg" alt=""> <span id="mce_45" class=""><span id="mce_46" class=""><span id="mce_47" class=""><br></span></span></span></a><div><h6 id="mce_48" class="">Escola activa, familiar, innovadora i acollidora</h6><span id="mce_49" class="">Participació dels pares amb les activitats</span></div><p id="mce_50" class=""><b>Escola activa, familiar, innovadora i acollidora</b> Participació dels pares amb les activitats</p></div></li></ul>
                    </div>

                </div>

            </div>

        </div>

    </div>
<div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
               <br>
                 <br><br>
                 <br>
                 <br><br>
                 <br>
                 <br><br>
                 <br>
                 <br>
                 <br>
            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

				<!-- Content -->
				<div class="template-main">

					<!-- Header -->
					<h3 id="mce_41" class="template-align-center">Informació <strong>Cita prèvia</strong></h3><div class="template-component-divider template-component-divider-style-2"></div>
					
					<!-- Section -->
					<div data-id="section-1">
<!-- Dropcap -->
<div class="template-component-italic template-margin-top-3 template-margin-bottom-3">

Per demanar informació sobre la preinscripció o sol·licitar cita prèvia es poden utilitzar els canals habituals de l’escola:


				</div>

			</div>
					
					</div>
					

					<!-- Preformatted text -->
					<div class="template-component-preformatted-text" data-id-rel="section-1">
						
						<pre></pre>
						
					</div>
					

					<!-- Header -->
					

					<!-- Section -->
					

					
						
					</div>							

				</div>
<div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">


                        
            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">


                        
                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                    </div>

                </div>

                <!-- Right column -->
                <!-- Content -->
<div class="template-content">

    <!-- Main -->
    <div class="template-content-section template-main">
        

        <!-- Feature -->
        <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">

            <!-- Layout 33x33x33 -->
            <ul class="template-layout-33x33x33 template-clear-fix">

                <!-- Left column -->
                <li class="template-layout-column-left">
                    <div class="template-icon-feature template-icon-feature-name-envelope-alt"></div>
                    <h5>Adreça</h5>
                    <p>
                        Carrer Capellades, 2<br/>
                        08700 Igualada<br/>
                        Centre Concertat GC
                    </p>
                </li>

                <!-- Center column -->
                <li class="template-layout-column-center">
                    <div class="template-icon-feature template-icon-feature-name-mobile-alt"></div>
                    <h5>Telèfon i email</h5>
                    <p>
                        Tel: 93 803 15 77<br/>
                        Mòbil: 608 188 004<br/>
                        <a href="mailto:office@fable.com">info@monalco.cat</a>
                    </p>
                </li>

                <!-- Right column -->
                <li class="template-layout-column-right template-margin-bottom-5">
                    <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                    <h5>Horaris Secretaria</h5>
                    <p>
                        Dilluns a divendres<br/>
                        8h – 19h<br/>
                        Cap de setmana tancat
                    </p>
                </li>

            </ul>

        </div>
        <!-- Contact form -->
        <div class="template-component-contact-form" id="contactenosform">

            <form action="<?= base_url('paginas/frontend/contacto') ?>" method="post">
                
                <!-- Layout 50x50 -->
                <div class="template-layout-50x50 template-clear-fix">

                    <!-- Left column -->
                    <div class="template-layout-column-left">

                        <!-- Name -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-name">Nom *</label>
                            <input type="text" name="nombre" id="contact-form-name" />
                        </div>

                        <!-- E-mail -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-email"> E-mail *</label>
                            <input type="text" name="email" id="contact-form-email" />
                        </div>

                        <!-- Subject -->
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-subject">Curs</label>
                            <input type="text" name="tema" id="contact-form-subject" />
                        </div>

                    </div>

                    <!-- Right column -->
                    <div class="template-layout-column-right">
                        <div class="template-form-line template-state-block">
                            <label for="contact-form-message">Missatge *</label>
                            <textarea rows="1" cols="1" name="message" id="contact-form-message"></textarea>
                        </div>
                    </div>

                </div>
                <div style="margin:10px">
                    <?php 
                        echo @$_SESSION['msj'];
                        unset($_SESSION['msj']);
                    ?>
                </div>
                <div class="template-form-line template-align-center">
                    <div style="display:inline-block; width:auto;">
                        <div class="g-recaptcha" data-sitekey="6LcjXm0UAAAAAMO1NqE3fW0ITNfL1l0xxSz6uXeN"></div>
                    </div>
                </div>

                <div class="template-form-line template-state-block" style="text-align:center; margin:20px;">
                    <input name="politicas" id="contact-form-subject" placeholder="Curs i informació sol·licitud matrícula" type="checkbox"> He llegit i accepto la <a href="<?= base_url('aviso-legal.html') ?>" target="_new">política de privacitat*</a>
                </div>

                <div class="template-form-line template-form-line-submit template-align-center">

                    <div class="template-state-block">

                        <!-- Submit button -->
                        <input class="template-component-button template-component-button-style-1" type="submit" value="enviar missatge" name="contact-form-submit" id="contact-form-submit"/>

                    </div>

                </div>

            </form>

        </div>


            </div>


            </div>

        </div>

    </div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div><div style="text-align:center;" class="saveSectionDiv"></div>

				<!-- Sidebar -->
				<div class="template-content-layout-column-right">

					

				</div>

			</div><div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
                <ul class="template-layout-25x25x25x25 template-clear-fix" id="mce_54"><li class="template-layout-column-left"><div class="template-icon-feature template-icon-feature-name-teddy-alt"><br></div><h5 id="mce_55" class="">Potenciar</h5><p id="mce_56" class="">Totes les persones tenim talents que cal descobrir i potenciar</p></li><li class="template-layout-column-center-left"><div class="template-icon-feature template-icon-feature-name-heart-alt"><br></div><h5 id="mce_57" class="">Ajudar</h5><p id="mce_58">Totes les persones necessitem ajuda davant les dificultats</p></li><li class="template-layout-column-center-right"><div class="template-icon-feature template-icon-feature-name-graph-alt"><br></div><h5 id="mce_59">Orientar</h5><p id="mce_60" class="">Totes les persones, amb una bona orientació, podem assolir els nostres propòsits</p></li><li class="template-layout-column-right"><div class="template-icon-feature template-icon-feature-name-globe-alt"><br></div><h5 id="mce_61" class="">Acompanyar</h5><p id="mce_62">L'objectiu de la nostra escola és una formació integral de l'alumne i posem eines per aconseguir-ho</p></li></ul>
            </div>

        </div>

    </div>

    <!-- Section -->

                        