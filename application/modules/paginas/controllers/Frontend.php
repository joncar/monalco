<?php 
require_once APPPATH.'/controllers/Panel.php';    
class Frontend extends Main{        
    function __construct() {
        parent::__construct();
        $this->load->model('querys');
        $this->load->model('elements');
        $this->load->library('form_validation');
    }        
    private $restricted = array('quotes');
    function read($url){
        if(!$this->user->log && in_array($url,$this->restricted)){
            redirect('main');
        }
        $theme = $this->theme;
        $params = $this->uri->segments;
        $this->load->model('querys');            
        $this->loadView(
            array(
                'view'=>'read',
                'theme'=>$url,
                'page'=>$this->load->view($theme.$url,array(),TRUE),
                'title'=>ucfirst(str_replace('-',' ',$url))                    
            )
        );
    }
    
    function getFormReg($x = '2'){                    
        return $this->querys->getFormReg($x);
    }
    
    function editor($url){            
        $this->load->helper('string');
        if(!empty($_SESSION['user']) && $this->user->admin==1){                
            //$page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
            /*$page = str_replace('<?php','[?php',$page);
            $page = str_replace('<?=','[?=',$page);
            $page = str_replace('&gt;','>',$page);
            $page = str_replace('&lt;','<',$page);*/
            $page = $this->load->view($url,array(),TRUE);
            $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
        }else{
            redirect(base_url());
        }
    }
    
    function contacto($ajax = ''){
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('nombre','Nom','required');
        $this->form_validation->set_rules('message','Comentario','required');
        $this->form_validation->set_rules('politicas','Politicas','required');
        if($this->form_validation->run()){
            $this->load->library('recaptcha');
            if(empty($_POST['echo']) && !$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                $_SESSION['msj'] = $this->error('<span style="color:red">Captcha introduit incorrectament</span>');
            }else{     
                $datos = $_POST;
                if(!empty($_POST['extras'])){
                    unset($datos['extras']);
                    $datos['extras'] = '';
                    foreach($_POST['extras'] as $n=>$v){
                        $datos['extras'].= '<p><b>'.$n.': </b> '.$v.'</p>';
                    }
                }else{
                    $datos['extras'] = '';
                }

                if(empty($_POST['asunto'])){
                    $datos['asunto'] = '';
                }
                $emails = 'infoweb@monalco.cat';
                if(!empty($_POST['to'])){
                    unset($datos['to']);
                    $emails = $_POST['to'];
                }
                if(!empty($_POST['echo'])){
                    unset($datos['echo']);
                }
                foreach(explode(',',$emails) as $email){
                    $this->enviarcorreo((object)$datos,1,$email);                
                }
                $_SESSION['msj'] = $this->success('<span style="color:green">Gràcies per contactar-nos, en breu ens posarem en contacte amb vostè</span>');
                if(!empty($_POST['echo'])){
                    echo $_SESSION['msj'];
                    unset($_SESSION['msj']);
                    die();
                }
            }                        
        }else{                
           $_SESSION['msj'] = $this->success('<span style="color:red">Si us plau completi les dades sol·licitades</span>');               
        }     
        if($ajax=='ajax'){
            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
            die();
        }
        if(!empty($_GET['redirect'])){
            redirect($_GET['redirect']);
        }else{
            redirect(base_url('contacte.html').'#contactenosform');
        }
    }
    
    function enviarCurriculum(){
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('nombre','Nombre','required');
        $this->form_validation->set_rules('mensaje','Comentario','required');
        //$this->form_validation->set_rules('curriculum','Curriculum','required');            
        if($this->form_validation->run() && !empty($_FILES['curriculum'])){
            get_instance()->load->library('mailer');
            get_instance()->mailer->mail->AddAttachment($_FILES['curriculum']['tmp_name'],$_FILES['curriculum']['name']);
            $this->enviarcorreo((object)$_POST,2,'info@finques-sasi.com');
            //$_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
            echo json_encode(array('success'=>TRUE,'message'=>'Gracias por contactarnos, en breve le contactaremos'));
        }else{
            //$_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
           echo json_encode(array('success'=>FALSE,'message'=>'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>'));
        }
        /*if(!empty($_GET['redirect'])){
            redirect($_GET['redirect']);
        }else{
            redirect(base_url('p/contacto'));
        }*/
    }
    
    function subscribir(){
        $this->form_validation->set_rules('email','Email','required|valid_email');
        if($this->form_validation->run()){
            $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
            $success = $emails->num_rows()==0?TRUE:FALSE;
            if($success){
                $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                echo $this->success('Subscrito satisfactoriamente');
            }else{
                echo $this->error('Correo ya existente');
            }
        }else{
            echo $this->error($this->form_validation->error_string());
        }
    }
    
    function unsubscribe(){
        if(empty($_POST)){
            $this->loadView('includes/template/unsubscribe');
        }else{
            $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
            $success = $emails->num_rows()>0?TRUE:FALSE;
            if($success){
                $this->db->delete('subscritos',array('email'=>$_POST['email']));
                echo $this->success('Correo desafiliado al sistema de noticias');
            }            
            $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
        }
    }
    
    function pdf(){
        require_once APPPATH.'libraries/html2pdf/html2pdf.php';
        $papel = 'L';
        $orientacion = 'P';
        $html2pdf = new HTML2PDF();
        $html2pdf->setDefaultFont('raleway');
        $html2pdf->addFont("ralewayb","B");
        $html2pdf->addFont("titanone","");
        $menu = $this->db->get('menu_del_dia')->row();
        $html = $menu->pdf;
        foreach($menu as $n=>$v){
            $html = str_replace('['.$n.']', str_replace('&bull;','<br/> &bull;',strip_tags($v)),$html);
        }
        $html = str_replace('[precio]',$this->db->get('ajustes')->row()->precio_menu_dia.'€',$html);
        $html = $this->load->view('pdf',array('html'=>$html),TRUE);
        $html2pdf->writeHTML($html); 
        //echo $this->db->get('menu_del_dia')->row()->pdf;
        ob_end_clean();         
        $html2pdf->Output('Menu-del-dia-'.date("dmY").'.pdf');
    }

    function galeria(){
        $galeria = $this->db->get_where('categoria_galeria');
        foreach($galeria->result() as $n=>$g){
            $this->db->order_by('orden','ASC');
            $galeria->row($n)->fotos = $this->db->get_where('galeria',array('categoria_galeria_id'=>$g->id));
        }
        $page = $this->load->view('_galeria',array('galeria'=>$galeria),TRUE);

        $this->loadView(
            array(
                'view'=>'read',
                'theme'=>'galeria',
                'page'=>$page,
                'title'=>'Galeria'                   
            )
        );
    }

    function zona_privada(){

        if($this->user->log){
            $galeria = $this->db->get_where('categoria_fotos_privada');
            foreach($galeria->result() as $n=>$g){
                $this->db->order_by('orden','ASC');
                $galeria->row($n)->fotos = $this->db->get_where('galeria_privada',array('categoria_galeria_id'=>$g->id));
            }
            $page = $this->load->view('_zona-privada',array('galeria'=>$galeria),TRUE);

            $this->loadView(
                array(
                    'view'=>'read',
                    'theme'=>'zona-privada',
                    'page'=>$page,
                    'title'=>'Zona privada'                   
                )
            );
        }else{
            throw new exception('Usted no posee los permisos necesarios para entrar a esta zona',403);
        }
    }

    function videos(){
        $galeria = $this->db->get_where('categoria_videos');
        foreach($galeria->result() as $n=>$g){
            $this->db->order_by('orden','ASC');
            $galeria->row($n)->videos = $this->db->get_where('videos',array('categoria_videos_id'=>$g->id));
        }
        $page = $this->load->view('_videos',array('videos'=>$galeria),TRUE);

        $this->loadView(
            array(
                'view'=>'read',
                'theme'=>'videos',
                'page'=>$page,
                'title'=>'Videos'                   
            )
        );
    }

    function buscar(){
        if(!empty($_GET['q'])){
            $this->db->or_like('texto1',$_GET['q']);
            $this->db->or_like('texto2',$_GET['q']);
        }
        $blog = $this->db->get_where('buscador');
        $page = $this->load->view('buscador',array('resultados'=>$blog,'q'=>$_GET['q']),TRUE);        
        $page = str_replace('[header]',$this->load->view('includes/template/header',array(),TRUE),$page);
        $this->loadView(array('view'=>'read','page'=>$page));
    }

    function popup(){
        $popup = $this->db->get('ajustes')->row()->popup;
        if(!empty($popup)){
            $popup = $this->db->get_where('popups',array('id'=>$popup))->row();
            $this->load->view('popups/show',array('popup'=>$popup));
        }
    }
}
