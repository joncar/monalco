<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Informaciones extends Panel{
        function __construct() {
            parent::__construct();            
        }
        
        function informaciones_mensuales(){
        	$crud = $this->crud_function('','');
            $crud->set_field_upload('pdf','files');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>