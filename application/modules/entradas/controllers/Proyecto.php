<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Proyecto extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function proyectos(){
            $crud = $this->crud_function("",""); 
            $crud->set_field_upload('foto','img/proyectos'); 
            $crud->columns('proyectos_categorias_id','proyectos_nombre');   
            $crud->set_field_upload('icono','img/icon');      
            $crud->add_action('Adm. Fotos','',base_url('entradas/proyecto/proyectos_fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function proyectos_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('proyectos_fotos')
                      ->set_image_path('img/proyectos')
                      ->set_url_field('foto')
                      ->set_ordering_field('orden')
                      ->set_relation_field('proyectos_id');
            $crud->module = "entradas";
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
