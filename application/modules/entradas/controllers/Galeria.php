<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Galeroa extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function categorias_fotos(){
            $crud = $this->crud_function("","");                         
            $crud->add_action('Adm. Fotos','',base_url('entradas/galeria/fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('galeria')
                      ->set_image_path('img/galeria')
                      ->set_url_field('foto')
                      ->set_ordering_field('orden')
                      ->set_relation_field('categorias_fotos_id');
            $crud->module = "entradas";
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
