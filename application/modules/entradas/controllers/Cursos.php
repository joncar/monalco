<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Cursos extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function cursos_niveles(){
        	$crud = $this->crud_function('','');      
        	$crud->field_type('tipo','dropdown',array('1'=>'Primaria','2'=>'Secundaria'));    
        	$crud->set_field_upload('foto','img/cursos');
        	$crud = $crud->render();        	
            $this->loadView($crud);
        }
    }
?>
