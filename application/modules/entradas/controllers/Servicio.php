<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Servicio extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function servicios(){
            $crud = $this->crud_function("","");            
            $crud->add_action('Adm. Fotos','',base_url('entradas/servicio/servicios_fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function servicios_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('servicios_fotos')
                      ->set_url_field('foto')
                      ->set_image_path('img/servicios')
                      ->set_ordering_field('orden')
                      ->set_relation_field('servicios_id');
            $crud->module = "entradas";
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
