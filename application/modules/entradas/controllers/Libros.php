<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Libros extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function libros(){
            $crud = $this->crud_function("",""); 
            $crud->set_relation('cursos_niveles_id','cursos_niveles','curso');           
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
