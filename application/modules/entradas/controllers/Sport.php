<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Sport extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function sports(){
            $crud = $this->crud_function("","");                         
            $crud->set_field_upload('foto','img/sports');
            $crud->set_field_upload('horarios','img/sports');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
