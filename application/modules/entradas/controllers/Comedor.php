<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Comedor extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function comedor_menu(){
        	$crud = $this->crud_function('','');      
        	$crud->set_subject('Comedor');      
        	$crud = $crud->render();
        	$crud->title = 'Definir Menu del comedor';
            $crud->output = '<a href="'.base_url('entradas/comedor/comedor_menu_descarga').'" class="btn btn-info">Menu en descarga</a>'.$crud->output;
            $this->loadView($crud);
        }

        function comedor_menu_descarga(){
            $crud = $this->crud_function('','');      
            $crud->set_subject('Comedor en pdf'); 
            $crud->set_field_upload('fichero','files/menjador');     
            $crud = $crud->render();
            $crud->title = 'Definir Menu del comedor para descarga';
            $this->loadView($crud);
        }
    }
?>
