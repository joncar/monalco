<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        
        
        public function servicios($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $servicios = new Bdsource();
                $servicios->where('id',$id);
                $servicios->init('servicios',TRUE);
                
                $this->loadView(
                    array(
                        'view'=>'servicios',
                        'detail'=>$this->servicios,
                        'title'=>$this->servicios->servicios_nombre
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }

        public function documentos(){
            if(!$this->user->log){
                redirect('main');
            }
            $this->db->order_by('id','ASC');
            $documentos = $this->db->get_where('categorias_documentos',array('tipo'=>1,'mostrar_en_pagina_documentos'=>1));
            foreach($documentos->result() as $n=>$g){
                $documentos->row($n)->subcategorias = $this->db->get_where('subcategorias_documentos',array('categorias_documentos_id'=>$g->id));
                foreach($documentos->row($n)->subcategorias->result() as $nn=>$s){
                    $this->db->order_by('orden','ASC');
                    $documentos->row($n)->subcategorias->row($nn)->documentos = $this->db->get_where('documentos',array('subcategorias_documentos_id'=>$s->id,'tipo'=>1));                    
                }
            }
            $this->loadView(
                array(
                    'view'=>'documentos',
                    'documentos'=>$documentos,
                    'title'=>'Documents'
                ));
        }

        public function autorizaciones(){
            if(!$this->user->log){
                redirect('main');
            }
            $this->db->order_by('id','ASC');
            $documentos = $this->db->get_where('categorias_documentos',array('tipo'=>2));
            foreach($documentos->result() as $n=>$g){
                $documentos->row($n)->subcategorias = $this->db->get_where('subcategorias_documentos',array('categorias_documentos_id'=>$g->id));
                foreach($documentos->row($n)->subcategorias->result() as $nn=>$s){
                    $documentos->row($n)->subcategorias->row($nn)->documentos = $this->db->get_where('documentos',array('subcategorias_documentos_id'=>$s->id,'tipo'=>2));                    
                }
            }
            $this->loadView(
                array(
                    'view'=>'autorizaciones',
                    'documentos'=>$documentos,
                    'title'=>'Documents'
                ));
           
        }

        public function becas(){
            $this->db->order_by('nombre','ASC');
            $documentos = $this->db->get_where('documentos',array('tipo'=>3));            
            $this->loadView(
                array(
                    'view'=>'becas',
                    'documentos'=>$documentos,
                    'title'=>'beques-i-subvencions'
            ));
           
        }
        
        
        
        public function proyectos($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $proyectos = new Bdsource();
                $proyectos->where('id',$id);
                $proyectos->init('proyectos',TRUE);
                $this->proyectos->portada = base_url('img/proyectos/'.$this->proyectos->foto);
                $this->proyectos->fotos = $this->db->get_where('proyectos_fotos',array('proyectos_id'=>$this->proyectos->id));
                foreach($this->proyectos->fotos->result() as $n=>$f){
                    $this->proyectos->fotos->row($n)->foto = base_url('img/proyectos/'.$f->foto);
                }
                $this->loadView(
                    array(
                        'view'=>'proyectos',
                        'detail'=>$this->proyectos,
                        'title'=>$this->proyectos->proyectos_nombre
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }

        public function comedor_menu(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_theme('bootstrap2')
                 ->set_table('comedor_menu')
                 ->set_subject('Comedor')
                 ->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();            
            $crud = $crud->render();            
        }

        public function extraescolars(){
            $this->loadView(array('view'=>'extraescolar','title'=>'Extraescolars'));
        }
        
    }
?>
