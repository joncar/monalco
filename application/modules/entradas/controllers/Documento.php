<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Documento extends Panel{
        function __construct() {
            parent::__construct();
        }

        function categorias_documentos(){
            $crud = $this->crud_function("","");    
            $crud->field_type('tipo','dropdown',array('1'=>'Documentos','2'=>'Autorizaciones'));        
            $crud = $crud->render();  
            $crud->output = '<a href="'.base_url('entradas/documento/documentos').'" class="btn btn-info">Volver a documentos</a>'.$crud->output;
            $this->loadView($crud);
        }

        function subcategorias_documentos(){
            $crud = $this->crud_function("","");            
            $crud = $crud->render();      
            $crud->output = '<a href="'.base_url('entradas/documento/documentos').'" class="btn btn-info">Volver a documentos</a>'.$crud->output;      
            $this->loadView($crud);
        }

        
        function documentos(){
            $crud = $this->crud_function("","");
            $crud->set_field_upload('fichero','files/documentos');
            $crud->set_relation('categorias_documentos_id','view_tipo','{tipo_doc} - {nombre}');
            $crud->set_primary_key('id','view_tipo');
            $crud->set_relation('subcategorias_documentos_id','subcategorias_documentos','nombre');
            $crud->set_relation_dependency('subcategorias_documentos_id','categorias_documentos_id','categorias_documentos_id');
            $crud->field_type('tipo','dropdown',array('1'=>'Documentos','2'=>'Autorizaciones','3'=>'Becas'));            
            $crud = $crud->render();
            $crud->output = $this->load->view('admin_documentos',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }
    }
?>
