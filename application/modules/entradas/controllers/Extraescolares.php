<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Extraescolares extends Panel{
        function __construct() {
            parent::__construct();
        }

        function get_lists($apartado,$where){
        	$this->as['extraescolar'] = $apartado;
        	$crud = $this->crud_function('','');
        	$crud->where('extraescolar_id',$where)
        		 
        		 ->set_url('entradas/extraescolares/'.$apartado.'/'.$where.'/');
        	$crud = $crud->render(1);
        	return $crud;
        }

        function extraescolar_apartados($x){
        	$crud = $this->crud_function('','');
        	$crud->where('extraescolar_id',$x)
        		 ->field_type('extraescolar_id','hidden',$x)
        		 ->unset_back_to_list();
        	$crud = $crud->render();
        	$crud->output = '<a class="btn btn-info" href="'.base_url('entradas/extraescolares/extraescolar/edit/'.$x).'">Volver a extraescolares</a>'.$crud->output;
        	$this->loadView($crud);
        }
        function extraescolar_precios($x){
        	$crud = $this->crud_function('','');
        	$crud->where('extraescolar_id',$x)
        		 ->field_type('extraescolar_id','hidden',$x)
        		 ->unset_back_to_list();
        	$crud = $crud->render();
        	$crud->output = '<a class="btn btn-info" href="'.base_url('entradas/extraescolares/extraescolar/edit/'.$x).'">Volver a extraescolares</a>'.$crud->output;
        	$this->loadView($crud);
        }
        
        function extraescolar($x = '',$y = ''){
            $crud = $this->crud_function("","");
            $crud->set_field_upload('icono','img/extraescolares');
            $crud->set_field_upload('foto1','img/extraescolares');
            $crud->set_field_upload('foto2','img/extraescolares');
            $crud->set_field_upload('foto3','img/extraescolares');
            $crud->set_lang_string('insert_success_message','Se ha guardado con éxito <script>document.location.href="'.base_url('entradas/extraescolares/extraescolar/edit').'/{id}";</script>');
            $output = $crud->render();
            if($crud->getParameters()=='edit'){
            	$output->apartados = $this->get_lists('extraescolar_apartados',$y);
            	$output->precios = $this->get_lists('extraescolar_precios',$y);
            	$output->output = $this->load->view('admin_extraescolares',array('output'=>$output),TRUE);
            	//$output->js_files = array_merge($output->js_files,$output->apartados->js_files);
            	$output->css_files = array_merge($output->css_files,$output->apartados->css_files);
            }
            
            $this->loadView($output);
        }
    }
?>
