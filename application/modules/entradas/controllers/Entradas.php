<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Entradas extends Panel{
        function __construct() {
            parent::__construct();
        }

        function popups(){
            $crud = $this->crud_function('','');                
            $crud->field_type('foto','image',array('path'=>'img/popups','width'=>'800px','height'=>'614px'))
                 ->field_type('campos','editor',array('type'=>'textarea'))
                 ->callback_column('campos',function($val,$row){return strip_tags($val);})
                 ->set_clone();
            
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function revista_anual(){
        	$crud = $this->crud_function('','');              	
        	$crud->set_field_upload('fichero','files/revista_anual');
        	$crud->unset_add()
        		 ->unset_delete()
        		 ->unset_read()
        		 ->unset_print()
        		 ->unset_export();
        	$crud = $crud->render();        	
            $this->loadView($crud);
        }
    }
?>
