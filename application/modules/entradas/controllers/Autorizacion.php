<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Autorizacion extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function autorizaciones(){
            $crud = $this->crud_function("","");
            $crud->set_field_upload('fichero','files/autorizaciones');
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
