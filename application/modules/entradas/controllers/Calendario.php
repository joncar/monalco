<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Calendario extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function setCalendario(){
        	$this->as['setCalendario'] = 'calendario';
            $crud = $this->crud_function("","");            
            $crud->set_field_upload('fichero','files/autorizaciones');
            //$crud->field_type('color','dropdown',array('1'=>'FESTIU','2'=>'DIA DE LLIURE ELECCIÓ','3'=>'LABORABLE LLAR INFANTS','4'=>'LLAR DE 9H A 13H'));
            $crud->set_relation('color','calendario_colores','nombre');
            $crud->set_ruleS('fecha','Fecha','required|is_unique[calendario.fecha]');
            $action = $crud->getParameters();
            $crud = $crud->render();    
            $crud->output = '<a href="'.base_url('entradas/calendario/calendario_descarga').'" class="btn btn-info">Calendario descargable</a> <a href="'.base_url('entradas/calendario/calendario_colores').'" class="btn btn-info">Definir colores</a>'.$crud->output;        
            $crud->title = 'Calendario';
            $this->loadView($crud);
        }

        function calendario_colores(){
            $crud = $this->crud_function('','');      
            $crud->set_subject('Colores'); 
            $crud->callback_field('color',function($val){
                return '<input type="color" name="color" id="field-color" class="form-control" value="'.$val.'">';
            });
            $crud = $crud->render();
            $crud->title = 'Definir colores del calendario';
            $this->loadView($crud);
        }

        function calendario_descarga(){
            $crud = $this->crud_function('','');      
            $crud->set_subject('Calendario en pdf'); 
            $crud->set_field_upload('calendario','files/calendario');     
            $crud = $crud->render();
            $crud->title = 'Definir calendario descargable';
            $this->loadView($crud);
        }

        function vercalendario(){
            $this->loadView(array('view'=>'calendario-escolar','title'=>'Calendari escolar'));
        }
    }
?>
