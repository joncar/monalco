<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class CalendarioFrontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function setCalendario(){
        	$this->as['setCalendario'] = 'calendario';
            $crud = $this->crud_function("","");            
            $crud->set_field_upload('fichero','files/autorizaciones');
            $crud->field_type('color','dropdown',array('1'=>'Verde','2'=>'Rojo','3'=>'Azul'));
            $crud->set_ruleS('fecha','Fecha','required|is_unique[calendario.fecha]');
            $action = $crud->getParameters();
            $crud = $crud->render();            
            $crud->title = 'Calendario';
            $this->loadView($crud);
        }

        function vercalendario(){
            $class = '<style>';
            $colores = $this->db->get_where('calendario_colores');
            foreach($colores->result() as $c){
                $class.= '.color'.$c->id.'{background: '.$c->color.';}';
            }
            $class.= '</style>';
            $this->loadView(array('view'=>'calendario-escolar','title'=>'Calendari escolar','customStyle'=>$class));
        }
    }
?>
