<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="<?= base_url('entradas/documento/categorias_documentos') ?>">Categorias</a></li>
    <li role="presentation"><a href="<?= base_url('entradas/documento/subcategorias_documentos') ?>">Subcategorias</a></li>
    <li role="presentation" class="active"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Documentos</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="home">...</div>
    <div role="tabpanel" class="tab-pane" id="profile">...</div>
    <div role="tabpanel" class="tab-pane active" id="messages">
		<?= $output ?>
    </div>
  </div>

</div>