<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home"  role="tab" data-toggle="tab">Ficha</a></li>
    <li role="presentation"><a href="#profile"  role="tab" data-toggle="tab">Apartados</a></li>
    <li role="presentation"><a href="#messages"  role="tab" data-toggle="tab">Precios</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home"><?= $output->output ?></div>
    <div role="tabpanel" class="tab-pane" id="profile"><?= $output->apartados->output ?></div>
    <div role="tabpanel" class="tab-pane" id="messages"><?= $output->precios->output ?></div>
  </div>

</div>