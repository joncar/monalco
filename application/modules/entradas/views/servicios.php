<?php 
if($detail->id!=2){
	$page = $this->load->view('servicios',array(),TRUE,'paginas');
}else{
	$page = $this->load->view('menjador',array(),TRUE,'paginas');
}

$page = str_replace('[header]',$this->load->view('includes/template/header',array(),TRUE),$page);
$page = str_replace('[base_url]',base_url(),$page);

echo $page; 
?>
<div id="ContentComedor" style="display:none">
	

<!-- Section -->
<div class="template-content-section template-padding-bottom-5 template-clear-fix template-background-color-2">

	<!-- Main -->
	<div class="template-main">

		<!-- Section -->
		<div data-id="section-2">

				<!-- Pricing plans -->
			<div class="template-component-pricing-plan template-component-pricing-plan-style-2">
				<!-- Layout 33x33x33 -->
				<ul class="template-layout-50x50 template-clear-fix">
					
					<li class="template-layout-column-center template-component-pricing-plan-background-1">
						<div>
							<div class="hidden-xs" style="background:url(<?= base_url('img/comedor2.jpg') ?>); width:27%; height:307px;position: absolute;right: 2px;top: 0px;"></div>
							<!-- Price -->
							<div class="template-component-pricing-plan-price menuFechaDiv">
								<span id="fecha" style="font-size: 30px;"></span>								
							</div>

							<div class="template-component-list template-component-list-style-1" style="height:197px">
									<ul>
										<li>
											<h5 style="margin-bottom: 0px; color:lightgray; font-weight:300" class="template-component-pricing-plan-header">Primer plat </h5>
											<div style="margin-top:0px" class="template-component-pricing-plan-feature" id="primero">Algo</div>
										</li>
										<li>
											<h5 style="margin-bottom: 0px; color:lightgray; font-weight:300" class="template-component-pricing-plan-header">Segon plat </h5>
											<div style="margin-top:0px" class="template-component-pricing-plan-feature" id="segundo">Algo</div>
										</li>
										<li>
											<h5 style="margin-bottom: 0px; color:lightgray; font-weight:300" class="template-component-pricing-plan-header">Postres </h5>
											<div style="margin-top:0px" class="template-component-pricing-plan-feature" id="postres">Algo</div>
										</li>										
									</ul>
								</div>
						</div>
					</li>
					

					
				</ul>
			</div>
		</div>
	</div>
</div>
</div>


</div>
<link rel="stylesheet" type="text/css" href="<?= base_url('css/template/bootstrap.datepicker.css') ?>">
<script src="<?= base_url('js/template/bootstrap.datepicker.js') ?>"></script>
<script>
	var year = <?php

		$year = date("Y");		
		echo $year;

	?>;
	var month = <?php 
		$mes = date("m");
		echo $mes;
	?>;
	var fechas = <?php 
		$fechas = array();
		foreach($this->db->get_where('comedor_menu',array('YEAR(fecha) >='=>$year))->result() as $f){
			$fechas[] = $f->fecha;
		}
		echo json_encode($fechas);
	?>;
	

	function getColor(color){
		switch(color){
			case '1':return 'green';
			case '2':return 'red';
			case '3':return 'blue';
			case '4':return 'yellow';
		}
	}

	function beforeshowday(date){
		var da = date;
        var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
        var m = (da.getMonth()+1);
        m = m<10?'0'+m:m;
        var d = da.getFullYear()+'-'+m+'-'+dia;
        return fechas.indexOf(d)<0?{enabled:false}:date;
	}

	function changeDay(e){
		var newDate    = new Date(e.date),
            newDateStr =  newDate.getFullYear()+ '-' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '-' + ("0" + newDate.getDate()).slice(-2);
        $.post('<?= base_url('entradas/frontend/comedor_menu/json_list') ?>',{
        	'search_field[]':'fecha',
        	'search_text[]':newDateStr
        },function(data){
        	var data = JSON.parse(data);
        	if(data.length>0){
        		data = data[0];
        		for(var i in data){
        			$("#"+i).html(data[i]);
        		}
        		jQuery.fancybox.open({href: "#ContentComedor"});
        	}else{
        		alert("Menu no encontrado");
        	}

        });  
	}

	
	<?php 
		$mesa = $mes-1;
		for($i=0;$i<12;$i++): 			
			$mesa = strtotime(date("Y-m-d").' +'.$i.' months');			
	?>
		$("#datetimepicker<?= ($i+1) ?>-inline").datepicker({
		        format:    "dd-mm-yyyy",
		        defaultViewDate: {year:<?= (int)date("Y",$mesa) ?>, month:<?= (int)date("m",$mesa)-1 ?>, day:1},	        
		        weekStart: 1,
		        beforeShowDay:beforeshowday  
		}).on('changeDate',changeDay);
	<?php endfor; ?>
	


</script>