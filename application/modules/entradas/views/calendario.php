<style>
	.tipos, .tipos li{
		margin:0px;
		padding:0px;
	}
	.tipos li{
		display:inline-block;

	}

	.festiu, .eleccion, .laborable, .del{
		width:30px;
		height:30px;
		display: inline-block;
		vertical-align: middle;
		margin: 10px;
	}

	.festiu{
		background:green;
	}

	.eleccion{
		background: red;
	}

	.laborable{
		background: blue;
	}
	.del{
		border:1px solid gray;
	}
</style>
<ul class="tipos">
	<li><label><span class="festiu"></span> <input type="radio" name="tipo" class="tipo" value="1" checked=""> FESTIU </label></li>
	<li><label><span class="eleccion"></span> <input type="radio" name="tipo" class="tipo" value="2"> DIA DE LLIURE ELECCIÓ</label></li>
	<li><label><span class="laborable"></span> <input type="radio" name="tipo" class="tipo" value="3"> LABORABLE LLAR INFANTS</label></li>
	<li><label><span class="del"></span> <input type="radio" name="tipo" class="tipo" value="-1"> Eliminar</label></li>
</ul>
<div id="datetimepicker12-inline" class="menjador">
</div>
<link rel="stylesheet" type="text/css" href="<?= base_url('css/template/bootstrap.datepicker.css') ?>">
<script src="<?= base_url('js/template/bootstrap.datepicker.js') ?>"></script>
<script>
	var festiu = [];
	var eleccion = [];
	var laborable = [];
	
	$("#datetimepicker12-inline").datepicker({
	        format:    "dd-mm-yyyy",
	        startDate: new Date(),       
	        weekStart: 1,
	        beforeShowDay: function(date){

	            if(typeof(reservas)!=='undefined'){
	                var da = date;
	                var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
	                var m = (da.getMonth()+1);
	                m = m<10?'0'+m:m;
	                var d = da.getFullYear()+'-'+m+'-'+dia;
	                console.log(d);
	                return reservas.indexOf(d)>=0?:date;
	            }
	            return date;
	        }  
	});

	console.log($("#datetimepicker12-inline"));
</script>