<link rel="stylesheet" type="text/css" href="<?= base_url('css/template/bootstrap.datepicker.css') ?>">
<script src="<?= base_url('js/template/bootstrap.datepicker.js') ?>"></script>
<?php 
	$page = $this->load->view('calendari-escolar',array(),TRUE,'paginas');
	$page = str_replace('[header]',$this->load->view('includes/template/header',array(),TRUE),$page);
	$page = str_replace('[base_url]',base_url(),$page);
	echo $page;
?>
<script>
	var dias = <?php 
		$year = date("Y");
		$anio = $this->db->query('SELECT MAX(YEAR(fecha)) as year from calendario');
		if($anio->num_rows()>0){
			$year = $anio->row()->year;
		}
		$calendario = $this->db->get_where('calendario',array('YEAR(fecha) >='=>$year-1));
		$dias = array();
		foreach($calendario->result() as $c){
			$dias[] = array('dia'=>$c->fecha,'color'=>$c->color);
		}
		echo json_encode($dias);
	?>;

	function getColor(color){
		switch(color){
			case '1':return 'green';
			case '2':return 'red';
			case '3':return 'blue';
			case '4':return 'yellow';
		}
	}

	function beforeshowday(date){
		if(typeof(dias)!=='undefined'){
            var da = date;
            var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
            var m = (da.getMonth()+1);
            m = m<10?'0'+m:m;
            var d = da.getFullYear()+'-'+m+'-'+dia;	
            for(var i in dias){
            	if(dias[i].dia===d){
            		return {
            			//classes:getColor(dias[i].color)
            			classes:'color'+dias[i].color
            		}
            	}
            }
        }	
	}

	function changeDay(e){
		var newDate    = new Date(e.date),
            newDateStr =  newDate.getFullYear()+ '-' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '-' + ("0" + newDate.getDate()).slice(-2);
        $.post('<?= base_url('entradas/frontend/comedor_menu/json_list') ?>',{
        	'search_field[]':'fecha',
        	'search_text[]':newDateStr
        },function(data){
        	var data = JSON.parse(data);
        	if(data.length>0){
        		data = data[0];
        		for(var i in data){
        			$("#"+i).html(data[i]);
        		}
        		jQuery.fancybox.open({href: "#ContentComedor"});
        	}else{
        		alert("Menu no encontrado");
        	}

        });  
	}

	var year = <?php

		$year = date("Y");		
		echo $year;

	?>;
	var month = <?php 
		$mes = date("m");
		echo $mes;
	?>;
	var fechas = <?php 
		$fechas = array();
		foreach($this->db->get_where('comedor_menu',array('YEAR(fecha) >='=>$year))->result() as $f){
			$fechas[] = $f->fecha;
		}
		echo json_encode($fechas);
	?>;

	<?php 
		$mesa = $mes-1;
		for($i=0;$i<12;$i++): 			
			$mesa = strtotime(date("Y-m-d").' +'.$i.' months');			
	?>
		$("#datetimepicker<?= ($i+1) ?>-inline").datepicker({
		        format:    "dd-mm-yyyy",
		        defaultViewDate: {year:<?= (int)date("Y",$mesa) ?>, month:<?= (int)date("m",$mesa)-1 ?>, day:1},	        
		        weekStart: 1,
		        beforeShowDay:beforeshowday  
		}).on('changeDate',changeDay);
	<?php endfor; ?>
</script>