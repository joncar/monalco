<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background">
            <div class="template-main">
                <h1>Blog</h1>
                <h6>Darres notícies de l'escola</h6>
            </div>
        </div>
    </div>
</div>





<!-- Content -->
		<div class="template-content">

			<!-- Section -->
			<div class="template-content-section template-content-layout template-content-layout-sidebar-right template-main template-clear-fix ">

				<!-- Content -->
				<div class="template-content-layout-column-left">
					
					<!-- Post list -->
					<ul class="template-post-list">
						<?php foreach ($detail->result() as $d): ?>
						<!-- Post -->
						<li>

							<!-- Post container -->
							<div class="template-post">

								<!-- Header -->
								<div class="template-post-section-header">
									<h4 class="template-post-header">
										<a href="<?= $d->link ?>" title="<?= $d->titulo ?>"><?= $d->titulo ?></a>
									</h4>
									<a href="<?= $d->link ?>" class="template-post-date" title="Veure <?= $d->titulo ?>">
										<?= utf8_encode(strftime("%B, %d %Y",strtotime($d->fecha))) ?>
									</a>
								</div>

								<!-- Icon -->
								<div class="template-post-section-icon">
									<div class="template-post-icon template-post-icon-image"></div>
								</div>

								<!-- Image -->
								<div class="template-post-section-preambule">
									<div class="template-component-image template-preloader">
										<a href="<?= $d->link ?>" title="<?= $d->titulo ?>">
											<img src="<?= $d->foto ?>" alt="" />
											<span><span><span></span></span></span>
										</a>
									</div>
								</div>

								<!-- Meta -->
								<div class="template-post-section-meta">

									<div class="template-post-meta">

										<!-- Tag -->
										<!--<div class="template-icon-blog template-icon-blog-tag">	
											<ul class="template-reset-list">
												<?php foreach (explode(',',$d->tags) as $t): ?>
		                                            <li><a href="<?= $d->link ?>" title="Veure mes"><?= $t ?></a>,&nbsp;</li>
		                                        <?php endforeach ?>
											</ul>
										</div>-->

										<!-- Author -->
		                                <!--<div class="template-icon-blog template-icon-blog-author">
		                                    <a href="<?= $d->link ?>" title=""><?= $d->user ?></a>
		                                </div>
		                                <!-- Comment -->
		                                <!--<div class="template-icon-blog template-icon-blog-comment">
		                                    <a href="<?= $d->link ?>" title=""><?= $d->comentarios ?> Comentaris</a>
		                                </div>	 -->

									</div>

								</div>

								<!-- Excerpt -->
								<div class="template-post-section-content">
									<div class="template-post-content">
										<p><?= strip_tags($d->texto) ?></p>
									</div>
								</div>

								<!-- "Continue reading" button -->
								<div class="template-post-section-button">
									<a href="<?= $d->link ?>" class="template-component-button template-component-button-style-1" title="View post &quot;Drawing and Painting Lessons&quot;">
										Llegir Més<i></i>
									</a>
								</div>

								<!-- Divider -->
								<div class="template-post-section-divider">
									<div class="template-component-divider template-component-divider-style-1"></div>
								</div>

							</div>

						</li>
						<?php endforeach ?>
					</ul>

					 <!-- Pagination -->
			        <div class="template-pagination template-pagination-style-2 template-clear-fix">
			            <ul class="template-clear-fix">
			                <li><a href="javascript:changePage(1)" aria-label="Anterior">&lt;</a></li>
			                <?php 
			                	$desde = empty($_GET['page'])?1:$_GET['page'];	
			                	$desde = $desde>2?$desde-2:$desde;		                	
			                	for ($i = $desde; $i <= $desde+4; $i++): if($i<=$total_pages): ?>
			                    <li><a href="<?= base_url() ?>blog?page=<?= $i ?>" class="<?= $i==$desde?'template-pagination-selected':'' ?>"><?= $i ?></a></li>
			                <?php endif; endfor ?>
			                    <li><a href="<?= base_url() ?>blog?page=<?= !empty($_GET['page'])?$_GET['page']+1:2 ?>)" aria-label="Següent"><span aria-hidden="true">&gt;</span></a></li>
			            </ul>
			        </div>			
				
				</div>

				<!-- Sidebar -->
				<div class="template-content-layout-column-right">
							
					<ul class="template-widget-list">
							

						<!-- Recent posts widget  -->
						<li>
							<h6>
								<span>Últims posts</span>
								<span></span>
							</h6>
							<div>
								<div class="template-widget-recent-post template-widget-recent-post-style-1">
									<ul>
										<?php foreach($recientes->result() as $r): ?>
											<li>
												<a href="<?= $r->link ?>">
													<img src="<?= $r->foto ?>" alt="<?= $r->titulo ?>"/>
												</a>
												<h6>
													<a href="<?= $r->link ?>" title="<?= $r->titulo ?>">
														<?= $r->titulo ?>
													</a>
												</h6>
												<span class="template-icon-blog template-icon-blog-date">
													<?= utf8_encode(strftime("%B, %d %Y",strtotime($d->fecha))) ?>																						
												</span>
											</li>
										<?php endforeach ?>
									</ul>
								</div>										
							</div>
						</li>

						<!-- Categories -->
						<li>
							<h6>
								<span>Categories</span>
								<span></span>
							</h6>
							<div>
								<div class="template-widget-category template-widget-category-style-1">
									<ul>
										<?php foreach($categorias->result() as $c): ?>
											<li>
												<a href="<?= base_url('blog/categorias/'.toUrl($c->id.'-'.$c->blog_categorias_nombre)) ?>" title="<?= $c->blog_categorias_nombre ?>"><?= $c->blog_categorias_nombre ?></a>
											</li>											
										<?php endforeach ?>
									</ul>
								</div>										
							</div>
						</li>

						<!-- Tags -->
						<!--<li>
							<h6>
								<span>Tags</span>
								<span></span>
							</h6>
							<div>
								<div class="template-widget-tag">
									<?php if($detail->num_rows()>0): ?>
										<ul>
											<?php foreach (explode(',',$detail->row()->tags) as $t): ?>
	                                            <li><a href="<?= base_url('blog') ?>?direccion=<?= $t ?>" title="Veure mes"><?= $t ?></a>,&nbsp;</li>
	                                        <?php endforeach ?>
										</ul>
									<?php endif ?>
								</div>
							</div>
						</li>-->

					</ul>				
				
				</div>						

			</div>

		</div>