<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">
        <div class="template-header-bottom-background">
            <div class="template-main">
                <h1>Blog</h1>
                <h6>Darreres notícies de l'escola</h6>
            </div>
        </div>
    </div>
</div>

<!-- Content -->
<div class="template-content">
    <!-- Main -->
    <div class="template-content-section template-main template-clear-fix">
        <!-- Post list -->
        <ul class="template-post-list">
            <?php foreach ($detail->result() as $d): ?>
                <!-- Post -->
                <li>
                    <!-- Post container -->
                    <div class="template-post">
                        <!-- Header -->
                        <div class="template-post-section-header">
                            <h2 class="template-post-header">
                                <a href="<?= $d->link ?>" title="Veure <?= $d->titulo ?>"><?= $d->titulo ?></a>
                            </h2>
                            <a href="<?= $d->link ?>" class="template-post-date" title="Veure <?= $d->titulo ?>">
                                <?= utf8_decode(strftime("%B, %d %Y",strtotime($d->fecha))); ?>                                    
                            </a>
                        </div>
                        <!-- Icon -->
                        <div class="template-post-section-icon">
                            <div class="template-post-icon template-post-icon-image"></div>
                        </div>
                        <!-- Image -->
                        <div class="template-post-section-preambule">
                            <div class="template-component-image template-preloader">
                                <a href="<?= $d->link ?>" title="Veure <?= $d->titulo ?>">
                                    <img src="<?= $d->foto ?>" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                            </div>
                        </div>
                        <!-- Meta -->
                        <div class="template-post-section-meta">
                            <div class="template-post-meta">
                                <!-- Tag -->
                                <div class="template-icon-blog template-icon-blog-tag">	
                                    <ul class="template-reset-list">
                                        <?php foreach (explode(',',$d->tags) as $t): ?>
                                            <li><a href="<?= $d->link ?>" title="Veure mes"><?= $t ?></a>,&nbsp;</li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                                <!-- Author -->
                                <div class="template-icon-blog template-icon-blog-author">
                                    <a href="<?= $d->link ?>" title=""><?= $d->user ?></a>
                                </div>
                                <!-- Comment -->
                                <div class="template-icon-blog template-icon-blog-comment">
                                    <a href="<?= $d->link ?>" title=""><?= $d->comentarios ?> Comentaris</a>
                                </div>	
                            </div>
                        </div>
                        <!-- Excerpt -->
                        <div class="template-post-section-content">
                            <div class="template-post-content">
                                <p><?= cortar_palabras(strip_tags($d->texto), 50) . '...' ?></p>
                            </div>
                        </div>
                        <!-- "Continue reading" button -->
                        <div class="template-post-section-button">
                            <a href="<?= $d->link ?>" class="template-component-button template-component-button-style-1" title="">
                                Llegir Més<i></i>
                            </a>
                        </div>
                        <!-- Divider -->
                        <div class="template-post-section-divider">
                            <div class="template-component-divider template-component-divider-style-1"></div>
                        </div>
                    </div>
                </li>
             <?php endforeach ?>
        </ul>

        <!-- Pagination -->
        <div class="template-pagination template-pagination-style-2 template-clear-fix">
            <ul class="template-clear-fix">
                <li><a href="javascript:changePage(1)" aria-label="Anterior">&lt;</a></li>
                <?php 
                    $desde = empty($_GET['page'])?1:$_GET['page'];  
                    $desde = $desde>2?$desde-2:$desde;                          
                    for ($i = $desde; $i <= $desde+4; $i++): if($i<=$total_pages): ?>
                    <li><a href="<?= base_url() ?>blog?page=<?= $i ?>" class="<?= $i==$desde?'template-pagination-selected':'' ?>"><?= $i ?></a></li>
                <?php endif; endfor ?>
                    <li><a href="<?= base_url() ?>blog?page=<?= !empty($_GET['page'])?$_GET['page']+1:2 ?>)" aria-label="Següent"><span aria-hidden="true">&gt;</span></a></li>
            </ul>
        </div>

    </div>

</div>