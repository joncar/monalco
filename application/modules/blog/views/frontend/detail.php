<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background">
            <div class="template-main">
                <h1><?= $detail->titulo ?></h1>

                <h6><?= utf8_encode(strftime("%B, %d %Y",strtotime($detail->fecha))) ?></h6>
            </div>
        </div>

    </div>
</div>

<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-content-layout template-content-layout-sidebar-right template-main template-clear-fix ">

        <!-- Content -->
        <div class="template-content-layout-column-left">

            <!-- Post -->
            <div class="template-post">

                <!-- Icon -->
                <div class="template-post-section-icon">
                    <div class="template-post-icon template-post-icon-image"></div>
                </div>

                <!-- Preambule -->
                <div class="template-post-section-preambule">
                    <!-- Image -->
                    <div class="template-component-image template-fancybox template-preloader">
                        <a href="<?= $detail->foto  ?>">
                            <img src="<?= $detail->foto  ?>" alt="" />                            
                        </a>
                    </div>
                </div>
                <div class="galleryBlog template-component-image template-fancybox">
                    <?php foreach($detail->fotos as $f): ?>
                        <a href="<?= $f ?>" class="">
                            <img src="<?= $f ?>" alt="">
                        </a>
                    <?php endforeach ?>
                </div>

                <!-- Meta -->
                <div class="template-post-section-meta">

                    <div class="template-post-meta">

                        

                    </div>

                </div>

                <!-- Content -->
                <div class="template-post-section-content">
                    <div class="template-post-content">
                        <?= $detail->texto ?>
                    </div>
                </div>
                

            </div>				

        </div>

        <!-- Sidebar -->
        <div class="template-content-layout-column-right">

            <ul class="template-widget-list">

                <!-- Recent posts widget  -->
                <li>
                    <h6>
                        <span>Publicacions recents</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-recent-post template-widget-recent-post-style-1">
                            <ul>
                                
                                <?php foreach($relacionados->result() as $e): ?>
                                    <li>
                                        <a href="<?= $e->link ?>">
                                            <img src="<?= $e->foto ?>" alt="Veure <?= $e->titulo ?>"/>
                                        </a>
                                        <h6>
                                            <a href="<?= $e->link ?>" title="Veure <?= $e->titulo ?>">
                                                <?= $e->titulo ?>
                                            </a>
                                        </h6>
                                        <span class="template-icon-blog template-icon-blog-date">
                                            <?= utf8_encode(strftime("%B, %d %Y",strtotime($e->fecha))) ?>
                                        </span>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>										
                    </div>
                </li>

                <!-- Categories -->
                <li>
                    <h6>
                        <span>Categories</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-category template-widget-category-style-1">
                            <ul>
                                <?php foreach($categorias->result() as $e): ?>
                                        <li><a href="<?= base_url('blog') ?>?categorias_id=<?= $e->id ?>" class="tag"><?= $e->blog_categorias_nombre ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </div>										
                    </div>
                </li>

                <!-- Tags -->
                <!--<li>
                    <h6>
                        <span>Tags</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-tag">
                            <ul>
                                <?php foreach(explode(',',$detail->tags) as $e): ?>
                                        <li><a href="<?= base_url('blog') ?>?direccion=<?= $e ?>"><?= $e ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </li> -->

            </ul>				

        </div>		

    </div>

</div>