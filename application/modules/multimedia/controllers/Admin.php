<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        public function categoria_fotos_privada(){
            $crud = $this->crud_function("","");                         
            $crud->add_action('Adm. Fotos','',base_url('multimedia/admin/galeria_privada').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function galeria_privada($x = '',$y = ''){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('galeria_privada')
                     ->set_url_field('foto')
                     ->set_image_path('img/galeria')
                     ->set_subject('Galeria')
                     ->set_ordering_field('orden')
                     ->set_title_field('titulo')
                     ->set_relation_field('categoria_galeria_id');
            $crud->module = 'multimedia';
            $crud = $crud->render();
            $crud->output = '<div class="alert alert-info">Separar el titulo con el caracter "|" para indicar "titulo|subtitulo"</div>'.$crud->output;
            $crud->title = 'Galeria fotográfica Privada';
            $this->loadView($crud);
        }
        
        public function galeria(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('galeria')
                      ->set_image_path('img/galeria')
                      ->set_url_field('foto')
                      ->set_ordering_field('orden');
            $crud->module = "multimedia";
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function categoria_videos($x = '',$y = ''){
            $crud = $this->crud_function("","");                         
            $crud->add_action('Adm. Videos','',base_url('multimedia/admin/videos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function videos($x = '',$y = ''){
            $crud = $this->crud_function("","");                         
            $crud->where('categoria_videos_id',$x);
            $crud->field_type('categoria_videos_id','hidden',$x);
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function galeria_home($x = '',$y = ''){
            $crud = $this->crud_function("","");                                     
            $crud->field_type('foto','image',array('path'=>'img/galeria','width'=>'1050px','height'=>'760px'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
