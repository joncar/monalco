<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function asignaturas(){
        	$crud = $this->crud_function('','');  
        	$crud->field_type('curso','dropdown',array('1'=>'Primaria','2'=>'Segundaria'));            	
        	$crud = $crud->render();        	
            $this->loadView($crud);
        }

        function calificaciones(){
        	$crud = $this->crud_function('','');
        	$crud->set_relation('asignaturas_id','asignaturas','nombre');              	
        	$crud = $crud->render();        	
            $this->loadView($crud);
        }

        function encuestas(){
            $crud = $this->crud_function('',''); 
            $crud->field_type('curso','dropdown',array('3'=>'Llar de infants','4'=>'Infantil','1'=>'Primaria','2'=>'Secundaria'));
            $crud->set_lang_string('insert_success_message','Se han almacenado los datos con èxito <script>document.location.href="'.base_url('calificaciones/admin/encuestas_detalles/').'"{id}/add";</script>');
            $crud->add_action('Valores','',base_url('calificaciones/admin/encuestas_detalles/').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function encuestas_detalles($valor){
            $crud = $this->crud_function('',''); 
            $crud->where('encuestas_id',$valor)
                 ->field_type('encuestas_id','hidden',$valor)
                 ->unset_columns('encuestas_id');
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
