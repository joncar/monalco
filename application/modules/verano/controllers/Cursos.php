<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Cursos extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function cursos(){
        	$crud = $this->crud_function('','');      
        	$crud->set_subject('Cursos');      
        	$crud->set_field_upload('pdf','files');
        	$crud = $crud->render();        	
            $this->loadView($crud);
        }

        function casal_precios(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function casal_fotos(){
            $crud = $this->crud_function('','');
            $crud->field_type('foto','image',array('path'=>'img/_sample/690x506','width'=>'690px','height'=>'506px'));
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function cursos_semanas(){
        	$crud = $this->crud_function('','');      
        	$crud->set_subject('Semana');      
        	$crud->callback_field('color_tabla',function($val){
        		return '<input type="color" class="form-group" name="color_tabla" value="'.$val.'" id="field-color_tabla">';
        	});      	
        	$crud->set_lang_string('insert_success_message','Datos guardados con éxito, <script>document.location.href="'.base_url('verano/cursos/dias_semanas/').'/{id}/add";</script>');
        	$crud->add_action('Dias','',base_url('verano/cursos/dias_semanas/').'/');
        	$crud = $crud->render();        	
            $this->loadView($crud);
        }

        function dias_semanas($semana){
        	$crud = $this->crud_function('','');      
        	$crud->set_subject('Dias');
        	$crud->where('cursos_semanas_id',$semana);
        	$crud->field_type('cursos_semanas_id','hidden',$semana); 
        	$crud->unset_columns('cursos_semanas_id');       	
        	$crud->unset_back_to_list();
        	$crud = $crud->render();        	
        	$this->as['dias_semanas'] = 'cursos_semanas';
        	$header = $this->crud_function('','');
        	$header->set_theme('header_data')
        		 ->where('cursos_semanas.id',$semana)
        		 ->set_url('verano/cursos/cursos_semanas/');
        	$header = $header->render(1);
        	$header = $header->output;
        	$crud->output = $this->load->view('dias',array('output'=>$crud->output,'header'=>$header),TRUE);

            $this->loadView($crud);
        }	
    }
?>
