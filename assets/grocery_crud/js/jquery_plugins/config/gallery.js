var croppers = [];
var crops = [];
var example = '';
$(document).ready(function(){	
	
    $(".galleryCrop").each(function(){
        initCrop($(this));
    });

    $(".addCrop").on('click',function(e){
    	addCrop(e,this);
    });

    $(document).on('click',".image-erase",function(e){
    	e.preventDefault();
        var contenedor = $(this).parents('.form-group');
    	$(this).parents('.galleryCrop').remove();

        var urls = contenedor.find('input[type="text"]');
        urls.val('');
        if(contenedor.find('.parentCrop input').length>0){
            for(var i in contenedor.find('.parentCrop input')){
                var l = contenedor.find('.parentCrop input')[i];
                urls.val(urls.val()+','+$(l).val());
            }
        }
    });
});

function addCrop(e,el){
	e.preventDefault();
	var newel = example.clone();
	newel.attr('id',newel.attr('id')+$(document).find('.galleryCrop').length);
	$(el).parents('.form-group').find('.parentCrop').append(newel);
	initCrop(newel);

}

function initCrop(el){
	console.log('ad');
	options = {
        uploadUrl:'cropper/save',
        cropUrl:'cropper/crop',
        deleteUrl:'cropper/delete_crop',
        modal:true,
        imgEyecandyOpacity:0.4,
        loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ' 
	};
    c = new GalleryCroppic(el.attr('id'),options,$('#'+el.attr('id')+" input"));
    crops.push(c);
    example = example==''?el:example;
}