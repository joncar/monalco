<?php 
	session_start();
	$_SESSION["verify"] = "FileManager4TinyMCE";
	include 'config.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
	<title>Administrador FTP</title>

	<!-- Section CSS -->
	<!-- jQuery UI (REQUIRED) -->
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/jquery/jquery-ui-1.12.0.css" type="text/css">

	<!-- elfinder css -->
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/commands.css"    type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/common.css"      type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/contextmenu.css" type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/cwd.css"         type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/dialog.css"      type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/fonts.css"       type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/navbar.css"      type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/places.css"      type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/quicklook.css"   type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/statusbar.css"   type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/theme.css"       type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/toast.css"       type="text/css">
	<link rel="stylesheet" href="<?= $base_url ?>assets/elfinder/css/toolbar.css"     type="text/css">

	<!-- Section JavaScript -->
	<!-- jQuery and jQuery UI (REQUIRED) -->
	<script src="<?= $base_url ?>assets/elfinder/jquery/jquery-1.12.4.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?= $base_url ?>assets/elfinder/jquery/jquery-ui-1.12.0.js" type="text/javascript" charset="utf-8"></script>

	<!-- elfinder core -->
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.version.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/jquery.elfinder.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.mimetypes.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.options.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.options.netmount.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.history.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.command.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/elFinder.resources.js"></script>

	<!-- elfinder dialog -->
	<script src="<?= $base_url ?>assets/elfinder/js/jquery.dialogelfinder.js"></script>

	<!-- elfinder default lang -->
	<script src="<?= $base_url ?>assets/elfinder/js/i18n/elfinder.en.js"></script>

	<!-- elfinder ui -->
	<script src="<?= $base_url ?>assets/elfinder/js/ui/button.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/contextmenu.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/cwd.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/dialog.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/fullscreenbutton.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/navbar.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/navdock.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/overlay.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/panel.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/path.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/places.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/searchbutton.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/sortbutton.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/stat.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/toast.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/toolbar.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/tree.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/uploadButton.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/viewbutton.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/ui/workzone.js"></script>

	<!-- elfinder commands -->
	<script src="<?= $base_url ?>assets/elfinder/js/commands/archive.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/back.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/copy.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/cut.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/chmod.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/colwidth.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/download.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/duplicate.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/edit.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/empty.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/extract.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/forward.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/fullscreen.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/getfile.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/help.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/hidden.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/home.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/info.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/mkdir.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/mkfile.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/netmount.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/open.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/opendir.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/paste.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/places.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/quicklook.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/quicklook.plugins.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/reload.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/rename.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/resize.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/restore.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/rm.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/search.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/selectall.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/selectinvert.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/selectnone.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/sort.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/undo.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/up.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/upload.js"></script>
	<script src="<?= $base_url ?>assets/elfinder/js/commands/view.js"></script>

	<!-- elfinder 1.x connector API support (OPTIONAL) -->
	<script src="<?= $base_url ?>assets/elfinder/js/proxy/elFinderSupportVer1.js"></script>

	<!-- Extra contents editors (OPTIONAL) -->
	<script src="<?= $base_url ?>assets/elfinder/js/extras/editors.default.js"></script>

	<!-- GoogleDocs Quicklook plugin for GoogleDrive Volume (OPTIONAL) -->
	<script src="<?= $base_url ?>assets/elfinder/js/extras/quicklook.googledocs.js"></script>

	<!-- elfinder initialization  -->
	<script>
		$(function() {
			$('#elfinder').elfinder(
				// 1st Arg - options
				{
					// Disable CSS auto loading
					cssAutoLoad : false,

					// Base URL to <?= $base_url ?>assets/elfinder/css/*, <?= $base_url ?>assets/elfinder/js/*
					baseUrl : './',

					// Connector URL
					url : '<?= $base_url ?>paginas/admin/verImg/1',

					// Callback when a file is double-clicked
					getFileCallback : function(file) {
						var window_parent=window.parent;
						var track = '<?php echo $_GET['editor']; ?>';
						var target = window_parent.document.getElementsByClassName('mce-img_'+track);						
						if(target.length==0){
							target = window_parent.document.getElementsByClassName('mce-link_'+track);
						}
						var closed = window_parent.document.getElementsByClassName('mce-filemanager');
						var base_url = '<?= $base_url ?>';
						$(target).val(base_url+file.path);
						$(closed).find('.mce-close').trigger('click');
						console.log(window_parent);
						console.log(file);
						if(typeof(window_parent.onSelModalCropPhoto)!=='undefined'){
							window_parent.onSelModalCropPhoto(file.path);
						}
					},
				},
				
				// 2nd Arg - before boot up function
				function(fm, extraObj) {
					// `init` event callback function
					fm.bind('init', function() {
						// Optional for Japanese decoder "extras/encoding-japanese.min"
						delete fm.options.rawStringDecoder;
						if (fm.lang === 'ja') {
							fm.loadScript(
								[ fm.baseUrl + '<?= $base_url ?>assets/elfinder/js/extras/encoding-japanese.min.js' ],
								function() {
									if (window.Encoding && Encoding.convert) {
										fm.options.rawStringDecoder = function(s) {
											return Encoding.convert(s,{to:'UNICODE',type:'string'});
										};
									}
								},
								{ loadType: 'tag' }
							);
						}
					});
					
					// Optional for set document.title dynamically.
					var title = document.title;
					fm.bind('open', function() {
						var path = '',
							cwd  = fm.cwd();
						if (cwd) {
							path = fm.path(cwd.hash) || null;
						}
						document.title = path? path + ':' + title : title;
					}).bind('destroy', function() {
						document.title = title;
					});
				}
			);
		});
	</script>
</head>
<body>
	<div style="background:lightblue; padding:10px;">
		Pulsa doble click en la imagen para seleccionarla.
	</div>
	<div id="elfinder"></div>
</body>
</html>
